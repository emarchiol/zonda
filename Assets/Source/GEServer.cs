﻿using UnityEngine;
using System;
using UnityEngine.UI;
using System.Net.Sockets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

//Manipulacion de objetos y eventos durante el juego
public class GEServer : AGameEnviroment
{

    // Prefab de sidebar para volver a transformar los objetos.
    public GameObject PrefabSideBar;
    public GameObject Canvas;
    //public GameObject GGEInformationPanel;
    public Button ButtonShowSideBar;
    
    //public GameObject Console;
    //Vector3 offset = Vector3.zero;
    public GameObject[] PlayersZone;

    public override void Start()
    {
        InformationPanel = GGEInformationPanel;
        // Board started, enables player zone.
        if (session.playersAmount > 0)
            PlayersZone[session.playersAmount - 1].SetActive(true);
        // Debido a que la cámara está angulada al hacer zoom in se modifica el valor de Y ocasionando que la cámara deje de enfocar a la mesa (se aleja o acerca demasiado).    
        CameraYPosition = GetComponent<Camera>().transform.position.y;
        //SetObjectsOnBoard();
        ConsoleScript.AddConsoleOutput("Board started.");
        WorldTouch = new GameObject("TempWorldTouch");
        //Esto es para que las cartas no se pongan "on top" con la camara
        Camera.main.transparencySortMode = TransparencySortMode.Orthographic;

        // La mesa fisica de la scene la asocio a la que fue cargada desde playerOptions
        //this.FloorPrefab.GetComponent<FloorScript>().Floor = session.PlayerFloor;
        if (!session.restartSession)
        { 
            // Solo servidor.
            NetMessage nm;
            // Foreacheo los jugadores a los jugadores.
            foreach (Player p in session.GetPlayersList())
            {
                foreach (Player pTosend in session.GetPlayersList())
                {
                    if (p != pTosend)
                    {
                        // Creo el mensaje y envío.
                        nm = new NetMessage(pTosend);
                        Network.SendAGGEToPlayer(nm, p);
                    }
                }
                // Los clientes ya pueden mostrar la mesa.
                /*
                nm = new NetMessage(session.PlayerInfo, "SLOADED");
                Debug.Log("sending SLOADED");
                Network.SendAGGEToPlayer(nm, p);
                Debug.Log("sever loaded msg sent to:" + p.name);*/
            }
        }
    }

    public override void Update()
    {
        //CameraControl y touchControl tiran nullReference exception
        DetectDevice();

            if (session.GetNetworkSidebar().Count > 0)
            {
                // Agrego item a la sidebar.
                foreach (AGenericGameElement g in session.GetNetworkSidebar())
                {
                    Debug.Log("networkSidebar agge:" + g.Name);
                    session.AddToSideBarGGEs(g);
                }

                // Lo remuevo de network.
                /*List<AGenericGameElement> sidebarCopy;
                sidebarCopy = session.GetSideBarGGEs();*/
                foreach (AGenericGameElement g in session.GetSideBarGGEs())
                {
                    Debug.Log("sidebar agge:" + g.Name);
                    session.RemoveFromNetworkSidebar(g);
                }
        }
        //Debug.Log("onboard tiene:" + session.GetOnBoardGGES().Count);
        //Debug.Log("players amount for this client:" + session.Players.Count);
    }

    public void RestartSession()
    {
        // Limpio las listas sidebar y gameboard.
        session.test++;
        session.ResetSession();
        session.restartSession = true;
        try
        {
            // Si soy servidor aviso a los demás, este if no va en el cliente.
            if (session.PlayerInfo.pNumber == 0)
            {
                // Dejar de recibir cosas y avisar el resto de los jugadores que resetee la sesión.
                NetMessage nm;
                foreach (Player p in session.GetPlayersList())
                {
                    nm = new NetMessage(session.PlayerInfo, "SRESTART");
                    Network.SendAGGEToPlayer(nm, p);
                }
            }
            // La carga de objetos del player se hace sola desde AGameEnviroment.
            // Cierro el hilo viejo escuchando.
            ShutDownListeningThread();
            // Finalmente cargamos la scene.

            ConsoleScript.AddConsoleOutput("Session restarted.");
            ConsoleScript.AddConsoleOutput("---------------------------------");
            SceneManager.LoadScene(5);
        }
        catch (Exception e)
        {
            Debug.Log(e);
            //Application.LoadLevel(5);
        }
    }

    public override void MainMenuButton()
    {
        ShutDownListeningThread();
        SceneManager.LoadScene(0);
    }

    public override void ShutDownListeningThread()
    {
        try
        {
            if (ThreadSocket != null) { 
                ThreadSocket.Close();
                Debug.Log("thread socket closed");
            }
        }
        catch (SocketException e)
        {
            Debug.Log("GEServer: " + e);
        }

        // Solo session ?
        try
        {
            if (WaitingForObjectsThread != null)
            { 
                WaitingForObjectsThread.Abort();
                Debug.Log("listening thread aborted.");
            }
        }
        catch (Exception e)
        {
            Debug.Log("GEServer:thread abort:" + e);
        }
    }

}
