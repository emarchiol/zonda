﻿using System;
using UnityEngine;

public class FloorScript : AGGEScript {

    public Board Floor { set; get; }
    // Use this for initialization
    void Start()
    {
        Floor = session.PlayerFloor;
        this.transform.parent.GetComponent<Transform>().localScale = new Vector3(Floor.RatioW, 1, Floor.RatioH);
        this.isStatic = true;
    }

    public override void DoubleTap() { }

    public override void TripleTap() { }

    public override string GetName()
    {
        return "Floor";
    }

    public override void Lock() { }

    public override void SetPrefabReference(AGenericGameElement gge) { }

    public override void StackObject()
    {
    }

    public override void InitObject()
    {
    }
}
