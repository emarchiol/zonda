﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Reflection;

public class CardScript : AGGEScript {

    public Card card;
    public Plane Border;

    private Material MaterialFront;
    private Material MaterialBack;
    private Material MaterialNoImage;
    private Material BorderStatus;
    //public bool ShowFront {set;get;}
	
    void Start()
    {
        // Use InitObject for initialization.
    }

    public override string GetName()
	{
		return "Card";
	}

	public override void Lock()
	{
		this.isLocked = !this.isLocked;
		if(this.isLocked)
		{
			MaterialFront.color = Color.gray;
			MaterialBack.color = Color.gray;

            Color c;
            if (ColorUtility.TryParseHtmlString("FFD700BE", out c))
            {
                BorderStatus.color = c;
                this.GetComponentInChildren<GameObject>().GetComponent<Renderer>().material = BorderStatus;
                Debug.Log("apliqué el color");

            }

            /*Color color = BorderStatus.color;
            color.a = 190;
            BorderStatus.color = color;*/
        }
		else
		{
			MaterialFront.color = Color.white;
			MaterialBack.color = Color.white;
		}
	}

	public override void TripleTap()
    {
    }

	public override void SetPrefabReference (AGenericGameElement gge)
	{
		this.card = (Card)gge;
        
        //Cargo las imagenes de la carta en materiales y aplico el tamaño que posee
        MaterialBack = this.MaterialLoad(card.BackImage);
        MaterialFront = this.MaterialLoad(card.FrontImage);
        this.transform.localScale = new Vector3(card.RatioW, 1, card.RatioH);

        if (this.card.isFlipped)
			this.GetComponent<Renderer>().material = MaterialFront;
		else
			this.GetComponent<Renderer>().material = MaterialBack;

        //Si las caras son iguales siempre esta de frente
        if (card.BackImage == card.FrontImage)
        {
            this.card.isFlipped = true;
        }
    }

	public override void DoubleTap()
	{
        //Debug.Log("Card id:" + this.card.Id);
		if(this.card.isFlipped)
		{
			this.GetComponent<Renderer>().material = MaterialBack;
            if (card.BackImage != card.FrontImage) //Si las caras son iguales siempre esta de frente
                this.card.isFlipped = false;
		}
		else
		{
			this.GetComponent<Renderer>().material = MaterialFront;
            if (card.BackImage != card.FrontImage) //Si las caras son iguales siempre esta de frente
                this.card.isFlipped = true;
		}
	}
    

    //[MenuItem]
    public void CreateDeck()
    {

    }

    public Material MaterialLoad(string ImageToApply)
	{
        WWW www;
        string url;
        //Tomo el path para www y www lo "descarga" desde el disco para convertir la imagen en textura

        if (ImageToApply.CompareTo("")==0 || ImageToApply == null)
        {
            Debug.Log("noimage material loaded");
            return MaterialNoImage;
        }
        else
        {
            url = GetPath(ImageToApply);
            www = new WWW(url);
            //Termine de descargar ?
            if (www.isDone) { }
            //Puntero al renderer del objeto creado (carta o lo que sea)
            MeshRenderer renderer = this.GetComponent<MeshRenderer>();
            //Creo un nuevo material y se lo asigno al MeshRender de clone
            try
            {
                renderer.material = new Material(Shader.Find("Sprites/Default"));
                renderer.material.mainTexture = www.texture as Texture2D;
                return renderer.material;
            }
            catch (Exception e)
            {
                Debug.Log("No pude asignar/crear el material con esa textura:" + e);
                MaterialBack = MaterialNoImage;
                return MaterialNoImage;
            }
        }
    }

    //Devuelve el path del archivo independiente del OS
	private string GetPath(string image)
	{
		string path;
		string partialPath = session.GamePath;
		//partialPath = Application.persistentDataPath;
		//Debug.Log("General Path:"+this.generalPath);
		#if UNITY_EDITOR
		partialPath = partialPath.Replace("c:/","C://");
		partialPath = partialPath.Replace("C:/","C://");
		path = "file:///" + partialPath +"images/"+image;
		//Debug.Log("Unity path:"+path);
		#elif UNITY_ANDROID
		path = "file://"+ partialPath + "images/"+image;
		//Debug.Log("Android path:"+path);
		#elif UNITY_IOS
		path = "file:" + partialPath + "images/"+image;
		#else
		//Desktop (Mac OS or Windows)
		partialPath = partialPath.Replace("c:/","C://");
		partialPath = partialPath.Replace("C:/","C://");
		path = "file:///"+ partialPath + "images/"+image;
		#endif
		return path;
	}

	public bool isFront()
	{
		return this.card.isFlipped;
	}

    public override void StackObject()
    {
        GameObject ChildToStack = this.gameObject;
        //float DistanceToStack = 3.5f;
        float zDistance = 1.5f;
        Ray rayObject = new Ray(ChildToStack.transform.position, Vector3.down);
        RaycastHit objHit;

        if (Physics.Raycast(rayObject, out objHit, 200))
        {
            if (objHit.collider != null)
            {
                if (objHit.collider.GetComponent<AGGEScript>() != null)
                    if (objHit.collider.GetComponent<CardScript>() != null)
                    {
                        if (Vector3.Distance(ChildToStack.transform.position, objHit.collider.transform.position) < DistanceToStack)
                        {
                            if (objHit.collider.transform.childCount <= 0)
                            {
                                ChildToStack.transform.SetParent(objHit.collider.transform);
                                ChildToStack.GetComponent<RectTransform>().transform.position = new Vector3(objHit.collider.transform.position.x, objHit.collider.transform.position.y + 0.1f, objHit.collider.transform.position.z - zDistance);
                            }
                            else
                            {
                                GameObject child;
                                do
                                {
                                    child = objHit.collider.transform.GetChild(0).gameObject;
                                } while (child.transform.childCount <= 0);

                                ChildToStack.transform.SetParent(objHit.collider.transform);
                                ChildToStack.transform.position = new Vector3(0, 0, 0);
                                ChildToStack.transform.position = new Vector3(ChildToStack.transform.position.x, +0.1f, +1.2f);
                            }
                        }
                    }
                    else if (objHit.collider.GetComponent<CardDeckScript>()!=null)
                    {
                        if (Vector3.Distance(ChildToStack.transform.position, objHit.collider.transform.position) < DistanceToStack)
                        {
                            CardDeckScript cdS = objHit.collider.GetComponent<CardDeckScript>();
                            if (this.transform.childCount <= 0)
                            {
                                cdS.AddToDeck(ChildToStack.gameObject.GetComponent<CardScript>().card);
                            }
                            else
                            {
                                // Agrego el padre.
                                cdS.AddToDeck(this.GetComponent<CardScript>().card);
                                GameObject child = this.transform.gameObject;
                                do
                                {
                                    child = child.transform.GetChild(0).gameObject;
                                    cdS.AddToDeck(child.GetComponent<CardScript>().card);
                                } while (child.transform.childCount > 0);
                            }
                            // Chau carta/s.
                            this.MenuScript.DestroySimpleMenu();
                            Destroy(this.gameObject);
                        }
                    }
            }
            else
            {
                // Nothing hitted with Object.
            }
        }
    }

    public override void InitObject()
    {
        this.gge = card;
        MaterialNoImage = new Material(Resources.Load("Materials/LogoCard") as Material);
        BorderStatus = new Material(Resources.Load("Materials/CardBorder") as Material);
    }
}
