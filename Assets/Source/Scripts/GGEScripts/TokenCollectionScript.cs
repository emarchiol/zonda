﻿using UnityEngine;
using System.Collections;
using System;
using System.Xml.Linq;
using System.Linq;

public class TokenCollectionScript : ACollectionGGEScript
{
    public bool isCube;

    public GameObject CirclePrefab;
    public GameObject prefabCubeCollectionMin;
    public GameObject prefabCubeCollectionMed;
    public GameObject prefabCubeCollectionMax;

    public GameObject prefabCircleCollectionMin;
    public GameObject prefabCircleCollectionMed;
    public GameObject prefabCircleCollectionMax;

    public TextMesh CounterText;
    public TokenCollection TCollection;

    public override void InitObject(){}

    public override void DoubleTap()
    {
        // Remove objects

        Token token = new Token();
        token = this.TCollection.Tokens[0];
        this.TCollection.Tokens.Remove(token);
        //AGameEnviroment.OnBoardGges.Add(carta);

        //session.AddToOnBoardGGEs(carta);

        //Debug.Log("prefab:" + this.GetPrefabClone().name);

        GameObjectGenerator.SpawnObject(token as AGenericGameElement,
            new Vector3(
                        this.GetPrefabClone().transform.position.x + (0.5f * 10) + 1
                        , this.GetPrefabClone().transform.position.y,
                        this.GetPrefabClone().transform.position.z));

        if (this.TCollection.Tokens.Count == 0)
        { 
            this.MenuScript.DestroySimpleMenu();
            Destroy(this.gameObject);
        }
        else
        { 
            this.CounterText.text = this.TCollection.Tokens.Count.ToString();
            SetDecoration();
        }
    }

    public override void TripleTap()
    {
    }

    public override string GetName()
    {
        return "Tokens collection";
    }

    public override void Lock()
    {
        this.isLocked = !this.isLocked;
    }

    public override void SetPrefabReference(AGenericGameElement gge)
    {
        this.TCollection = gge as TokenCollection;
        SetDecoration();
        this.CounterText.text = TCollection.Tokens.Count.ToString();
    }

    public override void StackObject()
    {

    }

    public override void AddToCollection(AGenericGameElement gge)
    {
        this.TCollection.Tokens.Add(gge as Token);
        this.CounterText.text = this.TCollection.Tokens.Count.ToString();
        SetDecoration();

    }

    public void SetDecoration()
    {
        // Preguntar que tipo de objeto es y activar el prefab
        if(TCollection.Tokens.Count>0)
            if (TCollection.Tokens[0].Shape == "cube")
                isCube = true;
            else
                isCube = false;
        if (this.TCollection.Tokens.Count >= 1 )
        {
            if (isCube)
            { 
                this.prefabCubeCollectionMin.SetActive(true);
                this.prefabCubeCollectionMed.SetActive(false);
                this.prefabCubeCollectionMax.SetActive(false);

                this.prefabCircleCollectionMin.SetActive(false);
                this.prefabCircleCollectionMed.SetActive(false);
                this.prefabCircleCollectionMax.SetActive(false);
                SetColor(this.prefabCubeCollectionMin);
            }
            else
            { 
                this.prefabCircleCollectionMin.SetActive(true);
                this.prefabCircleCollectionMed.SetActive(false);
                this.prefabCircleCollectionMax.SetActive(false);

                this.prefabCubeCollectionMin.SetActive(false);
                this.prefabCubeCollectionMed.SetActive(false);
                this.prefabCubeCollectionMax.SetActive(false);
                SetColor(this.prefabCircleCollectionMin);
            }
        }
        if (this.TCollection.Tokens.Count > 2)
        {
            if (isCube)
            {
                this.prefabCubeCollectionMin.SetActive(false);
                this.prefabCubeCollectionMed.SetActive(true);
                this.prefabCubeCollectionMax.SetActive(false);

                this.prefabCircleCollectionMin.SetActive(false);
                this.prefabCircleCollectionMed.SetActive(false);
                this.prefabCircleCollectionMax.SetActive(false);
                SetColor(this.prefabCubeCollectionMed);
            }
            else
            {
                this.prefabCircleCollectionMin.SetActive(false);
                this.prefabCircleCollectionMed.SetActive(true);
                this.prefabCircleCollectionMax.SetActive(false);

                this.prefabCubeCollectionMin.SetActive(false);
                this.prefabCubeCollectionMed.SetActive(false);
                this.prefabCubeCollectionMax.SetActive(false);
                SetColor(this.prefabCircleCollectionMed);
            }
        }
        if (this.TCollection.Tokens.Count > 3)
        {
            if (isCube)
            {
                this.prefabCubeCollectionMin.SetActive(false);
                this.prefabCubeCollectionMed.SetActive(false);
                this.prefabCubeCollectionMax.SetActive(true);

                this.prefabCircleCollectionMin.SetActive(false);
                this.prefabCircleCollectionMed.SetActive(false);
                this.prefabCircleCollectionMax.SetActive(false);
                SetColor(this.prefabCubeCollectionMax);
            }
            else
            {
                this.prefabCircleCollectionMin.SetActive(false);
                this.prefabCircleCollectionMed.SetActive(false);
                this.prefabCircleCollectionMax.SetActive(true);

                this.prefabCubeCollectionMin.SetActive(false);
                this.prefabCubeCollectionMed.SetActive(false);
                this.prefabCubeCollectionMax.SetActive(false);
                SetColor(this.prefabCircleCollectionMax);
            }
        }

        if (TCollection.Tokens[0].Shape == "cube")
            isCube = true;
        else
            isCube = false;
    }

    public void SetColor(GameObject parentDeco)
    {
        var allChildren = parentDeco.transform.Cast<Transform>().Select(t => t.gameObject).ToArray();
        // Set color
        foreach (GameObject child in allChildren)
        { 
            switch (this.TCollection.Tokens[0].Color)
            {
                case "red":
                    child.GetComponent<Renderer>().material.color = Color.red;
                    break;
                case "blue":
                    child.GetComponent<Renderer>().material.color = Color.blue;
                    break;
                case "green":
                    child.GetComponent<Renderer>().material.color = Color.green;
                    break;
                case "yellow":
                    child.GetComponent<Renderer>().material.color = Color.yellow;
                    break;
                case "black":
                    child.GetComponent<Renderer>().material.color = Color.black;
                    break;
                case "white":
                    child.GetComponent<Renderer>().material.color = Color.white;
                    break;
                default:
                    child.GetComponent<Renderer>().material.color = Color.grey;
                    break;
            }
        }
    }
}