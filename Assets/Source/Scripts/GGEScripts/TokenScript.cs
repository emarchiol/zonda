﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class TokenScript : AGGEScript {
	
	public Token token;

	void Start()
    {
        // Use InitObject for initialization.
    }

    public override void Lock()
	{
		this.isLocked = !this.isLocked;
	}

	public override void TripleTap(){}
	
	public override void DoubleTap ()
	{
    }

	public override string GetName ()
	{
		return "Token";
	}

	public override void SetPrefabReference (AGenericGameElement gge)
	{
		this.token = (Token)gge;
        // Set color
        switch (this.token.Color)
        {
            case "red":
                this.gameObject.GetComponent<Renderer>().material.color = Color.red;
                break;
            case "blue":
                this.gameObject.GetComponent<Renderer>().material.color = Color.blue;
                break;
            case "green":
                this.gameObject.GetComponent<Renderer>().material.color = Color.green;
                break;
            case "yellow":
                this.gameObject.GetComponent<Renderer>().material.color = Color.yellow;
                break;
            case "black":
                this.gameObject.GetComponent<Renderer>().material.color = Color.black;
                break;
            case "white":
                this.gameObject.GetComponent<Renderer>().material.color = Color.white;
                break;
            default:
                this.gameObject.GetComponent<Renderer>().material.color = Color.grey;
                break;
        }
    }

    public override void StackObject()
    {
        GameObject ChildToStack = this.gameObject;
        //float DistanceToStack = 3.5f;
        //float zDistance = 1.5f;

        Ray rayObject = new Ray(ChildToStack.transform.position, Vector3.down);
        RaycastHit objHit;

        if (Physics.Raycast(rayObject, out objHit, 200))
        {
            if (objHit.collider != null)
            {
                if (objHit.collider.GetComponent<AGGEScript>() != null)
                    // Dos token, crean un collection.
                    if (objHit.collider.GetComponent<TokenScript>() != null)
                    {
                        if (Vector3.Distance(ChildToStack.transform.position, objHit.collider.transform.position) < DistanceToStack)
                        {
                            // Creo un nuevo TokenCollection.
                            TokenCollection tc = new TokenCollection();
                            // Agrego los dos tokens a la lista.
                            tc.Add(gge);
                            tc.Add(objHit.collider.GetComponent<TokenScript>().token);
                            // Spawneo el collection en dónde estaba el token.
                            GameObjectGenerator.SpawnObject(tc,objHit.collider.transform.position);

                            objHit.collider.GetComponent<AGGEScript>().MenuScript.DestroySimpleMenu();
                            this.MenuScript.DestroySimpleMenu();

                            Destroy(objHit.collider.gameObject);
                            Destroy(this.gameObject);
                            /*
                            if (objHit.collider.transform.childCount <= 0)
                            {
                                ChildToStack.transform.SetParent(objHit.collider.transform);
                                ChildToStack.GetComponent<RectTransform>().transform.position = new Vector3(objHit.collider.transform.position.x, objHit.collider.transform.position.y + 0.1f, objHit.collider.transform.position.z - zDistance);
                            }
                            else
                            {
                                GameObject child;
                                do
                                {
                                    child = objHit.collider.transform.GetChild(0).gameObject;
                                } while (child.transform.childCount <= 0);

                                ChildToStack.transform.SetParent(objHit.collider.transform);
                                ChildToStack.transform.position = new Vector3(0, 0, 0);
                                ChildToStack.transform.position = new Vector3(ChildToStack.transform.position.x, +0.1f, +1.2f);
                            }*/
                        }
                    }
                    // Agrega al collection.
                    else if (objHit.collider.GetComponent<TokenCollectionScript>() != null)
                    {
                        if (Vector3.Distance(ChildToStack.transform.position, objHit.collider.transform.position) < DistanceToStack)
                        {
                            TokenCollectionScript tc = objHit.collider.GetComponent<TokenCollectionScript>();
                            // Agrego el padre.
                            tc.AddToCollection(this.GetComponent<TokenScript>().token);
                            // Chau token/s.
                            this.MenuScript.DestroySimpleMenu();
                            Destroy(this.gameObject);
                        }
                    }
            }
            else
            {
                // Nothing hitted with Object.
            }
        }
    }

    public override void InitObject()
    {
        
    }
}
