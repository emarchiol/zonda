﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using System.Reflection;
using UnityEngine.EventSystems;

public class CardDeckScript : ACollectionGGEScript{
	
	public CardDeck deck;
    public TextMesh CounterText;
	private Material MaterialFront;
	private Material MaterialBack;
	private Material MaterialOutOfCards;
    private Material MaterialNoImage;
    

    void Start()
    {
        // Use InitObject for initialization.
        this.CounterText.text = this.deck.Cards.Count.ToString();
    }

    public override void InitObject()
    {
        MaterialNoImage = new Material(Resources.Load("Materials/LogoCard") as Material);
    }

    public void AddToDeck(Card cardToAdd)
    {
        // Restablezco la imagen principal.
        if (this.deck.Cards.Count == 0)
        {
            if (this.deck.ShowFront)
                this.GetComponent<Renderer>().material = MaterialFront;
            else
                this.GetComponent<Renderer>().material = MaterialBack;
        }
        this.deck.Add(cardToAdd);
        this.CounterText.text = this.deck.Cards.Count.ToString();
    }

	public override string GetName()
	{
		return "Deck";
	}

	public override void Lock()
	{
		this.isLocked = !this.isLocked;
		if(this.isLocked)
			this.GetComponent<Renderer>().material.color = Color.gray;
		else
		{
			this.GetComponent<Renderer>().material.color = Color.white;
		}
	}
	
	public override void TripleTap(){}
	
	public override void DoubleTap()
	{
        Deal();
	}

    [MenuItemAttribute]
    public void Deal()
    {
        //Instanciar una carta y ponerla en OnBoard
        Card carta = new Card();
        carta = this.deck.Cards[0];
        this.deck.Cards.Remove(carta);
        //AGameEnviroment.OnBoardGges.Add(carta);

        //session.AddToOnBoardGGEs(carta);

        //Debug.Log("prefab:" + this.GetPrefabClone().name);

        GameObjectGenerator.SpawnObject(carta as AGenericGameElement,
            new Vector3(
                        this.GetPrefabClone().transform.position.x + (carta.RatioW * 10) + 1
                        , this.GetPrefabClone().transform.position.y,
                        this.GetPrefabClone().transform.position.z));

        if (this.deck.Cards.Count == 0)
            this.GetComponent<Renderer>().material = MaterialOutOfCards;

        this.CounterText.text = this.deck.Cards.Count.ToString();
    }
    [MenuItemAttribute]
    public void Shuffle()
	{
        this.deck.Shuffle();
    }

    public override void SetPrefabReference (AGenericGameElement gge)
	{
		this.deck = (CardDeck)gge;

        MaterialOutOfCards = Resources.Load("Materials/EmptyDeck") as Material;
        if (this.deck.ShowFront)
        {
            MaterialFront = MaterialLoad(this.deck.FrontImage);
            this.GetComponent<Renderer>().material = MaterialFront;
        }
        else
        {
            MaterialBack = MaterialLoad(this.deck.BackImage);
            this.GetComponent<Renderer>().material = MaterialBack;
        }
        this.transform.localScale = new Vector3(this.deck.RatioW, 1, this.deck.RatioH);


        if (this.deck.Cards.Count == 0)
		{
			this.GetComponent<Renderer>().material = MaterialOutOfCards;
		}
	}
	
    public Material MaterialLoad(string ImageToApply)
    {
        WWW www;
        string url;
        //Tomo el path para www y www lo "descarga" desde el disco para convertir la imagen en textura

        if (ImageToApply.CompareTo("") == 0)
        {
            return MaterialNoImage;
        }
        else
        {
            url = GetPath(ImageToApply);
            www = new WWW(url);
            // OJO esto no es correcto, no es espera (solo funciona xq el disco es rápido)
            if (www.isDone) { }

            //Puntero al renderer del objeto creado (carta o lo que sea)
            MeshRenderer renderer = this.GetComponent<MeshRenderer>();
            //Creo un nuevo material y se lo asigno al MeshRender de clone
            try
            {
                renderer.material = new Material(Shader.Find("Sprites/Default"));
                renderer.material.mainTexture = www.texture as Texture2D;
                return renderer.material;
            }
            catch (Exception e)
            {
                Debug.Log("No pude asignar/crear el material con esa textura:" + e);
                MaterialBack = MaterialNoImage;
                return MaterialNoImage;
            }
        }
    }

    //Devuelve el path del archivo independiente del OS
    private string GetPath(string image)
	{
		string path;
		string partialPath = session.GamePath;
		//partialPath = Application.persistentDataPath;
		//Debug.Log("General Path:"+this.generalPath);
		#if UNITY_EDITOR
		partialPath = partialPath.Replace("c:/","C://");
		partialPath = partialPath.Replace("C:/","C://");
		path = "file:///" + partialPath +"images/"+image;
		//Debug.Log("Unity path:"+path);
		#elif UNITY_ANDROID
		path = "file://"+ partialPath + "images/"+image;
		//Debug.Log("Android path:"+path);
		#elif UNITY_IOS
		path = "file:" + partialPath + "images/"+image;
		#else
		//Desktop (Mac OS or Windows)
		partialPath = partialPath.Replace("c:/","C://");
		partialPath = partialPath.Replace("C:/","C://");
		path = "file:///"+ partialPath + "images/"+image;
		#endif
		return path;
	}

    // Si me agregan una carta, la agrego al mazo.
    public override void StackObject(){}

    public override void AddToCollection(AGenericGameElement gge)
    {
        
    }
}