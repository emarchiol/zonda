﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class SideBarElementScript : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {

    public AGenericGameElement gge { get; set; }
    public Button Button;
    public Text Title;
    public Text Description;
    public int Id;
    public Image Avatar;
    public GameObject itSelf;
    private GameObject canvas;
    private Game session = Game.NewInstance();

    //private GameObject sideBarContainer;
    
    void Awake()
    {
        canvas = GameObject.Find("Canvas");
        //sideBarContainer = GameObject.Find("SideBarContent");
        //this.transform.SetParent(sideBarContainer.transform);
    }

    public void OnDrag(PointerEventData eventData)
    {
        this.transform.position = eventData.position;
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        this.transform.SetParent(canvas.transform);
        this.GetComponent<CanvasGroup>().blocksRaycasts = false;
        session.CameraEnabled = false;
        session.GGeTouch = false;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        this.GetComponent<CanvasGroup>().blocksRaycasts = true;
        RaycastHit hit;
        Ray landingRay = Camera.main.ScreenPointToRay(eventData.position);
        try
        {
            if (Physics.Raycast(landingRay, out hit, 300))
            {
                //Debug.Log("Ray position x:" + hit.point.x + ", y:" + hit.point.y + ", z:" + hit.point.z);
                Destroy(itSelf);
                GameObjectGenerator.SpawnObject(this.gge, hit.point);
                session.RemoveSideBarGGE(this.gge);
            }
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }
        finally
        {
            session.CameraEnabled = true;
            session.GGeTouch = true;
        }

    }

}

