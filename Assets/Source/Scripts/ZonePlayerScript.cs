﻿using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;

public class ZonePlayerScript : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

    //public GameObject[] PlayersZone;

    Game session;
    List<AGenericGameElement> GGEZoneToSpawn;
    //public TextMesh toPlayerText;
    int playerNum;
    //public GameObject UIPanel;
    public Text ToPlayerText;
    Vector3 initialScale;
    Player pToZone;

    void Start()
    {
        this.initialScale = this.GetComponent<RectTransform>().localScale;
        this.session = Game.NewInstance();
        GGEZoneToSpawn = new List<AGenericGameElement>();
        playerNum = Convert.ToInt16(this.gameObject.name);
        try // El try es solo para fake session.
        {
            foreach (Player p in session.GetPlayersList())
            {
                if (p.pNumber == playerNum)
                {
                    this.ToPlayerText.text = "To/From "+p.name;
                    pToZone = p;
                }
            }
        }
        catch (Exception ) { }
    }
    // Zonas viejas 3D.
    /*
    void OnTriggerExit(Collider objToSend)
    {
        Debug.Log("OnTriggerExit");
        objToSend.GetComponent<AGGEScript>().inZone = false;

        //if (objToSend.GetComponent<AGGEScript>() != null)
            objToSend.GetComponent<AGGEScript>().OpenMenu();
        // Enable the collider for drag and drop
        GameObject childObject = objToSend.gameObject;
        // Disable the collider otherwise it will send it multiple times if has childs.
        if (childObject.GetComponent<AGGEScript>().gge.GetType().ToString() == "Card")
        {
            if (childObject.transform.parent.gameObject.GetComponent<AGGEScript>() == null)
            {
                if (childObject.transform.childCount > 0)
                {
                    do
                    {
                        childObject = childObject.gameObject.transform.GetChild(0).gameObject;
                        childObject.GetComponent<MeshCollider>().enabled = true;
                    } while (childObject.gameObject.transform.childCount > 0);
                }
            }
        }
    }

    void OnTriggerEnter(Collider objToSend)
    {
        // Si el objeto aparece en la zona.
        if (!objToSend.gameObject.GetComponent<AGGEScript>().isTouched)
        {
            Debug.Log("OnTriggerEnter inZone true");
            objToSend.gameObject.GetComponent<AGGEScript>().inZone = true;
        }
        else
        { 
            GameObject childObject = objToSend.gameObject;
            // Disable the collider otherwise it will send it multiple times if has childs.
            Debug.Log("Destroying menu");
            objToSend.gameObject.GetComponent<AGGEScript>().DestroyMenu();
            //objToSend.gameObject.GetComponent<AGGEScript>().simpleMenuOpen = false;

            if (objToSend.gameObject.GetComponent<AGGEScript>().gge.GetType().ToString() == "Card")
            {
                if (objToSend.gameObject.transform.parent.gameObject.GetComponent<AGGEScript>()==null)
                {
                    if (objToSend.gameObject.transform.childCount > 0)
                    {
                        do
                        {
                            childObject = childObject.gameObject.transform.GetChild(0).gameObject;
                            childObject.GetComponent<MeshCollider>().enabled = false;
                        } while (childObject.gameObject.transform.childCount > 0);
                    }
                }
            }
        }
    }

    void OnTriggerStay(Collider objToSend)
    {
        if (objToSend.gameObject != null && objToSend.gameObject.GetComponent<AGGEScript>() != null)
        {
            // Si se suelta el objeto y no llegó desde network entonces lo envío.
            if (!objToSend.gameObject.GetComponent<AGGEScript>().isTouched && !objToSend.gameObject.GetComponent<AGGEScript>().inZone)
            { 
                Debug.Log("sending obj:" + objToSend.gameObject.GetComponent<AGGEScript>().gge.Name);
                SendGGEToPlayer(objToSend.gameObject.GetComponent<AGGEScript>(), session.FindPlayer(playerNum));
                session.RemoveOnBoardGGE(objToSend.gameObject.GetComponent<AGGEScript>());
                Destroy(objToSend.gameObject);
            }
            else{
                //Debug.Log("obj is being touch or spawned inZone");
            }
        }
    }
    */
    // Fin Zonas viejas 3D.

    public void SendGGEToPlayer(AGGEScript ggeScript)
    {
        Debug.Log("send obj zone");
        if (ggeScript.gge.GetType().ToString() == "Card")
        {
            if (ggeScript.gameObject.transform.childCount > 0)
            {
                // Envío el padre.
                Network.SendAGGEToPlayer(ggeScript.gge, pToZone);
                AGGEScript childObject;
                childObject = ggeScript;
                do
                {
                    // Envío los hijos.
                    childObject = childObject.gameObject.transform.GetChild(0).GetComponent<AGGEScript>();
                    Network.SendAGGEToPlayer(childObject.gge, pToZone);
                } while (childObject.gameObject.transform.childCount > 0);
            }
            else
            {
                Network.SendAGGEToPlayer(ggeScript.gge, pToZone);
            }
        }
        else
        {
            Network.SendAGGEToPlayer(ggeScript.gge, pToZone);
        }
        // Destroy the menus and the object
        Destroy(ggeScript.gameObject);
        try
        {
            ggeScript.DestroyMenu();
        }
        catch (NullReferenceException) { Debug.Log("no menu duh");}
    }

    void Update()
    {
        /*
        Vector3 position = Camera.main.ScreenToWorldPoint(UIPanel.transform.position);
        position.y = 0;
        this.gameObject.transform.position = position;
        */

        if (session.FindPlayer(this.playerNum) == null)
        {
            ConsoleScript.AddConsoleOutput(pToZone.name+" disconnected.");
            // Auto destroy.
            Destroy(this.gameObject);
        }

        // GGEToPlayerZone es un diccionario con num de jugador y gge, la zona compara si el num de jug coincide y lo agrega.
        if (session.GetGGEToPlayerZone().Count > 0)
        {
            foreach (var value in session.GetGGEToPlayerZone())
            {
                if (value.Value == playerNum)
                {
                    //Spawn object
                    this.GGEZoneToSpawn.Add(value.Key);
                }
            }
        }

        if (this.GGEZoneToSpawn.Count > 0)
        {
            foreach (AGenericGameElement g in this.GGEZoneToSpawn)
            {
                session.RemoveGGEToPlayerZone(g);
                GameObjectGenerator.SpawnObject(g, Camera.main.ScreenToWorldPoint(this.gameObject.transform.position));
                #if (!UNITY_EDITOR && (UNITY_IPHONE || UNITY_ANDROID))
                    Handheld.Vibrate();   
                #endif
            }
            this.GGEZoneToSpawn.Clear();
        }
    
        //session.
        /*
        if(gge!=null)
            if (shrinked)
            {
                gge.transform.localScale -= Vector3.one * Time.deltaTime * shrinkSpeed;
                if (gge.transform.localScale.y < targetScale)
                { 
                    shrinked = false;
                    sent = false;
                    Destroy(gge);
                }
            }
            */
    }

    // Fancy crap.
    public void OnPointerEnter(PointerEventData eventData)
    {
        if (AGameEnviroment.ObjBeingTouched != null)
        {
            ToPlayerText.gameObject.SetActive(true);
            this.GetComponent<RectTransform>().localScale = new Vector3(1,1,1);
            Color c = Color.white;
            c.a = 0.5f;
            this.GetComponent<Image>().color = c;
            ToPlayerText.color = Color.black;
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        StartCoroutine(HideZone());
    }

    public IEnumerator HideZone()
    {
        yield return new WaitForEndOfFrame();
        ToPlayerText.gameObject.SetActive(false);
        this.GetComponent<RectTransform>().localScale = initialScale;
        Color c = Color.white;
        c.a = 0.25f;
        this.GetComponent<Image>().color = c;
        yield break;
    }
}
