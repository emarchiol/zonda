﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Collections;

public class ConsoleScript : MonoBehaviour {

    static public GameObject SingleOutput;
    static public GameObject ContentPanel;
    static public List<string> ConsoleOutputs = new List<string>();
    static public List<GameObject> Messages = new List<GameObject>();

    // Use this for initialization
    void Start ()
    {
        try
        {
            ConsoleScript.ContentPanel = GameObject.Find("ConsoleContent");
            ConsoleScript.SingleOutput = Resources.Load("Prefab/ConsoleOutput") as GameObject;
            
            // Limpia la lista de objetos porqué si algún componente se ejecuta antes que este con Awake o Start agrega mensajes duplicados.
            foreach (GameObject g in ConsoleScript.Messages)
            {
                Destroy(g);
            }
            ConsoleScript.Messages.Clear();

            foreach (string s in ConsoleScript.ConsoleOutputs)
            {
                var clone = Instantiate(SingleOutput);
                clone.GetComponent<ConsoleElementScript>().SetAttributes(s);

                clone.GetComponent<RectTransform>().SetParent(ConsoleScript.ContentPanel.transform);
                clone.transform.localScale = new Vector3(1, 1, 1);
                ConsoleScript.Messages.Add(clone);
            }
        }
        catch (Exception)
        {
            Debug.Log("No encontré la consola o no pude cargar el prefab consolecontent");
        }
    }

    static public void AddConsoleOutput(string message)
    {
        if (message != "")
        { 
            try
            {
                ConsoleScript.ContentPanel = GameObject.Find("ConsoleContent");
                ConsoleScript.SingleOutput = Resources.Load("Prefab/ConsoleOutput") as GameObject;
                ConsoleScript.ConsoleOutputs.Add(message);

                var clone = Instantiate(ConsoleScript.SingleOutput);
                clone.GetComponent<ConsoleElementScript>().SetAttributes(message);
                clone.GetComponent<RectTransform>().SetParent(ConsoleScript.ContentPanel.transform);
                clone.transform.localScale = new Vector3(1, 1, 1);

                ConsoleScript.Messages.Add(clone);
            }
            catch (Exception)
            {
                //Debug.Log("No encontré la consola o no pude cargar el prefab ConsoleOutput");
            }
        }
    }

}
