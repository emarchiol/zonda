﻿using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Collections;
using System;
using System.Security.Cryptography;
using UnityEngine.SceneManagement;

public class BrowserElementScript : MonoBehaviour {

    public Button GameButton;
    public Button DeleteGameButton;
    public Text GameName;
    public Text Version;
    public Text DownloadProgress;
    string gamePath;

    WWW www;

    public void DeleteGame()
    {
        ZondaConfigurationScript.RemoveGameFromXML(Path.GetFileNameWithoutExtension(gamePath));
        if(Directory.Exists(ZondaConfigurationScript.TempPath + Path.GetFileNameWithoutExtension(gamePath)))
            Directory.Delete(ZondaConfigurationScript.TempPath + Path.GetFileNameWithoutExtension(gamePath), true);

        File.Delete(gamePath);
        Destroy(this.gameObject);
    }

    public void SetAttributes(string path, string gameName, string author, string version, string size)
    {
        this.gamePath = path;
        this.GameName.text = Path.GetFileName(gameName);
        if (version != "")
            version = "v." + version;

        if (size != "")
        {
            if(version != "")
                size = "-"+size + "MB";
            else
                size = size + "MB";
        }

        if (author != "")
        {
            if (version != "" || size != "")
                author = "-" + author;
        }
        this.Version.text = version + size +  author;

        // Empieza una coroutine para poder mostrar el mensaje de la consola esperando a WaitForFixedUpdate.
        this.GetComponentInChildren<Button>().onClick.AddListener(() => { StartCoroutine(BrowserElementScript.Decompress(path)); });
    }

    // Cuando se descomprime un archivo se crea una carpeta con el mismo nombre del archivo (incluida la extensión), de tal manera el archivo puede ser .zon 
    // o .zip teniendo el mismo nombre. Dentro de esa carpeta está el paquete descomprimido, el framework se encargará de buscar el archivo core.xml y desde
    // ahí cargar app y players. (De esta manera, si creamos una copia de un juego .zip o .zon seguirá siendo valido)

    public static IEnumerator Decompress(string path)
    {
        // Creo un CollectionGenerator que se encargará de revisar los XML.
        CollectionGenerator cg;
        string tempDir = ZondaConfigurationScript.TempPath;
        string gameTempDir = tempDir + Path.GetFileName(path);

        // Si el juego ya está descomprimido sigo.
        if (!Directory.Exists(gameTempDir))
        {
            ConsoleScript.AddConsoleOutput("Extracting package, please wait...");
            // Para que aparezca el mensaje en la consola antes de empezar la extracción.
            yield return new WaitForFixedUpdate();
            
            Directory.CreateDirectory(gameTempDir);
            ZipUtil.Unzip(path, gameTempDir);
        }

        Game session = Game.NewInstance();
        session.GamePath = gameTempDir + "/";
        session.GamePathPacket = path;

        try
        {
            cg = new CollectionGenerator();
            // Game packet verfication.
            cg.CheckGameFormat();
            
            MD5 md5File = MD5.Create();
            var stream = File.OpenRead(path);
            session.PlayerInfo.GameMd5 = BitConverter.ToString(md5File.ComputeHash(stream)).Replace("-", "").ToLower();
            session.GameFileName = Path.GetFileName(path);

            // Terminamos de cargar los objetos y nos vamos a la scene de cliente-servidor.
            SceneManager.LoadScene(2);
        }
        catch (InvalidGameFormatException)
        {
            SceneManager.LoadScene(0);
        }
        yield return new WaitForEndOfFrame();
    }

    public void SetInternetAttribute(string name, string fileName, string version, string size, string author)
    {
        this.DeleteGameButton.gameObject.SetActive(false);
        this.GameName.text = Path.GetFileName(name);
        if (version != "")
            version = "v." + version;
        if (size != "")
        {
            if (version != "")
                size = " - " + size + " MB";
            else
                size = size + " MB";
        }
        if (author != "")
        {
            if (version != "" || size != "")
                author = " - " + author;
        }
        this.Version.text = version + size + author;

        // Empieza una coroutine para poder mostrar el mensaje de la consola esperando a WaitForFixedUpdate.
        this.GetComponentInChildren<Button>().onClick.AddListener(() => { StartCoroutine(DownloadGame(InternetGameBrowserScript.url+fileName, fileName)); }) ;
    }

    bool downloading = false;
    bool aborted = false;

    public IEnumerator DownloadGame(string url, string fileName)
    {
        if (!downloading)
        {
            aborted = false;
            downloading = true;
            url = url + "?t=" + DateTime.Now.Second + "" + DateTime.Now.Millisecond;
            Debug.Log("downloading:" + url);
            ConsoleScript.AddConsoleOutput("Downloading " + fileName + "...");
            DownloadProgress.gameObject.SetActive(true);
            yield return new WaitForFixedUpdate();

            www = new WWW(url);
            if (www == null)
                Debug.Log("www es null");
            try
            {
                if (!www.isDone)
                    StartCoroutine(Progress(www));
            }
            catch (NullReferenceException) { }
            yield return www;
            downloading = false;
            if(!aborted)
                if (www.error != null)
                {
                    ConsoleScript.AddConsoleOutput(fileName+ " download failed");
                    DownloadProgress.gameObject.SetActive(false);
                }

                else
                {
                    ConsoleScript.AddConsoleOutput(fileName + " completed.");
                    File.WriteAllBytes(Application.persistentDataPath + "/Games/" + fileName, www.bytes);
                }
        }
        else
        {
            aborted = true;
            downloading = false;
            www.Dispose();
            ConsoleScript.AddConsoleOutput(fileName+" download aborted by user");
            DownloadProgress.gameObject.SetActive(false);
            yield return new WaitForFixedUpdate();
            www = null;
        }
    }
    
    public IEnumerator Progress(WWW www)
    {
        while (downloading && www!=null)
        {
            DownloadProgress.text = ".";
            yield return new WaitForSeconds(0.5f);
            DownloadProgress.text = "..";
            yield return new WaitForSeconds(0.5f);
            DownloadProgress.text = "...";
            yield return new WaitForSeconds(0.5f);
        }
        if(!downloading)
        { 
            DownloadProgress.text = "Completed";
            this.GameButton.interactable = false;
        }

    }

    /*
    public IEnumerator CancelDownload()
    {
        
        if (downloading && www!=null)
        {
            aborted = true;
            downloading = false;

            www.Dispose();
            ConsoleScript.AddConsoleOutput("Download aborted by user");
            DownloadProgress.gameObject.SetActive(false);
            yield return new WaitForFixedUpdate();
            www = null;
        }
    }*/
}
