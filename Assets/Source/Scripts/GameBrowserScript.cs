﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Linq;
using System.IO;
using System.Security.Cryptography;
using System;
using System.Xml;

public class GameBrowserScript : MonoBehaviour {

    public GameObject BrowserElement;
    public GameObject ContentPanel;
    public List<string> ConsoleOutputs = new List<string>();
    public List<GameObject> Games = new List<GameObject>();
    List<string> GamesXML = new List<string>();
    string[] GamesFolderFiles;
    private string GamesDirectoryPath;
    public Text LoadingText;

    void Start()
    {
        GamesFolderFiles = Directory.GetFiles(ZondaConfigurationScript.GamesPath);
        GamesDirectoryPath = ZondaConfigurationScript.GamesPath;
        LoadGamesXML();
        // Si la carpeta tiene algún juego nuevo.
        foreach (string gameFileWithPath in GamesFolderFiles)
        {
            if (!GamesXML.Contains(Path.GetFileName(gameFileWithPath)))
            {
                bool addGame = true;
                // Verifico que no sea el mismo juego con otro nombre (agregado manualmente).
                string md5FileName;
                using (var md5 = MD5.Create())
                {
                    var stream = File.OpenRead(gameFileWithPath);
                    md5FileName = BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", "").ToLower() + ".zip";
                    stream.Close();
                }
                foreach (string xmlGame in GamesXML)
                {
                    if (xmlGame == md5FileName)
                        addGame = false;
                }

                // Agrego el juego nuevo al XML
                if (addGame)
                {
                    ZondaConfigurationScript.AddGameToXML(gameFileWithPath);
                }
                else
                {
                    File.Delete(gameFileWithPath);
                }
            }
            if (Directory.Exists(ZondaConfigurationScript.ConfigTempPath))
                Directory.Delete(ZondaConfigurationScript.ConfigTempPath, true);
        }

        // Si el XML tiene una referencia que ya no existe
        foreach (string xmlGame in GamesXML)
        {
            if (!File.Exists(GamesDirectoryPath + xmlGame))
            {
                // Borrar entrada XML.
                ZondaConfigurationScript.RemoveGameFromXML(Path.GetFileNameWithoutExtension(xmlGame));
            }
        }
        this.LoadingText.gameObject.SetActive(false);

        // Cargo la lista de los juegos...
        if (Directory.GetFiles(ZondaConfigurationScript.GamesPath).Count() > 0)
        {
            XElement xele = XElement.Load(ZondaConfigurationScript.XmlList);
            var query = from gameList in xele.Elements()
                        select new
                        {
                            gameName = gameList.Attribute("name").Value,
                            author = gameList.Attribute("author").Value,
                            md5 = gameList.Attribute("md5").Value,
                            version = gameList.Attribute("version").Value,
                            size = gameList.Attribute("size").Value
                        };

            foreach (var r in query)
            {
                var clone = Instantiate(BrowserElement);
                clone.GetComponent<BrowserElementScript>().SetAttributes(GamesDirectoryPath + r.md5 + ".zip", r.gameName, r.author, r.version, r.size);
                clone.GetComponent<RectTransform>().SetParent(ContentPanel.transform);
                clone.transform.localScale = new Vector3(1, 1, 1);
                this.Games.Add(clone);
            }
        }
        else
        {
            // No games message.
        }
    }

    void LoadGamesXML()
    {
        // Cargo la lista de los juegos en el XML.
        this.GamesDirectoryPath = ZondaConfigurationScript.GamesPath;
        try
        {
            XElement xele = XElement.Load(ZondaConfigurationScript.XmlList);
            var query = from gameList in xele.Elements()
                        select new
                        {
                            md5 = gameList.Attribute("md5").Value,
                        };

            foreach (var r in query)
            {
                this.GamesXML.Add(r.md5 + ".zip");
            }
        }
        catch (XmlException)
        {
            // No items in GamesXML
        }
    }
}
