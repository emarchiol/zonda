﻿using UnityEngine;
using System.Collections.Generic;
using System.Xml.Linq;
using UnityEngine.UI;
using System.Linq;
using System.Xml;
using System.Collections;
using System;

public class InternetGameBrowserScript : MonoBehaviour
{
    public GameObject BrowserElement;
    public GameObject ContentPanel;
    public List<string> ConsoleOutputs = new List<string>();
    //public List<string> DownloadedGames = new List<string>();
    public List<GameObject> Games = new List<GameObject>();
    public Text ServerError;
    public static string url = "http://ertai.com.ar/tra/ZondaGames/";

    // Use this for initialization
    IEnumerator Start()
    {
        // Descargar el list.xml y cargarlo como elementos
        WWW www;

        ServerError.text = "Waiting for server...";
        ServerError.gameObject.SetActive(true);
        www = new WWW(url + "list.xml" + "?t=" + DateTime.Now.Second + "" + DateTime.Now.Millisecond);
        yield return www;
        ServerError.gameObject.SetActive(false);

        try
        {
            XElement xele = XElement.Parse(www.text);
            var query = from gameList in xele.Elements()
                        select new
                        {
                            gameName = gameList.Element("name").Value,
                            fileName = gameList.Element("file").Value,
                            md5 = gameList.Element("md5").Value,
                            version = gameList.Element("version").Value,
                            size = gameList.Element("size").Value,
                            author = gameList.Element("author").Value
                        };

            foreach (var r in query)
            {
                // Preguntar si el juego que estoy agregando ya existe en mi games.xml, si existe, desactivar el botón.

                var clone = Instantiate(BrowserElement);
                clone.GetComponent<BrowserElementScript>().SetInternetAttribute(r.gameName, r.fileName, r.version, r.size, r.author);
                clone.GetComponent<RectTransform>().SetParent(ContentPanel.transform);
                clone.transform.localScale = new Vector3(1, 1, 1);
                this.Games.Add(clone);
                clone.GetComponentInChildren<Button>().interactable = !ZondaConfigurationScript.CheckGameInXML(r.md5);
            }
        }
        catch (XmlException e)
        {

            Debug.Log(e);
            ServerError.gameObject.SetActive(true);
            ServerError.text = "No internet connection or server problem, try again later.";
        }

        

        /*
        this.directoryPath = Application.persistentDataPath + "/Games";
        
        foreach (String game in Directory.GetFiles(directoryPath))
        {
            if (Path.GetExtension(game).CompareTo(".zon") == 0 || Path.GetExtension(game).CompareTo(".zip") == 0)
            {
                var clone = Instantiate(BrowserElement);
                clone.GetComponent<BrowserElementScript>().SetAttributes(game);

                clone.GetComponent<RectTransform>().SetParent(ContentPanel.transform);
                clone.transform.localScale = new Vector3(1, 1, 1);
                this.Games.Add(clone);
            }
        }
        */
    }
}

