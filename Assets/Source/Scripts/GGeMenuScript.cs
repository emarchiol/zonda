﻿using UnityEngine;
using UnityEngine.UI;
using System.Reflection;

public class GGeMenuScript : MonoBehaviour {

    public Player PlayerToSend { get; set; }
    public Button Title;
    public GameObject MenuPrefab;
    public GameObject SimpleMenuPrefab;
    GameObject Menu;
    GameObject SimpleMenu;
    GameObject Canv;
    Game session = Game.NewInstance();
    AGGEScript AGGE;
    Camera cam;
    float initialTimeSimpleMenuOpen = 1.0f;
    float timeSimpleMenuOpen;
    int buttonSize = 40;
    
    void Start()
    {
        this.MenuPrefab = (GameObject)Resources.Load("Prefab/GGEMenu");
        this.SimpleMenuPrefab = (GameObject)Resources.Load("Prefab/GGESimpleMenu");
        Canv = GameObject.Find("Canvas");// MMMMMMmmmmmmm.
        cam = Camera.main;
    }

    public void SendGGEToPlayer(GameObject Menu, AGGEScript ggeScript, Player p)
    {
        ggeScript.menuOpen = false;
        //Debug.Log("SendGgeToPlayer:" + PlayerToSend.ipAddress);
        Debug.Log("SendGgeToPlayer:" + p.ipAddress);
        Debug.Log("ggeScript.gge:" + ggeScript.gge.Name);

        if (ggeScript.gge.GetType().ToString() == "Card")
        {
            if (ggeScript.gameObject.transform.childCount > 0)
            {
                // Envío el padre.
                Network.SendAGGEToPlayer(ggeScript.gge, p);
                AGGEScript childObject;
                childObject = ggeScript;
                do {
                    // Envío los hijos.
                    childObject = childObject.gameObject.transform.GetChild(0).GetComponent<AGGEScript>();
                    Network.SendAGGEToPlayer(childObject.gge, p);
                } while (childObject.gameObject.transform.childCount > 0);
            }
            else
            { 
                Network.SendAGGEToPlayer(ggeScript.gge, p);
            }
        }
        else
        { 
            Network.SendAGGEToPlayer(ggeScript.gge, p);
        }
        DestroyMenu();
        Destroy(ggeScript.gameObject);
    }

    // Este va en aggescript
    public Button AddButtonToMenu(Button ButtonToClone, GameObject Menu, string caption)
    {
        Button ButtonToAdd = Instantiate(ButtonToClone);
        ButtonToAdd.transform.SetParent(Menu.transform.GetChild(1));
        ButtonToAdd.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        ButtonToAdd.name = caption;
        ButtonToAdd.GetComponentInChildren<Text>().text = caption;
        return ButtonToAdd;
    }

    // Crea el menu con los metodos correspondiente del objeto al cual se hizo touch&hold esto va en aggescript.
    public void CreateMenu(AGGEScript AGGE)
    {
        Destroy(SimpleMenu);
        if (Menu != null)
            DestroyMenu();
        this.AGGE = AGGE;
        //GameObject ggePrefab = ggeScript.GetComponent<AGGEScript>().GetPrefabClone();
        //Creamos el nuevo menu a partir del prefab, lo asigno al canvas
        Menu = Instantiate(MenuPrefab);
        this.Title = this.Menu.transform.GetChild(0).gameObject.GetComponent<Button>();
        Menu.GetComponent<RectTransform>().SetParent(Canv.transform, false);

        // Send to player buttons.
        foreach (Player p in session.GetPlayersList())
        {
            Button ToPlayer = AddButtonToMenu(Title, Menu, "To " + p.name);
            //ToPlayer.GetComponent<GGeMenuScript>().PlayerToSend = p;

            MethodInfo mf = this.GetType().GetMethod("SendGGEToPlayer");// ToPlayer.GetComponent<GGeMenuScript>().GetType().GetMethod("SendGGEToPlayer");

            System.Object[] parameters = { Menu, AGGE, p };
            ToPlayer.onClick.AddListener(() => mf.Invoke(this, parameters));
        }

        // Saco los metodos públicos de ggeScript.
        MethodInfo[] methodInfos = AGGE.GetType().GetMethods(BindingFlags.Public | BindingFlags.Instance);
        // Recorro cada metodo, si tiene el atributo agrego un boton.
        foreach (MethodInfo methodInfo in methodInfos)
        {
            var attributes = methodInfo.GetCustomAttributes(typeof(MenuItemAttribute), true);
            if (attributes != null && attributes.Length > 0)
            {
                MethodInfo mf = methodInfo;
                // Creo un boton por cada metodo.
                Button instance = AddButtonToMenu(Title, Menu, mf.Name);
                // Asigno el metodo al evento OnClick del boton.
                instance.onClick.AddListener(() => { mf.Invoke(AGGE, null); DestroyMenu(); });
            }
        }

        // Botón close que destruye el prefab.
        Button CloseButton = AddButtonToMenu(Title, Menu, "Close");
        CloseButton.onClick.AddListener(() => this.DestroyMenu());

        //Nombre del menu segun que objeto es y FancyCrap ESTO ES HORRIBLE ARREGLAR
        Title.interactable = false;
        Title.GetComponentInChildren<Text>().text = AGGE.GetName();
        //Es carta?
        if (this.GetComponent<CardScript>() != null)
        {
            //Está de frente ?
            if (AGGE.GetComponent<CardScript>().isFront())
                Title.GetComponentInChildren<Text>().text = AGGE.GetComponent<CardScript>().card.Name + "(Card)";
        }
        if (AGGE.GetComponent<CardDeckScript>() != null)
        {
            Title.GetComponentInChildren<Text>().text = AGGE.GetComponent<CardDeckScript>().deck.Name + "(Deck)";
        }
        
    }

    public void OpenSimpleMenu(AGGEScript AGGElement)
    {
        if (this.SimpleMenu == null)
        { 
            timeSimpleMenuOpen = initialTimeSimpleMenuOpen;
            this.AGGE = AGGElement;
            this.AGGE.simpleMenuOpen = true;
            this.AGGE.SimpleMenu = Instantiate(SimpleMenuPrefab);
            this.SimpleMenu = this.AGGE.SimpleMenu;
        
            // Tamaño de los botones simpleMenu según el tamaño de la cámara y asignación de OnClick.
            this.SimpleMenu.transform.GetChild(0).GetComponent<Button>().transform.localScale = new Vector3(buttonSize / cam.orthographicSize, buttonSize / cam.orthographicSize, 1);
            this.SimpleMenu.transform.GetChild(0).GetComponent<Button>().onClick.AddListener(() => CreateMenu(this.AGGE));
            this.SimpleMenu.transform.GetChild(1).GetComponent<Button>().transform.localScale = new Vector3(buttonSize / cam.orthographicSize, buttonSize / cam.orthographicSize, 1);
            this.SimpleMenu.transform.GetChild(1).GetComponent<Button>().onClick.AddListener(() => RotateButton(1));
            this.SimpleMenu.transform.GetChild(2).GetComponent<Button>().transform.localScale = new Vector3(buttonSize / cam.orthographicSize, buttonSize / cam.orthographicSize, 1);
            this.SimpleMenu.transform.GetChild(2).GetComponent<Button>().onClick.AddListener(() => RotateButton(-1));

            if (AGGElement.gge.GetType().ToString() == "Token" || !ZondaConfigurationScript.SimpleMenu)
            {
                this.SimpleMenu.transform.GetChild(1).GetComponent<Button>().gameObject.SetActive(false);
                this.SimpleMenu.transform.GetChild(2).GetComponent<Button>().gameObject.SetActive(false);
            }
        }
    }

    // Rotating the gge, need to check for childs.
    public void RotateButton(int direction)
    {

        if (this.transform.parent.GetComponent<CardScript>() == null)
        {
            if (direction > 0)
            {
                timeSimpleMenuOpen = initialTimeSimpleMenuOpen;
                this.AGGE.transform.eulerAngles = new Vector3(this.AGGE.transform.eulerAngles.x, this.AGGE.transform.eulerAngles.y + 90, this.AGGE.transform.eulerAngles.z);
            }
            else
            {
                timeSimpleMenuOpen = initialTimeSimpleMenuOpen;
                this.AGGE.transform.eulerAngles = new Vector3(this.AGGE.transform.eulerAngles.x, this.AGGE.transform.eulerAngles.y - 90, this.AGGE.transform.eulerAngles.z);
            }
        }
    }

    public void NonCardMenuMovement()
    {
        if (this.AGGE != null)
        {
            float distanceA = 250;
            float distanceB = 250;

            if (AGGE.gge is Token)
            {
                distanceA = 250;
                distanceB = 250;
            }
            if (AGGE.gge is TokenCollection)
            {
                distanceA = 100;
                distanceB = 100;
            }

            // Espera unos segundos y se elimina el prefab solo si no hubo clicks en los botones de rotate.
            if (!this.AGGE.isStatic)
            {
                if (this.SimpleMenu != null)
                {
                    // Adjuste del menú según zoom de la cámara.
                    this.SimpleMenu.transform.GetChild(0).GetComponent<Button>().transform.localScale = new Vector3(buttonSize / cam.orthographicSize, buttonSize / cam.orthographicSize, 1);
                    this.SimpleMenu.transform.GetChild(1).GetComponent<Button>().transform.localScale = new Vector3(buttonSize / cam.orthographicSize, buttonSize / cam.orthographicSize, 1);
                    this.SimpleMenu.transform.GetChild(2).GetComponent<Button>().transform.localScale = new Vector3(buttonSize / cam.orthographicSize, buttonSize / cam.orthographicSize, 1);

                    if (this.SimpleMenu.transform.parent != Canv.transform)
                        this.SimpleMenu.transform.SetParent(Canv.transform, false);

                    // Tiempo restante para destruir simpleMenu.
                    if (this.AGGE.simpleMenuOpen)
                    {
                        timeSimpleMenuOpen -= Time.deltaTime;
                        if (timeSimpleMenuOpen < 0)
                        {
                            Destroy(this.SimpleMenu);
                            this.AGGE.simpleMenuOpen = false;
                        }
                    }

                    // Posición de los botones según el objeto 3D.
                    float offsetMagnitudeX;
                    float offsetMagnitudeY;
                    Vector3 offset;
                    // Si angulo 0 180 360
                    if ((((int)this.AGGE.transform.localRotation.eulerAngles.y / 90) % 2) == 0)
                    {
                        offsetMagnitudeX = (float)((this.AGGE.gameObject.transform.localScale.x * distanceA) / 100); // Menu button.
                        offsetMagnitudeY = (float)((this.AGGE.gameObject.transform.localScale.z * distanceB) / 100); // +90 -90 buttons.

                        offset = new Vector3(0, 0, offsetMagnitudeY);
                        // Position of the menu button
                        this.SimpleMenu.transform.GetChild(0).transform.position = Camera.main.WorldToScreenPoint(this.AGGE.gameObject.transform.position + offset);
                        offset = new Vector2(offsetMagnitudeX, 0);
                        // Position of +90 rotation
                        this.SimpleMenu.transform.GetChild(1).transform.position = Camera.main.WorldToScreenPoint(this.AGGE.gameObject.transform.position + offset);
                        offset = new Vector2(-offsetMagnitudeX, 0);
                        // Position of -90 rotation
                        this.SimpleMenu.transform.GetChild(2).transform.position = Camera.main.WorldToScreenPoint(this.AGGE.gameObject.transform.position + offset);
                    }
                    // Si angulo es 90 270
                    else if ((((int)this.AGGE.transform.localRotation.eulerAngles.y / 90) % 2) != 0)
                    {
                        offsetMagnitudeX = (float)((this.AGGE.gameObject.transform.localScale.x * distanceB) / 100); // Menu button.
                        offsetMagnitudeY = (float)((this.AGGE.gameObject.transform.localScale.z * distanceA) / 100); // +90 -90 buttons.

                        offset = new Vector3(0, 0, offsetMagnitudeX);
                        // Position of the menu button
                        this.SimpleMenu.transform.GetChild(0).transform.position = Camera.main.WorldToScreenPoint(this.AGGE.gameObject.transform.position + offset);
                        offset = new Vector2(offsetMagnitudeY, 0);
                        // Position of +90 rotation
                        this.SimpleMenu.transform.GetChild(1).transform.position = Camera.main.WorldToScreenPoint(this.AGGE.gameObject.transform.position + offset);
                        offset = new Vector2(-offsetMagnitudeY, 0);
                        // Position of -90 rotation
                        this.SimpleMenu.transform.GetChild(2).transform.position = Camera.main.WorldToScreenPoint(this.AGGE.gameObject.transform.position + offset);
                    }
                }
            }
        }
    }

    public void CardMenuMovement()
    {
        if (this.AGGE != null)
        {
            // Espera unos segundos y se elimina el prefab solo si no hubo clicks en los botones de rotate.
            if (!this.AGGE.isStatic)
            {
                if (this.SimpleMenu != null)
                {
                    // Adjuste del menú según zoom de la cámara.
                    this.SimpleMenu.transform.GetChild(0).GetComponent<Button>().transform.localScale = new Vector3(buttonSize / cam.orthographicSize, buttonSize / cam.orthographicSize, 1);
                    this.SimpleMenu.transform.GetChild(1).GetComponent<Button>().transform.localScale = new Vector3(buttonSize / cam.orthographicSize, buttonSize / cam.orthographicSize, 1);
                    this.SimpleMenu.transform.GetChild(2).GetComponent<Button>().transform.localScale = new Vector3(buttonSize / cam.orthographicSize, buttonSize / cam.orthographicSize, 1);
                    if (this.transform.parent.GetComponent<CardScript>() != null)
                    {
                        this.SimpleMenu.transform.GetChild(1).GetChild(1).GetComponent<Image>().color = Color.grey;
                        this.SimpleMenu.transform.GetChild(2).GetChild(1).GetComponent<Image>().color = Color.grey;
                    }
                    if (this.SimpleMenu.transform.parent != Canv.transform)
                        this.SimpleMenu.transform.SetParent(Canv.transform, false);

                    // Tiempo restante para destruir simpleMenu.
                    if (this.AGGE.simpleMenuOpen)
                    {
                        timeSimpleMenuOpen -= Time.deltaTime;
                        if (timeSimpleMenuOpen < 0)
                        {
                            Destroy(this.SimpleMenu);
                            this.AGGE.simpleMenuOpen = false;
                        }
                    }

                    // Posición de los botones según el objeto 3D.
                    float offsetMagnitudeX;
                        float offsetMagnitudeY;
                        Vector3 offset;
                        // Si angulo 0 180 360
                        if ((((int)this.AGGE.transform.localRotation.eulerAngles.y / 90) % 2) == 0)
                        {
                            offsetMagnitudeX = (float)((this.AGGE.gameObject.transform.localScale.x * 800) / 100); // Menu button.
                            offsetMagnitudeY = (float)((this.AGGE.gameObject.transform.localScale.z * 650) / 100); // +90 -90 buttons.

                            offset = new Vector3(0, 0, offsetMagnitudeY);
                            // Position of the menu button
                            this.SimpleMenu.transform.GetChild(0).transform.position = Camera.main.WorldToScreenPoint(this.AGGE.gameObject.transform.position + offset);
                            offset = new Vector2(offsetMagnitudeX, 0);
                            // Position of +90 rotation
                            this.SimpleMenu.transform.GetChild(1).transform.position = Camera.main.WorldToScreenPoint(this.AGGE.gameObject.transform.position + offset);
                            offset = new Vector2(-offsetMagnitudeX, 0);
                            // Position of -90 rotation
                            this.SimpleMenu.transform.GetChild(2).transform.position = Camera.main.WorldToScreenPoint(this.AGGE.gameObject.transform.position + offset);
                        }
                        // Si angulo es 90 270
                        else if ((((int)this.AGGE.transform.localRotation.eulerAngles.y / 90) % 2) != 0)
                        {
                            offsetMagnitudeX = (float)((this.AGGE.gameObject.transform.localScale.x * 650) / 100); // Menu button.
                            offsetMagnitudeY = (float)((this.AGGE.gameObject.transform.localScale.z * 800) / 100); // +90 -90 buttons.

                            offset = new Vector3(0, 0, offsetMagnitudeX);
                            // Position of the menu button
                            this.SimpleMenu.transform.GetChild(0).transform.position = Camera.main.WorldToScreenPoint(this.AGGE.gameObject.transform.position + offset);
                            offset = new Vector2(offsetMagnitudeY, 0);
                            // Position of +90 rotation
                            this.SimpleMenu.transform.GetChild(1).transform.position = Camera.main.WorldToScreenPoint(this.AGGE.gameObject.transform.position + offset);
                            offset = new Vector2(-offsetMagnitudeY, 0);
                            // Position of -90 rotation
                            this.SimpleMenu.transform.GetChild(2).transform.position = Camera.main.WorldToScreenPoint(this.AGGE.gameObject.transform.position + offset);
                        }
                    
                }
            }
        }
    }

    public void Update()
    {
        if (AGGE != null)
        { 
            if (AGGE.gge.GetType().ToString() == "Card" || AGGE.gge.GetType().ToString() == "CardDeck")
                CardMenuMovement();
            else
                NonCardMenuMovement();
        }
    }

    public void DestroySimpleMenu()
    {
        Destroy(SimpleMenu);
    }

    public void DestroyMenu()
    {
        AGGE.menuOpen = false;
        Destroy(SimpleMenu);
        Destroy(Menu);
    }
}
