﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class OptionsScript : MonoBehaviour {

    public Toggle CheckInfoPanel;
    public Toggle CheckConsole;
    public Toggle CheckMenus;

    // Odio esta clase, siempre se falla algo.
    void Start()
    {
        this.CheckInfoPanel.isOn = ZondaConfigurationScript.InfoPanel;
        this.CheckConsole.isOn = ZondaConfigurationScript.ConsoleInGame;
        this.CheckMenus.isOn = ZondaConfigurationScript.SimpleMenu;

        ZondaConfigurationScript.InfoPanel = this.CheckInfoPanel.isOn;
        ZondaConfigurationScript.ConsoleInGame = this.CheckConsole.isOn;
        ZondaConfigurationScript.SimpleMenu = this.CheckMenus.isOn;
    }

    public void OpenCloseOptionsPanel()
    {
        if(this.gameObject.activeSelf)
            this.gameObject.SetActive(false);
        else { 
            this.gameObject.SetActive(true);
        }
    }

    public void DisableEnableInfoPanel()
    {
        ZondaConfigurationScript.InfoPanel = !ZondaConfigurationScript.InfoPanel;
    }
    public void DisableEnableConsoleInGame()
    {
        ZondaConfigurationScript.ConsoleInGame = !ZondaConfigurationScript.ConsoleInGame;
    }
    public void DisableEnableGGESimleMenus()
    {
        ZondaConfigurationScript.SimpleMenu = !ZondaConfigurationScript.SimpleMenu;
    }
}
