﻿using UnityEngine;
using System.Collections;

public class LogScript : MonoBehaviour {

	public GameObject LogPanel;
    public GameObject Menu;
	bool CameraStatus;
    Game session = Game.NewInstance();

	public void CloseLog()
	{
		if(!this.CameraStatus){}
		else
            session.CameraEnabled = true;

        session.GGeTouch = true;
		LogPanel.SetActive(false);
	}

	public void OpenLog()
	{
        Menu.SetActive(false);
		this.CameraStatus = session.CameraEnabled;
		if(session.CameraEnabled)
            session.CameraEnabled = false;
		else{}

        session.GGeTouch = false;
		LogPanel.SetActive(true);
	}
}
