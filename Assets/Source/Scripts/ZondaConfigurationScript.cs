﻿using UnityEngine;
using System.IO;
using System.Xml.Linq;
using System.Xml;
using System.Security.Cryptography;
using System;
using System.Linq;

public class ZondaConfigurationScript : MonoBehaviour {

    // Para que limpiar los temp se ejecute solo una vez cuando la app arranca
    static bool once = true;
    long tempFolderMaxMB = 200;
    public static string GamesPath;
    public static string TempPath;
    public static string ConfigTempPath;
    public static string XmlList;
    public static string SavePath;


    // Options, debería sacarlos de algún archivo, sino valores por defecto.
    public static bool InfoPanel = true;
    public static bool ConsoleInGame = false;
    public static bool SimpleMenu = true;

    void Awake()
    {
        if (Game.GetInstance() != null && Game.GetInstance().GameFileName == null)
        { 
            Debug.Log("cleaning sessions errors");
            Game.CleanSingleton();
        }

        if (Game.GetInstance() != null)
        {
            Debug.Log("from zonda");
            Game.DestroySession();
        }
    }
    // Este script sirve para verificar que exista algo en el path persistentdatapath y opciones del framework
    void Start()
    {
        GamesPath = Application.persistentDataPath + "/games/";
        TempPath = Application.persistentDataPath + "/temp/";
        ConfigTempPath = Application.persistentDataPath + "/cache/";
        SavePath = Application.persistentDataPath + "/saves/";
        XmlList = Application.persistentDataPath + "/games.xml";

        if (!Directory.Exists(GamesPath))
        {
            //Primer arranque
            ConsoleScript.AddConsoleOutput("Configuring application...");
            Directory.CreateDirectory(GamesPath);
        }
        if (!Directory.Exists(TempPath))
        {
            Directory.CreateDirectory(TempPath);
        }
        if (!Directory.Exists(SavePath))
        {
            Directory.CreateDirectory(SavePath);
        }
        if (Directory.Exists(ConfigTempPath))
        {
            Directory.Delete(ConfigTempPath,true);
        }

        // Existe games.xml ? Creo y meto tag games.
        if (!File.Exists(XmlList))
        {
            Debug.Log("creating file");
            XDocument doc = new XDocument(new XElement("Games"));
            doc.Save(XmlList);
        }

        // Si hay archivos en /games.
        if (Directory.GetFiles(GamesPath).Count() > 0)
        {
            XElement gamesTag = XElement.Load(XmlList);
            gamesTag.Save(XmlList);

            Directory.CreateDirectory(ConfigTempPath);
            //ConsoleScript.AddConsoleOutput("Updating games...");
            foreach (string gamePath in Directory.GetFiles(GamesPath))
            {
                AddGameToXML(gamePath);
            }

            // Limpio los temps.
            CleanTemp(true);
        }

        long folderSizeMB = (GetDirectorySize(TempPath) /1024)/ 1024;
        if (once)
        {
            once = false; 
            if(folderSizeMB > tempFolderMaxMB)
            {
                CleanTemp(false);
            }
        }
    }

    public static bool AddGameToXML(string gamePath)
    {
        // Creo el documento xml con tags principales.
        XElement gamesTag = XElement.Load(XmlList);
        XElement game;
        string MD5fileName;
        FileInfo fileSize;
        CollectionGenerator cg;

        // Calculo el MD5 del archivo.
        using (var md5 = MD5.Create())
        {
            var stream = File.OpenRead(gamePath);
            MD5fileName = BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", "").ToLower() + ".zip";
            stream.Close();
        }

        //Debug.Log(Path.GetFileNameWithoutExtension(MD5fileName));
        // Ver si el juego ya existe.
        if (CheckGameInXML(Path.GetFileNameWithoutExtension(MD5fileName)))
        {
            return false;
        }
        else
        {
            // Renombro.
            MD5fileName = Path.GetDirectoryName(gamePath) + "/" + MD5fileName;

            /*File.Copy(gamePath, MD5fileName);
            File.Delete(gamePath);*/
            File.Move(gamePath, MD5fileName);
            DirectoryInfo d = Directory.CreateDirectory(ConfigTempPath + Path.GetFileName(MD5fileName));

            // Descomprimo el juego y leo lo básico del core.
            try
            {
                ZipUtil.Unzip(MD5fileName, d.FullName + "/");
            }
            catch (Exception)
            {
                Debug.Log("Corrupted file, deleting...");
                File.Delete(MD5fileName);
                ZondaConfigurationScript.RemoveGameFromXML(Path.GetFileNameWithoutExtension(MD5fileName));
                return false;
            }

            // Verifico que el juego tenga los datos minimos para ser agregado al xml (archivo core y nombre del juego).
            try
            {
                cg = new CollectionGenerator(d.FullName + "/");
                game = new XElement("Game");
                fileSize = new FileInfo(MD5fileName);

                XAttribute name = new XAttribute("name", cg.gameName);
                XAttribute version = new XAttribute("version", cg.gameVersion);
                XAttribute author = new XAttribute("author", cg.gameAuthor);
                XAttribute size = new XAttribute("size", (((float)(fileSize.Length / 1024) / 1024)).ToString("0.00"));
                XAttribute md5 = new XAttribute("md5", Path.GetFileNameWithoutExtension(MD5fileName));

                game.Add(name);
                game.Add(version);
                game.Add(author);
                game.Add(size);
                game.Add(md5);
                gamesTag.Add(game);
                gamesTag.Save(XmlList);
                return true;
            }
            catch (InvalidGameFormatException)
            {
                Debug.Log("Game not valid.");
                return false;
            }
        }
        
    }

    public static void RemoveGameFromXML(string md5)
    {
        XElement gamesTag = XElement.Load(XmlList);
        gamesTag.Descendants("Game").Where(e => e.Attribute("md5").Value.Equals(md5)).Remove();
        gamesTag.Save(XmlList);
    }

    public static bool CheckGameInXML(string md5)
    {
        try
        {
            XElement gamesTag = XElement.Load(XmlList);
            if (gamesTag.Descendants("Game").Where(e => e.Attribute("md5").Value.Equals(md5)).Count() > 0)
            { 
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (XmlException)
        {
            Debug.Log("checkgame exception");
            return false;
        }
    }

    public static void CleanTemp(bool cacheOnly)
    {
        if (!cacheOnly)
        {
            ConsoleScript.AddConsoleOutput("Cleaning temp files...");
            Directory.Delete(TempPath, true);
            Directory.CreateDirectory(TempPath);
        }
        if (Directory.Exists(ConfigTempPath))
            Directory.Delete(ConfigTempPath, true);
    }

    static long GetDirectorySize(string p)
    {
        // 1.
        // Get array of all file names.
        string[] a = Directory.GetFiles(p, "*.*",SearchOption.AllDirectories);

        // 2.
        // Calculate total bytes of all files in a loop.
        long b = 0;
        foreach (string name in a)
        {
            // 3.
            // Use FileInfo to get length of each file.
            FileInfo info = new FileInfo(name);
            b += info.Length;
        }
        // 4.
        // Return total size
        return b;
    }
}
