﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class InfoPanelScript : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public GameObject Content;
    public GameObject InformationPanel;
    public GameObject InfoElement;
    public Text Title;
    AGenericGameElement GGe;
    bool itemAdded = false;

    public void SetGGe(AGGEScript aGGE)
    {
        //Limpio la lista
        foreach (Transform child in Content.transform)
        {
            Destroy(child.gameObject);
        }
        GGe = aGGE.gge;

        // En caso de que sea carta no puedo mostrar información si está boca abajo.
        if (GGe.GetType().ToString() == "Card")
        {
            CardScript c = (CardScript)aGGE;
            if (c.card.isFlipped)
            {
                this.Title.text = this.GGe.Name + " " + "(" + this.GGe.GetType().ToString() + ")";
                if(this.GGe.ggeAttr.Count>0)
                    foreach (Attr info in this.GGe.ggeAttr)
                    {
                        var clone = Instantiate(InfoElement);
                        clone.GetComponent<InfoElementScript>().SetAttributes(info.title, info.description);
                        clone.GetComponent<RectTransform>().SetParent(this.Content.transform);
                        clone.transform.localScale = new Vector3(1, 1, 1);
                        itemAdded = true;
                    }
            }
            else
            {
                this.Title.text = "(" + this.GGe.GetType().ToString() + ")";
                var clone = Instantiate(InfoElement);
                clone.GetComponent<InfoElementScript>().SetAttributes("Unknow", "This item is not yet revealed.");
                clone.GetComponent<RectTransform>().SetParent(this.Content.transform);
                clone.transform.localScale = new Vector3(1, 1, 1);
                itemAdded = true;
            }
        }
        else
        {
            this.Title.text = this.GGe.Name + " " + "(" + this.GGe.GetType().ToString() + ")";
            if (this.GGe.ggeAttr.Count>0)
            foreach (Attr info in this.GGe.ggeAttr)
            {
                var clone = Instantiate(InfoElement);
                clone.GetComponent<InfoElementScript>().SetAttributes(info.title, info.description);
                clone.GetComponent<RectTransform>().SetParent(this.Content.transform);
                clone.transform.localScale = new Vector3(1, 1, 1);
                itemAdded = true;
            }
        }

        if (!itemAdded)
        {
            this.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(this.gameObject.GetComponent<RectTransform>().sizeDelta.x, 35);
            this.InformationPanel.GetComponent<RectTransform>().sizeDelta = new Vector2(this.InformationPanel.GetComponent<RectTransform>().sizeDelta.x, 0);
        }
        else
        {
            itemAdded = false;
            this.gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(this.gameObject.GetComponent<RectTransform>().sizeDelta.x, 117);
            this.InformationPanel.GetComponent<RectTransform>().sizeDelta = new Vector2(this.InformationPanel.GetComponent<RectTransform>().sizeDelta.x, 87);
        }   
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if(AGameEnviroment.ObjBeingTouched != null)
        {
            this.gameObject.SetActive(false);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        this.gameObject.SetActive(true);
    }
}
