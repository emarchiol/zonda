﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.EventSystems;

public class SideBarScript : MonoBehaviour, IDropHandler, IObserver
{
    public GameObject Sidebar;
    public GameObject SideBarGGE;
    private Game session = Game.NewInstance();
    public RectTransform parentConainer;
    private List<SideBarElementScript> SideBarPrefabs;
    //public static List<AGenericGameElement> SpawnedObjectsOnSidebar { get; set; }
    bool ShowSideBar = true;
    public GameObject ButtonShowSideBar;
    public GameObject[] ToPlayerZones;

    void Awake()
    {
        ShowHideSidebar();
        SideBarPrefabs = new List<SideBarElementScript>();
        //SpawnedObjectsOnSidebar = new List<AGenericGameElement>();
        session.RegisterObserver(this);
    }

    public void ShowHideSidebar()
    {
        if (ShowSideBar)
        {
            this.ButtonShowSideBar.gameObject.SetActive(true);
            this.gameObject.SetActive(false);
            ShowSideBar = false;
            // Activo - Desactivo zonas de jugadores que estén encima de la sidebar.
            foreach (GameObject g in ToPlayerZones)
            {
                g.SetActive(true);
            }
        }
        else
        {
            this.ButtonShowSideBar.gameObject.SetActive(false);
            this.gameObject.SetActive(true);
            ShowSideBar = true;
            foreach (GameObject g in ToPlayerZones)
            {
                g.SetActive(false);
            }
        }
    }

    // Solo anda con otros UI que estén siendo drageados
    public void OnDrop(PointerEventData eventData)
    {
        //Debug.Log("object:" + EventSystem.current.gameObject.name);
        /*SideBarElementScript d = eventData.pointerDrag.GetComponent<SideBarElementScript>();
        if (d != null)
        {
            d.transform.SetParent(this.parentConainer);
        }*/
        
    }

    public void ClearSideBar()
    {
        foreach (SideBarElementScript sideElement in SideBarPrefabs)
        {
            Destroy(sideElement);
        }
        this.SideBarPrefabs.Clear();
    }

    public void AddedToSidebar(AGenericGameElement gge)
    {
        GameObject newSideBarGGE = Instantiate(SideBarGGE) as GameObject;
        SideBarElementScript sic = newSideBarGGE.GetComponent<SideBarElementScript>();
        sic.Title.text = gge.GetType().ToString();
        sic.Description.text = gge.Name;
        sic.gge = gge;
        sic.Id = gge.Id;
        newSideBarGGE.transform.SetParent(parentConainer);
        newSideBarGGE.transform.localScale = new Vector3(1, 1, 1);
        newSideBarGGE.SetActive(true);

        //Lo agrego a la lista de gameobject
        SideBarPrefabs.Add(sic);
        if (!ShowSideBar)
            ShowHideSidebar();
    }
}
