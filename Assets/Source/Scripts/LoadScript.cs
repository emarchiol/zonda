﻿using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class LoadScript : MonoBehaviour {

    //Es estatica porque GameObjectGenerator tmb lo usa.
    //CollectionGenerator cg;
    public GameObject Menu;
    public InputField GameName;
    public Text GameNameSelected;
    public Button ServerButton;
    Game session;
    

	void Start()
	{
        session = Game.GetInstance();
		SetScreenOrientation();
		/*if(GameName!=null)
		{
			GameName.text = "forgottenisland";
		}*/

        if (GameNameSelected != null)
        {
            if (this.session != null)
                GameNameSelected.text = "Game selected:" + this.session.GameFileName;
            else
                GameNameSelected.text = "Game selected: none.";
        }
        if (SceneManager.GetActiveScene().buildIndex == 2)
        {
            if (session.isFetchingGameFromServer)
                ServerButton.interactable = false;
        }
	}

    public void DownloadFromServer()
    {
        session = Game.NewInstance();
        session.isFetchingGameFromServer = true;
        SceneManager.LoadScene(2);
    }

	//Cuando queremos cambiar de scene
	public void Scene(int menu)
	{
        SceneManager.LoadScene(menu);
        if (menu == 1 || menu == 0)
        {
            if (session != null)
            {
                Game.DestroySession();
            }
        }
    }
    
    
	//Cuando hacemos click en "Exit"
	public void Exit()
	{
		try
        {
			this.GetComponent<Network>().ServerWaitingThread.Abort ();
			this.GetComponent<Network>().ServerThread.Abort ();
			this.GetComponent<Network>().listener.Close();
            gameObject.GetComponent<Network>().listener.Shutdown(System.Net.Sockets.SocketShutdown.Both);
        }
        catch(Exception e){
			Debug.Log("Hilo no creado: "+e);
		}
		Application.Quit();
	}
    
	public static void SetScreenOrientation()
	{
		//Rotacion de la pantalla segun el sentido del dispositivo (solo landscape)
		Screen.autorotateToLandscapeLeft = true;
		Screen.autorotateToLandscapeRight = true;
		Screen.autorotateToPortrait = false;
		Screen.autorotateToPortraitUpsideDown = false;
		Screen.orientation = ScreenOrientation.AutoRotation;
	}

    public void CloseMenu()
    {
        session.GGeTouch = true;
        Menu.SetActive(false);
    }

    public void OpenMenu()
    {
        session.GGeTouch = false;
        Menu.SetActive(true);
    }

    public GameObject infopanel;
    public void FastClose()
    {

    }
}
