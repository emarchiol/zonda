﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class ConsoleElementScript : MonoBehaviour
{
    public Text Title;

    public void SetAttributes(string title)
    {
        this.Title.text = title;
        this.gameObject.GetComponent<LayoutElement>().preferredHeight = this.Title.GetComponent<RectTransform>().sizeDelta.y;
    }
}
