﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Reflection;
using System;

public class DropDownScript : MonoBehaviour {

	public GameObject cloneButton;
	public RectTransform container;
	public bool isOpen;
	// Use this for initialization
	void Start () 
	{
		container = transform.FindChild("Container").GetComponent<RectTransform>();
		isOpen = true;
	}
	
	// Update is called once per frame
	void Update () 
	{
		Vector3 scale = container.localScale;
		scale.y = Mathf.Lerp(scale.y, isOpen ? 1 : 0, Time.deltaTime * 12);
		container.localScale = scale;
	}

	//Inception-Reflection
	public void AddButton()
	{
		MethodInfo[] methodInfos = typeof(CardScript).GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly);


		Debug.Log("Imprimiendo metodos");
		foreach (MethodInfo methodInfo in methodInfos)
		{
			Debug.Log(methodInfo.Name);
		}

		container = transform.FindChild("Container").GetComponent<RectTransform>();
		GameObject instancia = Instantiate(cloneButton);
		instancia.GetComponent<RectTransform>().SetParent(container);
		instancia.GetComponent<RectTransform>().localScale = new Vector3(1,1,1);

		instancia.GetComponent<Button>().onClick.AddListener(() => Invoke(methodInfos[1].Name, 0));

	}
}
