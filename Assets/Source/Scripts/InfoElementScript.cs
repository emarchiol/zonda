﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;

public class InfoElementScript : MonoBehaviour
{
    public Text Title;
    public Text Description;

    public void SetAttributes(string title, string description)
    {
        this.Title.text = title;
        this.Description.text = description;
        this.gameObject.GetComponent<LayoutElement>().preferredHeight = this.Title.GetComponent<RectTransform>().sizeDelta.y + this.Description.GetComponent<RectTransform>().sizeDelta.y;
    }
}
