﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Reflection;
using System.Linq;

public class GGEFactory {

    public Dictionary<string, Type> InstanciablesTypes { set; get; }
    
    public AGenericGameElement CreateInstance(string typeGGE)
	{
        AGenericGameElement resultado = (AGenericGameElement)Activator.CreateInstance(GetIGGEType(typeGGE));
        return resultado;
    }

    public GGEFactory()
    {
        // Usamos LINQ para obtener un diccionario (nombreClase, tipoClase) a partir de todos
        // aquellos tipos del ensamblado actual que implementen la clase abstracta AGenericGameElement.
        this.InstanciablesTypes = (from types in Assembly.GetExecutingAssembly().GetTypes()
                                       //where types.GetInterface(typeof(AGenericGameElement).ToString()) != null
                                   where (types.IsClass && types.IsSubclassOf(typeof(AGenericGameElement)) || types.IsSubclassOf(typeof(ACollectionGameElement)))
                                   select types).ToDictionary(t => t.ToString(),  // Clave: nombre del tipo
                                                        t => t);            // Valor: tipo
    }

    /*public void ConfigureFactory()
    {
        // Usamos LINQ para obtener un diccionario (nombreClase, tipoClase) a partir de todos
        // aquellos tipos del ensamblado actual que implementen la interfaz IMotor.
        this.InstanciablesTypes = (from types in Assembly.GetExecutingAssembly().GetTypes()
                                   where types.GetInterface(typeof(AGenericGameElement).ToString()) != null
                                   select types).ToDictionary(t => t.ToString(),  // Clave: nombre del tipo
                                                        t => t);            // Valor: tipo
    }*/

    public Type GetIGGEType(string nombreTipo)
    {
        if (this.InstanciablesTypes == null)
            return null;
        else
            return (this.InstanciablesTypes.ContainsKey(nombreTipo) ? this.InstanciablesTypes[nombreTipo] : null);
    }

}
