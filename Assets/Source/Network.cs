﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System;
using System.Threading;
using System.Linq;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Collections;
using UnityEngine.SceneManagement;

public class Network : MonoBehaviour
{

    static int sendbufferSize = 1024;

    public Text IPText;
    public Text WaitingText;
    public Text InfoText;
    public Thread ServerThread;
    public Thread ServerWaitingThread;
    private Game session = Game.NewInstance();
    Player player = new Player();
    public Socket listener;
    public Socket ClientSocket;
    public InputField playerInput;
    public InputField playerInputName;
    static int port = 11000;
    static int GameScene;
    private static Semaphore ClientsConnectingSemaphore;
    static bool LoadBoard = false;
    public List<Socket> PlayersSockets;
    public IPEndPoint remoteEP;

    void Start()
    {
        // Just in case.
        int scene = SceneManager.GetActiveScene().buildIndex;
        if (scene == 2 || scene == 3 || scene == 4)
            session.PlayersListClear();
        if (playerInputName != null)
            playerInputName.Select();
        else if (playerInput != null)
            playerInput.Select();
    }

    void Update()
    {
        if (LoadBoard)
        {
            // Terminar el hilo de serverwaitingforplayers

            if (ServerWaitingThread != null)
            {
                try
                {
                    listener.Close();
                    ServerWaitingThread.Abort();
                }
                catch (SocketException e)
                {
                    Debug.Log("Error de shutdown en ServerWaiting: " + e);
                }
            }
            LoadBoard = false;
            SceneManager.LoadScene(GameScene);
        }
    }

    // SERVER stuff.
    public void ServerAmountPlayersInput()
    {

        if (playerInput.text == "")
        {
            playerInput.placeholder.GetComponent<Text>().text = "Please type something...";
        }
        else
        {
            if (Convert.ToInt16(playerInput.text) > session.maxPlayersGame)
            {
                playerInput.text = "";
                playerInput.placeholder.GetComponent<Text>().text = "Max. " + session.maxPlayersGame + " players";
            }
            else if (Convert.ToInt16(playerInput.text) < 1)
            {
                GameScene = 5;
                LoadBoard = true;
            }
            else
            {
                // Verifico que no cree una partida con cantidad de jug. > a cantidad de player.xml.
                int i;
                int players = Convert.ToInt16(playerInput.text);
                bool XMLPlayerFail = true;
                for (i=0; i <= players; i++)
                {
                    Debug.Log("i:"+i);
                    if (!File.Exists(session.GamePath + "/players/player" + i + ".xml"))
                    {
                        playerInput.text = "";
                        playerInput.placeholder.GetComponent<Text>().text = "Not enough player.xml files";
                        XMLPlayerFail = false;
                    }
                }
                if (XMLPlayerFail)
                { 
                    session.maxPlayers = Convert.ToInt16(playerInput.text);
                    playerInput.gameObject.SetActive(false);
                    this.WaitingText.gameObject.SetActive(true);
                    InfoText.text = "";
                    ServerInitialization();
                }
            }
        }
    }

    public void ServerInitialization()
    {
        // Seteo Playerinfo como servidor, o sea 0.
        GameScene = 5;
        session.PlayerInfo.pNumber = 0;

        IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
        //Si quieremos harcodear la IP del server xq la agarra mal es aca
        IPAddress ipAddress = IPAddress.Parse(host.AddressList.FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork).ToString());
        //IPAddress ipAddress = IPAddress.Parse("192.168.0.100");
        ClientsConnectingSemaphore = new Semaphore(1, 1);

        if (ipAddress.ToString().CompareTo("127.0.0.1") == 0)
        {
            SceneManager.LoadScene(0);
            ConsoleScript.AddConsoleOutput("Error: wifi turned off?");
        }
        else
        {
            IPEndPoint localEndPoint = new IPEndPoint(ipAddress, port);

            // Create a TCP/IP socket.
            listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            Debug.Log("IP Detected:" + ipAddress.ToString());
            session.PlayerInfo.ipAddress = ipAddress.ToString();

            IPText.text = ipAddress.ToString();

            // Bind the socket to the local endpoint and 
            // listen for incoming connections.
            try
            {
                listener.Bind(localEndPoint);
                listener.Listen(100); //cantidad de peticiones antes de que tire busyResponseError, la cantidad de player esta limitada a 99 asi que lo dejo asi aunque sea meramente teorico
                                      //Deberia crearse un hilo por cada jugador que se conecte, hacer el handshake y si esta todo en orden sumar sobre una variable estatica maxPlayerReached
                                      //Server va revisando esa variable y cuando llega a su maximo deja de escuchar y spawnea los objetos en la mesa.
                ServerWaitingThread = new Thread(() => ServerWaitingForPlayers(listener));
                ServerWaitingThread.Start();
                Debug.Log("Hilo ServerWaiting creado, esperando conexiones");

            } catch (SocketException e) {
                Debug.Log("Server initizalization:" + e);
            }
        }
    }

    public void ServerWaitingForPlayers(Socket listener)
    {
        try {
            PlayersSockets = new List<Socket>();
            do
            {
                Thread.Sleep(1);
                // Program is suspended while waiting for an incoming connection.
                Socket ThreadListener = listener.Accept();
                if (session.playersAmount < session.maxPlayers)
                {
                    PlayersSockets.Add(ThreadListener);
                    //Handshake o in game ?
                    //Creo hilo y paso el parametro

                    ServerThread = new Thread(() => ServerHandshake(ThreadListener));
                    Debug.Log("Handshake finalized");

                    ServerThread.Start();
                }

            } while (session.playersAmount < session.maxPlayers);
        }
        catch (ThreadAbortException e)
        {
            Debug.Log("ServerWaitingForPlayers thread aborted:" + e);
        }
        catch (SocketException e)
        {
            Debug.Log("ServerWaitingForPlayers:" + e);
        }
    }

    public void ServerHandshake(Socket ClientConnectedSocket)
    {
        player = (Player)ProtocolReceive(ClientConnectedSocket);

        if (player.name.CompareTo("") == 0 || player.ipAddress.CompareTo("") == 0)
        {
            Debug.Log("Objeto invalido");
            player.name = "null";
            player.pNumber = -2;
            player.GameMd5 = "null";
            player.ipAddress = "null";
            
            ProtocolSend(player, ClientConnectedSocket);

        }
        else
        {
            //ProtocolSend(player, ClientConnectedSocket);

            Debug.Log("player valido");
            //Player p = (Player)ProtocolReceive(ClientConnectedSocket);

            if (session.PlayerInfo.GameMd5 == player.GameMd5)
            {
                Debug.Log("version matchs!");
                // Returning player with md5.
                ProtocolSend(player, ClientConnectedSocket);
                Debug.Log("Handshake finalized");
            }
            // Fetch from server
            else if (player.GameMd5 == "NFETCH")
            {
                player.GameMd5 = session.PlayerInfo.GameMd5;
                ProtocolSend(player, ClientConnectedSocket);
                // Version mismatch, returning empty md5 and starting game transfer.
                Debug.Log("Client is requesting game.");

                // Waiting for client to be ready to dl.
                player = (Player)ProtocolReceive(ClientConnectedSocket);
                Debug.Log("sending file");
                if (player.GameMd5 == "READY")
                {
                    player.pNumber = -1;
                    player.GameMd5 = session.PlayerInfo.GameMd5;

                    // Envío del archivo zip
                    byte[] bufferSize = new byte[4];
                    bufferSize = Encoding.ASCII.GetBytes("0000");
                    Debug.Log("trying to send file");
                    ClientConnectedSocket.SendFile(session.GamePathPacket,null, bufferSize, TransmitFileOptions.UseDefaultWorkerThread);
                    Debug.Log("file sent");
                }
                else if (player.GameMd5 == "GAMEOK")
                {
                    Debug.Log("he has game");
                }
            }
            else if (session.PlayerInfo.GameMd5 != player.GameMd5)
            {
                // Other game selected.
                Debug.Log("Different games :(");

                player.GameMd5 = "";
                player.gameName = "";

                ProtocolSend(player, ClientConnectedSocket);
                Debug.Log("Handshake finalized, bye bye.");
                
                //ClientsConnectingSemaphore.WaitOne();
                //AGameEnviroment.session.playersAmount--;
            }
            // Este no debería existir
            /*
            else
            {
                // Version mismatch, returning empty md5 and starting game transfer, hay que terminar la conexión acá.
                Debug.Log("version mismatch :(");
                Debug.Log("Devuelvo Player con numero de jugador y el md5 del server para post verificacion");
                player.pNumber = 1;
                AGameEnviroment.session.AddPlayerToPlayersList(player);
                player.GameMd5 = session.PlayerInfo.GameMd5;

                ProtocolSend(player, ClientConnectedSocket);
                Debug.Log("Handshake finalized, sending game");
                // Envío del archivo zip.
                ClientConnectedSocket.SendFile(session.GamePathPacket);
                AGameEnviroment.session.playersAmount--;
            }*/
            // Espero el mensaje final del cliente para saber si todo está bien
            try
            {
                Debug.Log("esperando fracaso/exito del cliente");
                player = (Player)ProtocolReceive(ClientConnectedSocket);

                if (session.PlayerInfo.GameMd5 == player.GameMd5)
                {
                    Debug.Log("Good MD5, player connected successfully");
                    Debug.Log("Devuelvo Player con numero de jugador y lo agrego a lista de jugadores");

                    player.pNumber = session.playersAmount + 1;
                    Debug.Log("adding player:" + player.name + " ip:" + player.ipAddress);
                    session.AddPlayerToPlayersList(player);

                    // Envia número de jugador y cierro conexión de handshake.
                    ProtocolSend(player, ClientConnectedSocket);
                    try
                    { 
                        ClientConnectedSocket.Shutdown(SocketShutdown.Both);
                    }catch(Exception){ }


                    session.playersAmount++;
                }
                else
                {
                    Debug.Log("Bad MD5 in handshake");
                    ConsoleScript.AddConsoleOutput("Client disconnected");
                }
            }
            catch (InvalidCastException)
            {
                Debug.Log("Bad handshake.");
                ConsoleScript.AddConsoleOutput("Client disconnected");
                //AGameEnviroment.session.playersAmount--;
            }
            catch (Exception e)
            {
                Debug.Log(e);
            }

            // Semaforo.
            ClientsConnectingSemaphore.WaitOne();
            //AGameEnviroment.session.playersAmount++;
            Debug.Log("players connected:" + session.playersAmount);

            // Se alcanzó la cantidad de jugadores?, esperar a que los jugadores avisen
            if (session.playersAmount == session.maxPlayers)
            {
                LoadBoard = true;
            }
            ClientsConnectingSemaphore.Release();

            PlayersSockets.Remove(ClientConnectedSocket);
            // Cierro el socket.
            try
            {
                //ClientConnectedSocket.Shutdown(SocketShutdown.Both);
                ClientConnectedSocket.Close();
            }
            catch (SocketException e)
            {
                Debug.Log("ServerHandshake:" + e);
            }
        }
    }

    public void ServerCancelWait()
    {
        //El try catch es xq si no hay ningun cliente conectado SocketShutDown.Both tira excepcion (xq no tiene a nadie conectado para avisarle de cerrar el socket)
        try
        {
            // Close every connection that was opened with a client.
            if (PlayersSockets != null)
            {
                if (PlayersSockets.Count > 0)
                {
                    foreach (Socket s in PlayersSockets)
                    {
                        s.Close();
                    }
                    PlayersSockets.Clear();
                }
            }

            if (listener != null)
            {
                // Close server socket.
                listener.Close();
            }
        }
        catch (SocketException e) {
            Debug.Log("Cancel server wait:" + e);
        }

        // Finish the threads.
        if (ServerWaitingThread != null)
            ServerWaitingThread.Abort();
        if (ServerThread != null)
            ServerThread.Abort();

        session.playersAmount = 0;
        session.PlayersListClear();

        SceneManager.LoadScene(2);
    }

    // CLIENT stuff.
    public void ClientInputName()
    {
        //Nombre del jugador
        if (playerInputName.text != "")
        {
            InfoText.text = "Enter server IP...";
            session.PlayerInfo.name = playerInputName.text;
            playerInputName.gameObject.SetActive(false);
            playerInput.Select();
        }
    }

    // Called from the IP textbox in unity.
    public void ClientInitialization()
    {
        IPAddress ipAddress;
        GameScene = 7;

        if (playerInput.text == "")
        {
            // IP verification.
            if (!IPAddress.TryParse(playerInput.text, out ipAddress))
            {
                ConsoleScript.AddConsoleOutput("'" + playerInput.text + "' is an invalid IP address.");
                ClientCancelWait();
            }
        }
        else
        {
            try
            {
                playerInput.gameObject.SetActive(false);
                WaitingText.gameObject.SetActive(true);

                // Establish the remote endpoint for the socket.
                ipAddress = IPAddress.Parse(playerInput.text);
                remoteEP = new IPEndPoint(ipAddress, port);
                session.PlayerInfo.ipAddress = ipAddress.ToString();

                // Create a TCP/IP  socket.
                ClientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                // Connect the socket to the remote endpoint. Catch any errors.
                try
                {
                    // Timeout 5 sec.
                    IAsyncResult result = ClientSocket.BeginConnect(remoteEP, null, null);
                    result.AsyncWaitHandle.WaitOne(5000, true);

                    if (ClientSocket.Connected)
                    {
                        Debug.Log("Socket connected to " + ClientSocket.RemoteEndPoint.ToString());
                        Debug.Log("Handshake started...");
                        // Guard IP del server.
                        session.AddPlayerToPlayersList(new Player("Server", playerInput.text, 0, ""));
                        StartCoroutine(ClientHandshake());
                    }
                    else
                    {
                        ConsoleScript.AddConsoleOutput("Connection with " + ipAddress + " timed out.");
                        ClientCancelWait();
                    }
                }
                catch (ArgumentNullException ane)
                {
                    Debug.Log("ArgumentNullException : " + ane.ToString());
                }
                catch (SocketException se)
                {
                    Debug.Log("SocketException : " + se.ToString());
                }
                catch (SerializationException sse)
                {
                    Debug.Log("Serialization exception: " + sse.ToString());
                }
                catch (Exception e)
                {
                    Debug.Log("Unexpected exception : " + e.ToString());
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }

    bool downloadCompleted=false;
    public void FetchServerGame()
    {
        bool keepReading = true;
        int read;
        byte[] buf = new byte[sendbufferSize];

        var output = File.Create(ZondaConfigurationScript.GamesPath + player.GameMd5 + ".zip");
        
        while (keepReading)
        {
            read = ClientSocket.Receive(buf);
            // Wait for end file signal(0000).
            string size = Encoding.ASCII.GetString(buf,read-4,4);
            //messageSize = Convert.ToInt32(size);
            Debug.Log("size:" + size);
            if (size == "0000")
            {
                Debug.Log("bingo");
                keepReading = false;
                // Write without the ending signal otherwise it will change the MD5.
                output.Write(buf, 0, read-4);
            }
            else
            { 
                output.Write(buf, 0, read);
                Debug.Log("reading:" + read);
            }
        }

        output.Close();
        downloadCompleted = true;
        Debug.Log("thread completed");
    }

    public IEnumerator ClientHandshake()
    {
        //Player IP Address
        IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
        session.PlayerInfo.ipAddress = host.AddressList.FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork).ToString();
        session.PlayerInfo.gameName = session.GameName;

        if (session.isFetchingGameFromServer)
            session.PlayerInfo.GameMd5 = "NFETCH";

        ProtocolSend(session.PlayerInfo, ClientSocket);

        try
        {
            player = (Player)ProtocolReceive(ClientSocket);
        }
        catch (InvalidCastException e)
        {
            Debug.Log("Not a player object" + e);
            player = new Player("null", "null", -2, "null");
        }

        Debug.Log("Player number: " + player.pNumber);
        Debug.Log("ServerMd5:" + player.GameMd5);
        Debug.Log("SessionMd5:" + session.PlayerInfo.GameMd5);

        if (player.name != "null" && player.pNumber != -2 && player.GameMd5 != "null")
        {
            if (player.GameMd5 == session.PlayerInfo.GameMd5)
            {
                Debug.Log("md5 are ok");
                LoadBoard = true;
            }
            else if (player.GameMd5 != "")
            {
                // Verificar que el cliente ya no tenga el juego y no sea un manco.
                if (ZondaConfigurationScript.CheckGameInXML(player.GameMd5))
                {
                    ConsoleScript.AddConsoleOutput("You already have the game, why you do this ?");
                    ConsoleScript.AddConsoleOutput("Loading existing package...");
                    Debug.Log("sending GAMEOK");
                    ProtocolSend(new Player("","",0,"GAMEOK"), ClientSocket);

                    yield return new WaitForFixedUpdate();
                    // Loading game.
                    LoadDownloadedGame(ZondaConfigurationScript.GamesPath + player.GameMd5 + ".zip");
                    LoadBoard = true;
                }
                else
                {
                    ConsoleScript.AddConsoleOutput("Fetching server game, this may take a while...");
                    yield return new WaitForFixedUpdate();
                    Debug.Log("sending READY");

                    session.PlayerInfo.GameMd5 = player.GameMd5;
                    player.GameMd5 = "READY";
                    ProtocolSend(player, ClientSocket);
                    player.GameMd5 = session.PlayerInfo.GameMd5;

                    Thread FetchGameThread = new Thread(() => FetchServerGame());
                    FetchGameThread.Start();

                    do
                    {
                        yield return new WaitForFixedUpdate();
                    } while (!downloadCompleted);

                    
                    ConsoleScript.AddConsoleOutput("File received, working package...");
                    yield return new WaitForFixedUpdate();
                    // Adding package.
                    if (ZondaConfigurationScript.AddGameToXML(ZondaConfigurationScript.GamesPath + player.GameMd5 + ".zip"))
                    {
                        ConsoleScript.AddConsoleOutput("Download completed.");
                        yield return new WaitForFixedUpdate();
                        // Loading game.
                        LoadDownloadedGame(ZondaConfigurationScript.GamesPath + player.GameMd5 + ".zip");
                        this.session.PlayerInfo.GameMd5 = player.GameMd5;
                        LoadBoard = true;
                    }
                    else
                    {
                        ConsoleScript.AddConsoleOutput("Corrupted file.");
                        LoadBoard = false;
                    }
                    Directory.Delete(ZondaConfigurationScript.ConfigTempPath, true);
                }

            }
            else if (player.GameMd5 == "")
            {
                ConsoleScript.AddConsoleOutput("Disconnected: games are different.");
                LoadBoard = false;
            }
        }
        else
        {
            LoadBoard = false;
        }

        //Player.xml check.
        if (LoadBoard)
        {
            Debug.Log("aviso al servidor que fue todo un éxito");
            ProtocolSend(session.PlayerInfo, ClientSocket);
            try
            {
                // Ahora si, el server me pasa el número de jugador.
                player = (Player)ProtocolReceive(ClientSocket);
                player.printAtt();
                this.session.PlayerInfo = player;
            }
            catch (InvalidCastException e)
            {
                Debug.Log("Not a player object" + e);
            }
            // Load board.
            ConsoleScript.AddConsoleOutput("Connection successful, loading board...");
            yield return new WaitForFixedUpdate();
            SceneManager.LoadScene(GameScene);
        }
        else
        {
            Debug.Log("aviso al servidor que fue todo un fracaso:");
            session.PlayerInfo.GameMd5 = "";
            ProtocolSend(session.PlayerInfo, ClientSocket);
            SceneManager.LoadScene(0);
        }

        try
        {
            ClientSocket.Close();
        }
        catch (SocketException e)
        {
            Debug.Log("Client Handshake:" + e);
        }

        LoadBoard = false;
        yield break;
    }

    public void ClientCancelWait()
    {
        //El try catch es xq si no hay ningun cliente conectado SocketShutDown.Both tira excepcion (xq no tiene a nadie conectado para avisarle de cerrar el socket)
        try
        {
            if (ClientSocket != null)
            { 
                ClientSocket.Close();
            }
        }
        catch (SocketException e)
        {
            Debug.Log("ClientCancelWait:"+e);
        }
        finally
        {
            SceneManager.LoadScene(2);
        }
    }

    // Static methods. Use this to send objects.
    public static void SendAGGEToPlayer(AGenericGameElement agge, Player player)
    {
        Debug.Log("sending to "+player.name+":"+player.ipAddress);
        Socket ClientSendSocket;

        // Establish the remote endpoint for the socket.
        IPAddress ipAddress = IPAddress.Parse(player.ipAddress);
        IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);
        // Create a TCP/IP  socket.
        ClientSendSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        // Connect the socket to the remote endpoint. Catch any errors.

        try
        {
            // Timeout 5 sec.
            IAsyncResult result = ClientSendSocket.BeginConnect(remoteEP, null, null);
            result.AsyncWaitHandle.WaitOne(3000, true);

            if (ClientSendSocket.Connected)
            { 
                //ClientSendSocket.Connect(remoteEP);
                Debug.Log("Socket connected to " + ClientSendSocket.RemoteEndPoint.ToString());
                ProtocolSend(agge, ClientSendSocket);
            }
            else
                ConsoleScript.AddConsoleOutput("Connection timed out.");
        }
        catch (ArgumentNullException ane)
        {
            Debug.Log("ArgumentNullException : " + ane.ToString());
        }
        catch (SocketException se)
        {
            Debug.Log("SocketException : " + se.ToString());
        }
        catch (SerializationException sse)
        {
            Debug.Log("Serialization exception: " + sse.ToString());
        }

        // Close the damn socket.
        try
        {
            ClientSendSocket.Close();
            Debug.Log("socket closed");
        }
        catch (SocketException e)
        {
            Debug.Log("SendAGGEToServer:" + e);
        }
    }

    // Thread that keep listening for incomming objects while playing.
    public static void KeepListeningForObjects(Socket ThreadSocket)
    {
        Game session = Game.NewInstance();
        Thread PlayerServeThread;
        IPEndPoint localEndPoint;

        localEndPoint = new IPEndPoint(IPAddress.Parse(session.PlayerInfo.ipAddress), port);
        ThreadSocket.Bind(localEndPoint);
        ThreadSocket.Listen(100);
        
        do
        {
            Socket objSocket = ThreadSocket.Accept();
            PlayerServeThread = new Thread(() => Network.ReceiveObject(objSocket));
            PlayerServeThread.Start();
        } while (true);
    }
    // Solo para recibir netMessage o AGenericGameElements.
    public static void ReceiveObject(Socket playerConnected)
    {
        Game session = Game.NewInstance();
        AGenericGameElement aGGE;
        int pNumber=0;
        string ipRemote = ((IPEndPoint)playerConnected.RemoteEndPoint).Address.ToString();

        Debug.Log("obj received from:" + ipRemote);

        foreach (Player p in session.GetPlayersList())
        {
            if (p.ipAddress == ipRemote)
                pNumber = p.pNumber;
        }

        try
        {
            if ((aGGE = (AGenericGameElement)ProtocolReceive(playerConnected)) == null)
            { 
                Debug.Log("received obj is null");
            }
            else
            {
                Debug.Log("recibi:" + aGGE.Name);
                if (aGGE is NetMessage)
                {
                    // Check message stuff.
                    CheckNetMessage(aGGE as NetMessage);
                }
                else
                {
                    if (aGGE is AGenericGameElement)
                    {
                        // Adding to sidebar if its client and is received from other player, otherwise adding to boardZone.
                        if (session.PlayerInfo.pNumber != 0 && pNumber != 0)
                        {
                            session.AddToNetworkSidebar(aGGE);
                            #if (!UNITY_EDITOR && (UNITY_IPHONE || UNITY_ANDROID))
                                Handheld.Vibrate();        
                            #endif
                        }
                        // Si es servidor y no tiene zona porque hay más de 6 jugadores.
                        else if (session.PlayerInfo.pNumber == 0 && pNumber>6)
                        {
                            session.AddToNetworkSidebar(aGGE);
                        }
                        else
                            session.AddToGGEToPlayerZone(aGGE, pNumber);
                    }
                    else
                    {
                        Debug.Log("Unexpected type received.");
                    }
                }
            }
        }
        catch (Exception e)
        {
            Debug.Log("ReceiveObj:" + e);
            ProtocolSend(new NetMessage(new Player(),"NETERROR"), playerConnected);
        }
        
        try
        {
            playerConnected.Shutdown(SocketShutdown.Both);
            playerConnected.Close();
        }
        catch (SocketException e)
        {
            Debug.Log("ReceiveObject:" + e);
        }
    }

    // Protocol Send&Receive
    public static void ProtocolSend(System.Object anyObject, Socket socketToSend)
    {
        bool objectSent = false;
        do
        {
            // For Serialization.
            MemoryStream ms = new MemoryStream();
            BinaryFormatter formatter = new BinaryFormatter();
            NetworkStream ns = new NetworkStream(socketToSend);

            //serializo, meto en buffer y envio
            formatter.Serialize(ms, anyObject);
            byte[] buffer = ms.ToArray();

            byte[] bufferSize = new byte[4];
            byte[] aux = new byte[4];
            aux = Encoding.ASCII.GetBytes(buffer.Length.ToString());


            bufferSize = Encoding.ASCII.GetBytes("0000");

            if (aux.Length <= 4)
            {
                Buffer.BlockCopy(aux, 0, bufferSize, 4 - aux.Length, aux.Length);
            }
            ns.Write(bufferSize, 0, 4);
            Debug.Log("sending object, size to send:" + buffer.Length);
            socketToSend.Send(buffer, buffer.Length, 0);

            // Wait for OK response, AKA same buffer size again, if 0, then re send the object.
            byte[] clientSizeData = new byte[4];
            int readTotal = 4;
            int readSize = 0;
            do
            {
                readSize = ns.Read(clientSizeData, 0, 4);
                readTotal = readTotal - readSize;
            } while (readTotal > 0);

            string size = Encoding.ASCII.GetString(clientSizeData);
            //messageSize = Convert.ToInt32(size);
            if (size == "0000")
            {
                Debug.Log("size to receive:" + size);
                objectSent = false;
            }
            else
            {
                Debug.Log("size to receive:" + size);
                objectSent = true;
            }
            
        } while (!objectSent);

    }

    public static System.Object ProtocolReceive(Socket socketToReceive)
    {
        bool msgSent = false;
        System.Object objReceived=null;
        do { 
            int messageSize;
            byte[] clientSizeData = new byte[4];
            NetworkStream ns = new NetworkStream(socketToReceive);
            //ns.ReadTimeout = 10;
            int readTotal = 4;
            int readSize = 0;
            do
            {
                readSize = ns.Read(clientSizeData, 0, 4);
                readTotal = readTotal - readSize;
            } while (readTotal > 0);
        
            //string size = Encoding.ASCII.GetString(clientSizeData, 0, sizeof(int));
            string size = Encoding.ASCII.GetString(clientSizeData);
            messageSize = Convert.ToInt32(size);

            byte[] clientData = new byte[messageSize];
            BinaryFormatter formatter = new BinaryFormatter();
            Debug.Log("size to receive:" +messageSize);
            int read=0;
            int debugRead = 0;
            do
            {
                read = socketToReceive.Receive(clientData, read, messageSize, SocketFlags.None);
                messageSize = messageSize - read;
                debugRead += read;

            } while (messageSize>0);
            Debug.Log("amount received:" + debugRead);
            try
            {
                MemoryStream ms = new MemoryStream(clientData);
                objReceived = formatter.Deserialize(ms);
                if (objReceived == null)
                {
                    // Send NOT OK and re send object.
                    byte[] bufferSize = new byte[4];
                    bufferSize = Encoding.ASCII.GetBytes("0000");

                    ns.Write(bufferSize, 0, 4);
                    Debug.Log("not ok sent, re sending obj.");
                }
                else
                {
                    //Sending OK message
                    byte[] bufferSize = new byte[4];
                    bufferSize = Encoding.ASCII.GetBytes("9999");
                    // Send OK.
                    ns.Write(bufferSize, 0, 4);
                    Debug.Log("OK sent.");

                    msgSent = true;
                }
                
            }
            catch (Exception)
            {
                // Send NOT OK and re send object.
                byte[] bufferSize = new byte[4];
                bufferSize = Encoding.ASCII.GetBytes("0000");

                ns.Write(bufferSize, 0, 4);
                Debug.Log("not ok sent, re sending obj.");
            }

        } while (!msgSent);
        return objReceived;
    }

    public static void CheckNetMessage(NetMessage nMsg)
    {
        Game session = Game.NewInstance();

        switch (nMsg.checkFlag())
        {
            // Agrego a mi lista de players
            case "PSETUP":
                if (!session.ContainPlayer(nMsg.PlayerInfo))
                    session.AddPlayerToPlayersList(nMsg.PlayerInfo);
                break;

            case "SESSIONLOG":
                break;

            case "LOGREQUEST":
                break;

            case "SLOADED":
                if (session.PlayerInfo.pNumber == 0)
                {
                    NetMessage rMsg = new NetMessage(session.PlayerInfo, "SLOADED");
                    Network.SendAGGEToPlayer(rMsg, nMsg.PlayerInfo);
                }
                else
                { 
                    session.isServerLoaded = true;
                    Debug.Log("Client");
                }
                break;
            
            // Restarteo la sesión.
            case "SRESTART":
                session.restartSession = true;
                break;

            case "NULL":
                Debug.Log("Bad flag received in Network Message.");
                break;

            case "NETDISC":
                session.RemovePlayerFromPlayersList(session.FindPlayer(nMsg.PlayerInfo.pNumber));
                break;
        }
    }

    public void LoadDownloadedGame(string path)
    {
        // Creo un CollectionGenerator que se encargará de revisar los XML.
        CollectionGenerator cg;
        string tempDir = ZondaConfigurationScript.TempPath;
        string gameTempDir = tempDir + Path.GetFileName(path);

        // Si el juego ya está descomprimido sigo.
        if (!Directory.Exists(gameTempDir))
        {
            // Para que aparezca el mensaje en la consola antes de empezar la extracción.
            Directory.CreateDirectory(gameTempDir);
            ZipUtil.Unzip(path, gameTempDir);
        }

        session.GamePath = gameTempDir + "/";
        session.GamePathPacket = path;

        try
        {
            cg = new CollectionGenerator();
            // Game packet verfication.
            cg.CheckGameFormat();

            MD5 md5File = MD5.Create();
            var stream = File.OpenRead(path);
            session.PlayerInfo.GameMd5 = BitConverter.ToString(md5File.ComputeHash(stream)).Replace("-", "").ToLower();

            session.GameFileName = Path.GetFileName(path);
        }
        catch (InvalidGameFormatException)
        {
            ConsoleScript.AddConsoleOutput("Invalid game or corrupted package...");
            SceneManager.LoadScene(0);
        }
    }

}

