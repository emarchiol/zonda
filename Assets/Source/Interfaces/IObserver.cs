﻿public interface IObserver
{
    // Métodos de los observers que ejecutaran cuando el subject reciba un cambio
    void ClearSideBar();
    void AddedToSidebar(AGenericGameElement gge);
}

