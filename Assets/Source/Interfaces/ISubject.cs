﻿using UnityEngine;
using System.Collections;

public interface ISubject {
    void RegisterObserver(IObserver o);
    void DeleteObserver(IObserver o);

    // Métodos para notificar los observers
    void NotifyObserversClearSideBar();
    void NotifyObserversAddedToSidebar(AGenericGameElement gge);
}
