﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class IObjectDetectionUI : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerUpHandler, IPointerDownHandler{

    bool ObjOnDrag = false;
    public GameObject UIObject;
    private Game session = Game.NewInstance();
    void OnGUI()
    {
        if (ObjOnDrag)
        {
            if (!session.GGeTouch)
            {
                if(UIObject!=null)
                    Debug.Log("obj Dropped on:" + UIObject.name);
            }
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (AGameEnviroment.ObjBeingTouched != null)
        {
            //Debug.Log("obj:" + GEServer.ObjBeingTouched.name);
            UIObject = GEServer.ObjBeingTouched;
            ObjOnDrag = true;
        }
    }
    public void OnPointerUp(PointerEventData eventData)
    {

    }

    public void OnPointerExit(PointerEventData eventData)
    {
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        //Debug.Log("down");
    }
}
