﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Threading;

public class Game : ISubject
{
    //NETWORK
    //-> Tiene que popular a: Players, PlayerInfo, playersAmount
    // Observers.
    IList Subscribers;
    // Lista de todos los objetos de juego (GGes) y lista de objetos en la mesa (OnBoardGges).
    private List<AGenericGameElement> _AllGGEs;
    private List<AGGEScript> _OnBoardGGEs;
    private List<AGenericGameElement> _SideBarGGEs;

    // Por unity thread safe (un hilo no puede spawnear objs).
    //public List<AGenericGameElement> NetworkOnBoardGGE { get; set; }
    private List<AGenericGameElement> NetworkSidebarGGE { get; set; }
    private Dictionary<AGenericGameElement, int> GGEToPlayerZone { get; set; }

    public Board PlayerFloor { get; set; }
    public string GameFileName { get; set; }
    
    private List<Player> Players { set; get; }
    // Numero del jugador propio, esto lo tiene que setear network, server recibe 0, client recibe el número en el handshake.
    public Player PlayerInfo { set; get; }

    // Corresponde al checkbox.
    public bool CameraEnabled = true;
    public bool CameraIsChanging = false;
    // Cuando se abre el log/main menu se deshabilita el movimiento de los objetos de juego.
    public bool GGeTouch = true;

    // Esto lo tiene que sacar del core.xml.
    public int maxPlayersGame { get; set; } //maximo de jugadores que admite el juego.
    public int maxPlayers{get; set;} //maximo de jugadores que admite la sesión en curso.
    public int minPlayers{get; set;}
    public int playersAmount { get; set; }
    // Nombre del juego extraido desde core.xml
    public string GameName { get; set; }
    // Este path representa el path del core.xml dentro de la carpeta temp.
    public string GamePath { get; set; }
    // Este path representa el path del zip/zon
    public string GamePathPacket { get; set; }
    public bool restartSession = false;
    // Only for client
    public bool isServerLoaded = false;
    public bool isFetchingGameFromServer = false;
    public int test=0;

    // Para Singleton.
    private static Game _session = null;

    // Instanciador
    public static Game GetInstance()
    {
        return _session;
    }
    public static Game NewInstance()
    {
        if (_session == null)
        {
            ConsoleScript.AddConsoleOutput("New session started.");
            _session = new Game();
            _session.Subscribers = new List<System.Object>();
            _session._AllGGEs = new List<AGenericGameElement>();
            _session._OnBoardGGEs = new List<AGGEScript>();
            _session._SideBarGGEs = new List<AGenericGameElement>();
            _session.NetworkSidebarGGE = new List<AGenericGameElement>();
            _session.GGEToPlayerZone = new Dictionary<AGenericGameElement, int>();
            _session.Players = new List<Player>();
            _session.PlayerInfo = new Player();
            _session.PlayerFloor = new Board();
        }
        return _session;
    }
    public static void CleanSingleton()
    {
        _session = null;
    }
    public static Game NewInstance(List<AGenericGameElement> cGGes, List<AGGEScript> cOnBoardGges, List<Player> cPlayers)
    {
        if (_session == null)
        {
            ConsoleScript.AddConsoleOutput("New session started.");
            _session = new Game();
            _session.Subscribers = new List<System.Object>();
            _session._AllGGEs = cGGes;
            _session._OnBoardGGEs = cOnBoardGges;
            _session.Players = cPlayers;
        }
        return _session;
    }

    public static void DestroySession()
    {
        _session = null;
        ConsoleScript.AddConsoleOutput("Session ended.");
        ConsoleScript.AddConsoleOutput("---------------------------------");
    }

    // El constructor sobrecargado serviría en un futuro para cargar una partida vieja.
    private Game(List<AGenericGameElement> cGGes, List<AGGEScript> cOnBoardGges, List<Player> cPlayers)
    {
        this.Subscribers = new List<System.Object>();
        this._AllGGEs = cGGes;
        this._OnBoardGGEs = cOnBoardGges;
        this.Players = cPlayers;
    }

    private Game()
    {
        this.Subscribers = new List<System.Object>();
        this._AllGGEs = new List<AGenericGameElement>();
        this._OnBoardGGEs = new List<AGGEScript>();
        this._SideBarGGEs = new List<AGenericGameElement>();
        this.NetworkSidebarGGE = new List<AGenericGameElement>();
        //this.NetworkOnBoardGGE = new List<AGenericGameElement>();
        this.GGEToPlayerZone = new Dictionary<AGenericGameElement, int>();

        this.Players = new List<Player>();
        this.PlayerInfo = new Player();
        this.PlayerFloor = new Board();
    }

    public void ResetSession()
    {
        this.GGeTouch = true;
        this.Subscribers.Clear();
        lock (this.NetworkSidebarGGE)
        {
            this.OnBoardGGEsClear();
            this.SideBarGGEsClear();
            this.NetworkSidebarGGE.Clear();
            this.GGEToPlayerZone.Clear();
            //this.NetworkOnBoardGGE.Clear();
        }

        //this.NotifyObserversSideBar();
        //this.NotifyObserversClearSideBar();
    }

    // Todos los métodos de esta clase tienen que "lock"ear a sus atributos para que sean threadsafe.
    // PLAYER Methods.
    public Player FindPlayer(int number)
    {
        return Players.Find(player => player.pNumber == number);
    }

    public void RemovePlayerFromPlayersList(Player p)
    {
        lock(this.Players)
        {
            this.Players.Remove(p);
        }
    }

    public void AddPlayerToPlayersList(Player p)
    {
        lock(this.Players)
        {
            this.Players.Add(p);
        }
    }

    public List<Player> GetPlayersList()
    {
        return this.Players;
    }

    public void PlayersListClear()
    {
        lock(this.Players)
        {
            this.Players.Clear();
        }
    }

    public bool ContainPlayer(Player p)
    {
        lock (this.Players)
        {
            if (this.Players.Contains(p))
                return true;
            else
                return false;
        }
    }

    // SIDEBAR GGEs Methods.
    public void AddToSideBarGGEs(AGenericGameElement gge)
    {
        lock (this._SideBarGGEs)
        { 
            this._SideBarGGEs.Add(gge);
        }
        this.NotifyObserversAddedToSidebar(gge);
    }    
        
    public List<AGenericGameElement> GetSideBarGGEs()
    {
        return _SideBarGGEs;
    }

    public List<AGenericGameElement> FindDecksSideBarGGEs()
    {
        return this._SideBarGGEs.FindAll(fl => this._SideBarGGEs.Contains(fl as CardDeck));
    }

    public void SideBarGGEsClear()
    {
        lock (this._SideBarGGEs)
        { 
            this._SideBarGGEs.Clear();
        }
        NotifyObserversClearSideBar();
    }

    public void RemoveSideBarGGE(AGenericGameElement gge)
    {
        lock (this._SideBarGGEs)
        { 
            this._SideBarGGEs.Remove(gge);
        }

        // No hace falta porque el único que puede quitar elementos de la sidebar es la propia sidebar (el usuario con el dedo).
        //NotifyObserversSideBar();
    }

    // GGEToPlayerZone Methods
    public Dictionary<AGenericGameElement, int> GetGGEToPlayerZone()
    {
        return this.GGEToPlayerZone;
    }

    public void RemoveGGEToPlayerZone(AGenericGameElement gge)
    {
        lock (this.GGEToPlayerZone)
        {
            this.GGEToPlayerZone.Remove(gge);
        }
    }

    public void AddToGGEToPlayerZone(AGenericGameElement GGE, int zone)
    {
        lock (this.GGEToPlayerZone)
        {
            this.GGEToPlayerZone.Add(GGE, zone);
        }
    }

    // ONBOARD GGEs Methods.
    public void AddToOnBoardGGEs(AGGEScript GGE)
    {
        lock (this._OnBoardGGEs)
        {
            this._OnBoardGGEs.Add(GGE);
        }
    }

    public List<AGGEScript> GetOnBoardGGES()
    {
        return _OnBoardGGEs;
    }

    /*public List<AGGEScript> FindDecksOnBoardGGEs()
    {
        return this._OnBoardGGEs.FindAll(fl => this._OnBoardGGEs.Contains(fl as CardDeck));
    }*/

    /*public void OnBoardGGESRemoveAll(AGenericGameElement gge)
    {
        this._OnBoardGGEs.RemoveAll(x => x == gge);
    }*/

    public void OnBoardGGEsClear()
    {
        lock (this._OnBoardGGEs)
        {
            this._OnBoardGGEs.Clear();
        }
    }

    public void RemoveOnBoardGGE(AGGEScript gge)
    {
        lock (this._OnBoardGGEs)
        {
            this._OnBoardGGEs.Remove(gge);
        }

        // No hace falta porque el único que puede quitar elementos de la sidebar es la propia sidebar (el usuario con el dedo).
        //NotifyObserversSideBar();
    }

    // NETWORK SIDEBAR.
    public List<AGenericGameElement> GetNetworkSidebar()
    {
        return this.NetworkSidebarGGE;
    }

    public void RemoveFromNetworkSidebar(AGenericGameElement gge)
    {
        lock (this.NetworkSidebarGGE)
        { 
            this.NetworkSidebarGGE.Remove(gge);
        }
    }

    public void AddToNetworkSidebar(AGenericGameElement gge)
    {
        lock (this.NetworkSidebarGGE)
        {
            this.NetworkSidebarGGE.Add(gge);
        }
    }

    // ALLGGEs Methods.
    public ICollection<AGenericGameElement> AllGGEs
    {
        get { return _AllGGEs; }
    }

    public List<AGenericGameElement> FindCardsAllGGEs()
    {
        return this._AllGGEs.FindAll(fl => this._AllGGEs.Contains(fl as Card));
    }

    public List<AGenericGameElement> FindByFileNameAllGGEs(string FileName)
    {
        return this._AllGGEs.FindAll(x => x.FileName == FileName);
    }
    
    // OBSERVER.
    public void RegisterObserver(IObserver o)
    {
        if (!Subscribers.Contains(o))
            Subscribers.Add(o);
    }

    public void DeleteObserver(IObserver o)
    {
        if (Subscribers.Contains(o))
            Subscribers.Remove(o);
    }

    // SIDEBAR OBSERVER METHODS.
    public void NotifyObserversClearSideBar()
    {
        // Recorremos todos los objetos suscritos (observers).
        IObserver observer;
        foreach (System.Object o in Subscribers)
        {
            // Cada observer ya hara lo que estime necesario con esa informacion.
            observer = (IObserver)o;
            observer.ClearSideBar();
        }
    }

    public void NotifyObserversAddedToSidebar(AGenericGameElement gge)
    {
        // Recorremos todos los objetos suscritos (observers).
        IObserver observer;
        foreach (System.Object o in Subscribers)
        {
            // Cada observer ya hara lo que estime necesario con esa informacion.
            observer = (IObserver)o;
            observer.AddedToSidebar(gge);
        }
    }

}
