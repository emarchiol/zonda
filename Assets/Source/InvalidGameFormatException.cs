﻿using UnityEngine;
using System.Collections;
using System;

public class InvalidGameFormatException : Exception {

    public InvalidGameFormatException(string value)
   : base(String.Format(value))
    {
        ConsoleScript.AddConsoleOutput(value);
    }
}
