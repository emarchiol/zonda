﻿using UnityEngine;
using System;
[Serializable]
public class Token : AGenericGameElement
{
	//Atributos de juego
	public string Shape{ get; set; }
	public string Image{ get; set; }
    public string Color { get; set; }

	/*Para debug*/
	public override void printAtt()
    {
		Debug.Log (this.Name);
		foreach (var item in ggeAttr) {
			Debug.Log(item.title);
			Debug.Log(item.description);
			Debug.Log(item.value);
		}
	}

	public override void InitObject()
    {
        if (Shape != "cube" && Shape != "circle")
        {
            //Default value for incorrect value.
            Shape = "cube";
        }
    }
}