﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Xml.Linq;
[Serializable]
public class CardDeck : ACollectionGameElement
{

	public string content;
    // Atributos de juego.
    public float RatioW;
    public float RatioH;
	public bool ShowFront;
    public string FrontImage;
    public string BackImage;
    // top, bot o random, para agregar cartas al mazo según convenga.
    public string AddsOn;

    public List<Card> Cards;

	// Constructors.
	public CardDeck()
    {
        this.Cards = new List<Card>();
    }
	
	public CardDeck(int ratiow, int ratioh, string name,string frontimage,string backimage, string exvalue, string invalue,int infoQuantity, List<Attr> attributes)
	{
		this.RatioH = ratioh;
		this.RatioW = ratiow;
		this.Name = name;
		this.FrontImage = frontimage;
		this.BackImage = backimage;
		this.infoQuantity = infoQuantity;
	}
    // Métodos para la lista adjunta que tienen.
    public override void Add(AGenericGameElement gge)
    {
        lock (this.Cards)
        { 
            // Agrego la carta al mazo.
            if (this.AddsOn == "top")
            {
                this.Cards.Insert(0, gge as Card);
            }
            else if (this.AddsOn == "bot")
            {
                this.Cards.Add(gge as Card);
            }
            else if (this.AddsOn == "random")
            {
                // por ahora es bot
                this.Cards.Add(gge as Card);
            }
        }
    }

    /*Metodo para debug*/
    public override void printAtt()
	{
		Debug.Log (this.Name);
		Debug.Log (this.content);
		Debug.Log (this.RatioW);
		Debug.Log (this.RatioH);
		Debug.Log (this.FrontImage);
		Debug.Log (this.BackImage);
		foreach (var item in ggeAttr) {
			Debug.Log("title:"+item.title);
			Debug.Log("description:"+item.description);
			Debug.Log("value:"+item.value);
		}
	}

    public override void ClearCollection()
    {
        this.Cards.Clear();
    }

    //Relleno el mazo, from ACollectionGameElement
    public override void InitObject(XElement e)
    {
        Game session = Game.NewInstance();
        int quantity=0;
        if ( this.AddsOn == null || (this.AddsOn.ToLower() != "top" && this.AddsOn.ToLower() != "bot" && this.AddsOn.ToLower() != "random"))
        {
            // Default.
            this.AddsOn = "top";
        }

        foreach (XElement c in e.Elements())
        {
            // Verifico que el elemento sea del tipo que me interesa.
            if (c.Name.LocalName.CompareTo("Card")==0)
            {
                quantity = 0;
                // Ahora que sé que el usuario no escribió cualquier pavada saco el objeto de AllGGE y lo agrego al mazo.
                foreach (AGenericGameElement g in session.AllGGEs)
                {
                    // Busco la carta.
                    if ((g.GetType().ToString() == c.Name.LocalName) && (g.FileName == c.Attribute("filename").Value) && quantity < (int)c.Attribute("quantity"))
                    {
                        g.InitObject();
                        this.Cards.Add(g as Card);
                        quantity++;
                        //Debug.Log(g.GetType().ToString() + ":" + g.FileName + " added to deck.");
                    }
                }
            }
            else Debug.Log("Game object:'" + c.Name.LocalName + "' is invalid for CardDeck");
        }
    }

    static System.Random rng = new System.Random();
    public override void Shuffle()
    {
        int n = this.Cards.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            var value = this.Cards[k];
            this.Cards[k] = this.Cards[n];
            this.Cards[n] = value;
        }
        // Dar vuelta las cartas.
        foreach (Card c in Cards)
        {
            c.isFlipped = false;
        }
    }

    // From AGenericGameElement
    public override void InitObject()
    {
        //throw new NotImplementedException();
    }
}
