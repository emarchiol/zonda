﻿using System;
//Generic Game Element Attributes, atributos adicionales que pueda tener un obj. de juego

[Serializable]
public class Attr {
	public string title;
	public string description;
	public int value;

	public Attr(string titulo, string descripcion, int valor){
		this.title = titulo;
		this.description = descripcion;
		this.value = valor;
	}

	public Attr(){}

}

