﻿using System;
using UnityEngine;
using System.Xml.Linq;
using System.Collections.Generic;
[Serializable]
public class TokenCollection : ACollectionGameElement
{
    public List<Token> Tokens;

    public TokenCollection()
    {
        this.Tokens = new List<Token>();
    }

    public override void Add(AGenericGameElement gge)
    {
        this.Tokens.Add(gge as Token);
    }

    public override void ClearCollection()
    {
    }

    public override void InitObject()
    {
    }

    public override void InitObject(XElement e)
    {
        Game session = Game.NewInstance();
        int quantity = 0;

        foreach (XElement c in e.Elements())
        {
            // Verifico que el elemento sea del tipo que me interesa.
            if (c.Name.LocalName.CompareTo("Token") == 0)
            {
                quantity = 0;
                // Ahora que sé que el usuario no escribió cualquier pavada saco el objeto de AllGGE y lo agrego al mazo.
                foreach (AGenericGameElement g in session.AllGGEs)
                {
                    // Busco la carta.
                    if ((g.GetType().ToString() == c.Name.LocalName) && (g.FileName == c.Attribute("filename").Value) && quantity < (int)c.Attribute("quantity"))
                    {
                        g.InitObject();
                        this.Tokens.Add(g as Token);
                        quantity++;
                        //Debug.Log(g.GetType().ToString() + ":" + g.FileName + " added to deck.");
                    }
                }
            }
            else Debug.Log("Game object:'" + c.Name.LocalName + "' is invalid for Token collection");
        }
    }

    public override void printAtt()
    {
    }

    public override void Shuffle()
    {
    }
}
