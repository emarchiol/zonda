﻿using System;
/* 
El "piso" o la mesa fisica se interpreta como un board, la diferencia es que para este se utiliza ratioW y ratioH como valor unitario mientras que para 
las boards (como un tablero de ajedrez) se interpretará como porcentaje del "piso" actual.
De tal manera los tablero nunca podrán ser más grandes que la mesa. La mesa en sí está definida en el core.xml bajo las opciones, mientras que los tablero
serán asignados al boardsetup o a la sidebar.
*/
[Serializable]
public class Board : AGenericGameElement
{
    public float RatioW { get; set; }
    public float RatioH { get; set; }
    public string BoardImage { get; set; }

    public override void InitObject()
    {
        throw new NotImplementedException();
    }

    public override void printAtt()
    {
        throw new NotImplementedException();
    }
}
