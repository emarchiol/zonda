﻿using UnityEngine;
using System.Collections.Generic;
using System;
[Serializable]
public class Card : AGenericGameElement
{

    //Atributos de juego
    public float RatioW;
    public float RatioH;
    public string FrontImage;
    public string BackImage;
    public bool isFlipped = false;

	//Constructors
	public Card(){}

	public Card(int ratiow, int ratioh, string name,string frontimage,string backimage, int infoQuantity, List<Attr> attributes)
	{
		this.RatioH = ratioh;
		this.RatioW = ratiow;
		this.Name = name;
		this.FrontImage = frontimage;
		this.BackImage = backimage;
		this.infoQuantity = infoQuantity;
		this.ggeAttr = attributes;
	}
	public override void InitObject(){}
	/*Metodo para debug*/
	public override void printAtt()
	{
		Debug.Log ("Name:"+this.Name);
		Debug.Log ("RatioW:" + this.RatioW);
		Debug.Log ("RatioH:" + this.RatioH);
		Debug.Log ("FrontImage:" + this.FrontImage);
		Debug.Log ("BackImage:" + this.BackImage);
		foreach (var item in ggeAttr) {
			Debug.Log("ggeAttrTitle:" + item.title);
			Debug.Log("ggeAttrDescription:" + item.description);
			Debug.Log("ggeAttrValue:" + item.value);
		}
	}
}
