﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.Xml;
using System;
using System.Xml.Linq;
[Serializable]
public abstract class ACollectionGameElement : AGenericGameElement
{
    public abstract void Add(AGenericGameElement gge);
    public abstract void InitObject(XElement elements);
    public abstract void Shuffle();
    public abstract void ClearCollection();
}