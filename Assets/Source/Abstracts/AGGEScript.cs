﻿using UnityEngine;
using UnityEngine.UI;
using System.Reflection;

/*
 * Clase abstracta que hereda de monobeahiour y de la cual heredan las clases CardScript TokenScript, etc para que puedan interactuar con monobeahiour, esta clase sirve
unicamente para declarar los metodos en comun que tienen dichas clases, como DoubleTap y otros 
 */

public abstract class AGGEScript : MonoBehaviour
{
    public GameObject InformationPanel;
    InfoPanelScript InfoPanelScript;

    public bool inZone = false;
    public GameObject SimpleMenu;
    public GameObject Menu;
    public bool isTouched = false;
	public bool menuOpen = false;
    public bool simpleMenuOpen = false;
    public GGeMenuScript MenuScript;
    protected Game session = Game.NewInstance();
    /*
	 * isLocked no lo arrastra, isStatic no hace NADA al recibir touch, la diferencia reside en que si isLocked es true y isStatic es false 
	 * tendremos el objeto llama a un menu cuando se hace hold&touch
	 */
    public bool isLocked = false;
	public bool isStatic = false;
	public AGenericGameElement gge;
    private GameObject PrefabClone;

    // Stack
    protected float DistanceToStack = 4.5f;

    //No usar awake en otros GGE, solo Start
    void Awake()
    {
        this.SetGGEMenuScript();
        InitObject();
    }

    public void SetGGEMenuScript()
    {
        this.MenuScript = this.gameObject.AddComponent<GGeMenuScript>();

    }

    public abstract void InitObject();

    //[MenuItem]
	public abstract void DoubleTap();
	//[MenuItem]
	public abstract void TripleTap();

	public abstract string GetName();

    [MenuItemAttribute]
    public void Info(/*GameObject infoPanel*/)
    {
        //InformationPanel = infoPanel;// Camera.main.GetComponent<GEServer>().GGEInformationPanel;
        this.InfoPanelScript = InformationPanel.GetComponent<InfoPanelScript>();
        if(!AGameEnviroment.cameraMovement)
            this.InfoPanelScript.gameObject.SetActive(true);
        else
            this.InfoPanelScript.gameObject.SetActive(false);
        this.InfoPanelScript.SetGGe(this);
    }

    [MenuItemAttribute]
	public abstract void Lock();
	
	public GameObject GetPrefabClone()
	{
		return PrefabClone;
	}

	public void SetPrefabClone(GameObject Prefab)
	{
		if(this.PrefabClone==null)
		this.PrefabClone = Prefab;
	}

	public abstract void SetPrefabReference(AGenericGameElement gge);

    public void SetGGe(AGenericGameElement GGE)
    {
        this.gge = GGE;
    }
    
    public void DestroyMenu()
    {
        MenuScript.DestroyMenu();
    }

    public void OpenMenu()
    {
        //this.Menu = Instantiate(MenuPrefab);
        //this.menuOpen = true; // Este no va.
        this.MenuScript.OpenSimpleMenu(this);
    }
    // Stack objects of the same type
    public abstract void StackObject();

}
