﻿using System.Collections.Generic;

public abstract class ACollectionGGEScript : AGGEScript
{
    public abstract void AddToCollection(AGenericGameElement gge);

    public void InitScriptObject()
    {
        if (this.gge.ggeAttr == null)
        {
            this.gge.ggeAttr = new List<Attr>();
        }
    }
}
