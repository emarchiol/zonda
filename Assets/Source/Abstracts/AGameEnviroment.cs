﻿using UnityEngine;
using System.Collections.Generic;
using System.Threading;
using System.Net.Sockets;
using System.Collections;
using UnityEngine.EventSystems;
using System;

//Clase abstracta de la cual hereda GEServer o GEClient, manipulacion de objetos, camara y eventos durante el juego.
/*
 * NOTA: De momento solo esta asociada GEServer a la camara principal de la escena GameBoard, cuando Network este funcionando sera Network que despues de llamar a la escena GameBoard con LoadScene
 * asignara a la camara de esa escena la clase correspondiente de GameEnviroment (Si es un cliente asigna GEClient y si es el server a GEServer, siempre con AddComponent()).
 * Por ende la camara de la escena GameBoard no deberia tener asociado ningun script por default (que es como esta ahora).
 * 
 */
public abstract class AGameEnviroment : MonoBehaviour {

    // Variables que hay que setear en la board.
    public GameObject GGEInformationPanel;
    public GameObject Console;
    protected Vector3 offset = Vector3.zero;
    protected float CameraYPosition;

    // Para escuchar llamadas entrantes.
    protected Socket ThreadSocket;
    protected Thread WaitingForObjectsThread;
    //Lista de todos los objetos de juego
    protected Game session = Game.NewInstance();
    public GameObject ZoneToServer;

    //public GameObject GGEMenuPrefab;

    //Para Draggable
    float DistanceToFloorOnDrag = 4.0f;
    public GameObject Board;
    public static GameObject ObjBeingTouched;
    //public List<GameObject> SelectedGameObjects = new List<GameObject>();
    protected GameObject SelectedGameObject;
    protected RaycastHit hit;
    protected Vector3 screenPoint;

    //Double mouse click
    protected bool one_click = false;
    protected float timer_for_double_click = 0f;
    //this is how long in seconds to allow for a double click
    protected float delay = 0.2f;

    //Zoom y movimiento de camara (tal vez deberían ir en session).
    public float perspectiveZoomSpeed = 0.12f;  // The rate of change of the field of view in perspective mode.
    public float orthoZoomSpeed = 0.12f;        // The rate of change of the orthographic size in orthographic mode.
    public float maxZoomOut = 135;
    public float maxZoomIn = 30;
    public static bool cameraMovement = true;       // Si estoy arrastrando un objeto el movimiento de la camara se deshabilita.
    public static bool CameraEnabled = true; 	    // Corresponde al checkbox.
    public static bool touchingUI = false;                 // Si se tocó un elemento de UI.
    //public static bool GGeTouch = true;			// Cuando se abre el log se deshabilita el movimiento de los objetos de juego.
    protected GameObject WorldTouch;
    public static GameObject InformationPanel;
    public CollectionGenerator cg { get; set; }

    //SideBar
    protected bool ShowSideBar = false;

    /*
	 * ZoneToServer tiene su propio script para collider con su funcion Update y no puede ser ejecutado en Update de GameEnviroment porque ocasionaria doble ejecucion de codigo
	 * y ciertas funcionalidades touch se verian mal afectadas, por ende se crea la asociacion correspondiente en esta clase abstracta antes de llamar al Start correspondiente.
	 * Esta asociacion solo es necesaria para habilitar o deshabilitar dicho compomente segun si es servidor o cliente
	 */
    public void Awake()
    {
        // Ya que tenemos todos los jugadores conectados y con su numero de player, les digo de cargar sus GGEs correspondientes desde los archivos playersN.xml
        using (cg = new CollectionGenerator())
        {
            cg.LoadPlayerSetup();
        }
        //session.NotifyObserversSideBar();
        ConsoleScript.AddConsoleOutput("Loading Player setup...");
        // Start listening thread.

        if (session.PlayerInfo.pNumber == 0 && session.playersAmount == 0)
        {
            Debug.Log("Solo session");
        }
        else
        {
            try
            {
                ThreadSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                WaitingForObjectsThread = new Thread(() => Network.KeepListeningForObjects(ThreadSocket));
                WaitingForObjectsThread.Start();
            }
            catch (ThreadStartException e)
            {
                Debug.Log("AGameEnviroment:" + e);
            }
        }

    }

    public void DetectDevice()
    {
#if (!UNITY_EDITOR && (UNITY_IPHONE || UNITY_ANDROID))
                MobileTouch();
#else
        //MobileTouch();
        PcTouch();
#endif
    }

    public abstract void Start();

    public void SetCameraStatus()
    {
        this.session.CameraEnabled = !this.session.CameraEnabled;
    }

    public abstract void Update();

    public abstract void ShutDownListeningThread();

    // TOUCH STUFF
    public float CameraLimits(string LimitDirection)
    {
        GameObject Wall;
        Wall = GameObject.Find(LimitDirection);
        float limit = 0;
        // Margen a la pared, si es 0 se moverá hasta encima de la pared.
        float margin = 0;

        if (LimitDirection == "NorthWall" || LimitDirection == "SouthWall")
        {
            if (Wall.transform.position.z < 0)
                return limit = Wall.transform.position.z + margin;
            else
                return limit = Wall.transform.position.z - margin;
        }

        if (LimitDirection == "EastWall" || LimitDirection == "WestWall")
        {
            if (Wall.transform.position.x < 0)
                return limit = Wall.transform.position.x + margin;
            else
                return limit = Wall.transform.position.x - margin;
        }
        return limit;
    }

    //Zoom in-out y movimiento de la camara con un dedo
    public void MobileCameraControl()
    {
        session.CameraIsChanging = true;
        try
        {
            //Move the camera
            if (Input.touchCount == 1)
            {
                // Store the touch
                Touch touchZero = Input.GetTouch(0);

                // Find the position in the previous frame of the touch.
                Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;

                //Distancia recorrida
                float deltaMagnitudeDiffx = touchZeroPrevPos.x - touchZero.position.x;
                float deltaMagnitudeDiffy = touchZeroPrevPos.y - touchZero.position.y;

                // If the camera is orthographic...
                if (GetComponent<Camera>().orthographic)
                {
                    if (GetComponent<Camera>().transform.position.x + (float)(GetComponent<Camera>().orthographicSize * 1.45) >= CameraLimits("EastWall"))
                    {
                        //Moverse solo a la izquierda
                        if (deltaMagnitudeDiffx < 0)
                            GetComponent<Camera>().transform.Translate(deltaMagnitudeDiffx * orthoZoomSpeed, 0, 0);
                    }
                    else if (GetComponent<Camera>().transform.position.x - (float)(GetComponent<Camera>().orthographicSize * 1.45) <= CameraLimits("WestWall"))
                    {
                        //Moverse solo a la derecha
                        if (deltaMagnitudeDiffx > 0)
                            GetComponent<Camera>().transform.Translate(deltaMagnitudeDiffx * orthoZoomSpeed, 0, 0);
                    }
                    else
                    {
                        GetComponent<Camera>().transform.Translate(deltaMagnitudeDiffx * orthoZoomSpeed, 0, 0);
                    }

                    if ((GetComponent<Camera>().transform.position.z + (float)(GetComponent<Camera>().orthographicSize) >= CameraLimits("NorthWall")))
                    {
                        //Moverse solo para abajo
                        if (deltaMagnitudeDiffy < 0)
                            GetComponent<Camera>().transform.Translate(0, deltaMagnitudeDiffy * orthoZoomSpeed, 0);

                    }
                    else if (GetComponent<Camera>().transform.position.z - (float)(GetComponent<Camera>().orthographicSize) <= CameraLimits("SouthWall"))
                    {
                        //Moverse solo para arriba
                        if (deltaMagnitudeDiffy > 0)
                            GetComponent<Camera>().transform.Translate(0, deltaMagnitudeDiffy * orthoZoomSpeed, 0);
                    }
                    else
                    {
                        GetComponent<Camera>().transform.Translate(0, deltaMagnitudeDiffy * orthoZoomSpeed, 0);
                    }
                }
                else
                {/* Perspective camera mode.*/}
            }

            // Zoom the camera
            if (Input.touchCount == 2)
            {
                // Store both touches.
                Touch touchZero = Input.GetTouch(0);
                Touch touchOne = Input.GetTouch(1);

                // Find the position in the previous frame of each touch.
                Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

                // Find the magnitude of the vector (the distance) between the touches in each frame.
                float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

                // Find the difference in the distances between each frame.
                float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

                // If the camera is orthographic...
                if (GetComponent<Camera>().orthographic)
                {
                    //zoom out check
                    if (deltaMagnitudeDiff > 0)
                    {
                        // Zoom out.
                        if (GetComponent<Camera>().transform.position.x + (float)(GetComponent<Camera>().orthographicSize * 1.4) > CameraLimits("EastWall") || GetComponent<Camera>().transform.position.z - (float)(GetComponent<Camera>().orthographicSize * 0.9) < CameraLimits("SouthWall") || GetComponent<Camera>().transform.position.z + (float)(GetComponent<Camera>().orthographicSize * 0.9) > CameraLimits("NorthWall") || GetComponent<Camera>().transform.position.x - (float)(GetComponent<Camera>().orthographicSize * 1.4) < CameraLimits("WestWall"))
                        { }
                        else
                        {
                            // ... change the orthographic size based on the change in distance between the touches.
                            GetComponent<Camera>().orthographicSize += deltaMagnitudeDiff * orthoZoomSpeed;
                            // Make sure the orthographic size never drops below zero.
                            GetComponent<Camera>().orthographicSize = Mathf.Clamp(GetComponent<Camera>().orthographicSize, maxZoomIn, maxZoomOut);
                        }

                        //reposicionar la camara segun los limites nseo
                        if (GetComponent<Camera>().transform.position.x + (float)(GetComponent<Camera>().orthographicSize * 1.4) > CameraLimits("EastWall"))
                        {
                            GetComponent<Camera>().transform.position = new Vector3(CameraLimits("EastWall") - ((float)(GetComponent<Camera>().orthographicSize * 1.4)), GetComponent<Camera>().transform.position.y, GetComponent<Camera>().transform.position.z);
                        }
                        if (GetComponent<Camera>().transform.position.x - (float)(GetComponent<Camera>().orthographicSize * 1.4) < CameraLimits("WestWall"))
                        {
                            GetComponent<Camera>().transform.position = new Vector3(CameraLimits("WestWall") + ((float)(GetComponent<Camera>().orthographicSize * 1.4)), GetComponent<Camera>().transform.position.y, GetComponent<Camera>().transform.position.z);
                        }
                        if (GetComponent<Camera>().transform.position.z + (float)(GetComponent<Camera>().orthographicSize * 0.9) > CameraLimits("NorthWall"))
                        {
                            GetComponent<Camera>().transform.position = new Vector3(GetComponent<Camera>().transform.position.x, GetComponent<Camera>().transform.position.y, CameraLimits("NorthWall") - ((float)(GetComponent<Camera>().orthographicSize)));
                        }
                        if (GetComponent<Camera>().transform.position.z - (float)(GetComponent<Camera>().orthographicSize * 0.9) < CameraLimits("SouthWall"))
                        {
                            GetComponent<Camera>().transform.position = new Vector3(GetComponent<Camera>().transform.position.x, GetComponent<Camera>().transform.position.y, CameraLimits("SouthWall") + ((float)(GetComponent<Camera>().orthographicSize)));
                        }
                    }
                    //zoom in check
                    else
                    {
                        // ... change the orthographic size based on the change in distance between the touches.
                        GetComponent<Camera>().orthographicSize += deltaMagnitudeDiff * orthoZoomSpeed;
                        // Make sure the orthographic size never drops below zero.
                        GetComponent<Camera>().orthographicSize = Mathf.Clamp(GetComponent<Camera>().orthographicSize, maxZoomIn, maxZoomOut);
                    }
                }
                else
                {
                    // Otherwise change the field of view based on the change in distance between the touches.
                    GetComponent<Camera>().fieldOfView += deltaMagnitudeDiff * perspectiveZoomSpeed;

                    // Clamp the field of view to make sure it's between 0 and 180.
                    GetComponent<Camera>().fieldOfView = Mathf.Clamp(GetComponent<Camera>().fieldOfView, maxZoomIn, maxZoomOut);
                }
            }
        }
        catch (Exception) { }
        // Restablezco Y de la cámara para evitar alejamientos/acercamientos no deseados.
        GetComponent<Camera>().transform.position = new Vector3(GetComponent<Camera>().transform.position.x, CameraYPosition, GetComponent<Camera>().transform.position.z);
    }

    // Control del touch para dispositivos móviles.
    public void MobileTouch()
    {
        Console.SetActive(ZondaConfigurationScript.ConsoleInGame);
        GameObject gameObjectHitted;
        try
        {
            if (session.GGeTouch)
            {
                if (Input.touchCount == 1)
                {
                    if (Input.GetTouch(0).phase == TouchPhase.Began)
                    {
                        // Se está tocando UI? el || no debería ir, es para debug con unity editor.
                        if (EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
                        {
                            touchingUI = true;
                            cameraMovement = false;
                            Debug.Log("not touching UI");
                        }
                        else
                        {
                            touchingUI = false;
                            cameraMovement = true;
                            this.GGEInformationPanel.SetActive(false);
                            Debug.Log("touching UI");
                        }

                        Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
                        int objectMask = 1 << 9;
                        if (Physics.Raycast(ray, out hit, 300, objectMask) && !touchingUI)
                        {
                            ObjBeingTouched = hit.collider.gameObject;
                            gameObjectHitted = hit.collider.gameObject; // Solo sirve para began xq los otros no lo reconocen, se podría remover.
                            // if I hitted a GGE and is not static.
                            if (gameObjectHitted.GetComponent<AGGEScript>() != null)
                            {
                                if (!gameObjectHitted.GetComponent<AGGEScript>().isStatic)
                                {
                                    cameraMovement = false;
                                    // Show the simple menu.
                                    //if(!gameObjectHitted.GetComponent<AGGEScript>().simpleMenuOpen)
                                    //if (ZondaConfigurationScript.SimpleMenu)
                                        gameObjectHitted.GetComponent<AGGEScript>().OpenMenu();
                                    // Did he double tapped the object ?
                                    if (Input.GetTouch(0).tapCount == 2)
                                    {
                                        gameObjectHitted.GetComponent<AGGEScript>().DoubleTap();
                                        gameObjectHitted.GetComponent<AGGEScript>().DestroyMenu();
                                    }
                                    // Did he triple tapped the object ? Es algo inútil el triple tap, se podría quitar
                                    else if (Input.GetTouch(0).tapCount == 3)
                                    {
                                        gameObjectHitted.GetComponent<AGGEScript>().TripleTap();
                                        //gameObjectHitted.GetComponent<AGGEScript>().simpleMenuOpen = false;
                                    }
                                    else
                                    {
                                        gameObjectHitted.transform.SetParent(Board.transform);
                                        // Levanto el objeto un poco para que parezca como seleccionado.
                                        gameObjectHitted.transform.position = new Vector3(gameObjectHitted.transform.position.x, DistanceToFloorOnDrag, gameObjectHitted.transform.position.z);
                                        screenPoint = Camera.main.WorldToScreenPoint(gameObjectHitted.transform.position);

                                        WorldTouch.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, screenPoint.z));
                                        offset = hit.collider.transform.position - WorldTouch.transform.position;
                                    }
                                    // Activo el panel de información del GGE correspondiente.
                                    if (ZondaConfigurationScript.InfoPanel)
                                        gameObjectHitted.GetComponent<AGGEScript>().Info(/*this.GGEInformationPanel*/);
                                }
                            }
                        }
                    }
                    //Touch&Hold logic, esto no se usa de momento
                    else if (Input.GetTouch(0).phase == TouchPhase.Stationary)
                    {
                        /*
                        hit.collider.gameObject.GetComponent<AGGEScript>().isTouched = true;
                        if (!hit.collider.gameObject.GetComponent<AGGEScript>().menuOpen)
						{
							if(HoldTime >= 0.7)
							{
                                //SelectedGameObjects[t].GetComponent<AGGEScript>().OpenMenu(this.GGEMenuPrefab);
								HoldTime = 0;
							}
							else
							{
								HoldTime += 2*Time.deltaTime;
							}
						}
                        */
                    }
                    //Movimiento del objeto
                    else if (Input.GetTouch(0).phase == TouchPhase.Moved && !touchingUI)
                    {
                        if (cameraMovement && session.CameraEnabled)
                        {
                            MobileCameraControl();
                        }
                        else if (hit.collider != null)
                        {
                            if (!hit.collider.gameObject.GetComponent<AGGEScript>().isStatic)
                            {
                                hit.collider.gameObject.GetComponent<AGGEScript>().isTouched = true;
                                // Movimiento del objeto.                                
                                WorldTouch.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y, screenPoint.z));

                                if (!hit.collider.gameObject.GetComponent<AGGEScript>().isLocked)
                                {
                                    float zValue = 0f;
                                    float xValue = 0f;
                                    xValue = WorldTouch.transform.position.x + offset.x;
                                    zValue = WorldTouch.transform.position.z + offset.z;

                                    hit.collider.gameObject.transform.position = new Vector3(xValue, DistanceToFloorOnDrag, zValue);
                                }
                            }
                        }
                    }
                    // Si el movimiento de la camara esta habilitado en el checkbox y mi rayo laser no toco ningun objeto entonces muevo la camara.
                    else if (Input.GetTouch(0).phase == TouchPhase.Ended)
                    {
                        ObjBeingTouched = null;
                        PointerEventData finger = new PointerEventData(EventSystem.current);
                        finger.position = Input.GetTouch(0).position;
                        List<RaycastResult> objectsHit = new List<RaycastResult>();
                        EventSystem.current.RaycastAll(finger, objectsHit);
                        foreach (RaycastResult r in objectsHit)
                        {
                            if (r.gameObject.layer == 11)
                            {
                                if (r.gameObject.GetComponent<ZonePlayerScript>() != null)
                                {
                                    r.gameObject.GetComponent<ZonePlayerScript>().SendGGEToPlayer(hit.collider.gameObject.GetComponent<AGGEScript>());
                                }

                                // Agrego a la sidebar.
                                if (r.gameObject.GetComponent<SideBarScript>() != null && hit.collider != null)
                                {
                                    if (hit.collider.GetComponent<ACollectionGGEScript>())
                                    {
                                        r.gameObject.GetComponent<SideBarScript>().AddedToSidebar(hit.collider.gameObject.GetComponent<AGGEScript>().gge);
                                        hit.collider.gameObject.GetComponent<AGGEScript>().DestroyMenu();
                                        Destroy(hit.collider.gameObject);
                                    }
                                    else
                                    {
                                        for (int i = 0; i <= hit.collider.gameObject.transform.childCount; i++)
                                        {
                                            r.gameObject.GetComponent<SideBarScript>().AddedToSidebar(hit.collider.gameObject.GetComponent<AGGEScript>().gge);
                                            hit.collider.gameObject.GetComponent<AGGEScript>().DestroyMenu();
                                            Destroy(hit.collider.gameObject);
                                        }
                                    }
                                }

                            }
                        }

                        if (hit.collider != null)
                            if (hit.collider.gameObject.GetComponent<AGGEScript>() != null)
                                if (!hit.collider.gameObject.GetComponent<AGGEScript>().isStatic)
                                {
                                    // Desactivo el simpleMenu y verifico que mi objeto no caiga sobre otro del mismo tipo (StackObject)
                                    if (hit.collider.gameObject.transform.parent.gameObject.GetComponent<AGGEScript>() == null)
                                    {
                                        hit.collider.gameObject.GetComponent<AGGEScript>().StackObject();
                                    }
                                    hit.collider.gameObject.GetComponent<AGGEScript>().isTouched = false;

                                    // Si una carta que acabo de soltar se soltó arriba de otra (no tiene padre) carta no hace falta arreglar la posición.
                                    if (hit.collider.gameObject != null && hit.collider.gameObject.transform.parent.gameObject.GetComponent<AGGEScript>() == null)
                                    {
                                        // Si está lockeado le establezco una altura de 1 y sino 1.2.
                                        if (hit.collider.gameObject.GetComponent<AGGEScript>().isLocked)
                                            hit.collider.gameObject.transform.position = new Vector3(hit.collider.gameObject.transform.position.x, 1f, hit.collider.gameObject.transform.position.z);
                                        else
                                            hit.collider.gameObject.transform.position = new Vector3(hit.collider.gameObject.transform.position.x, 1.2f, hit.collider.gameObject.transform.position.z);
                                    }
                                    cameraMovement = true;
                                }
                    }
                }
                else if (Input.touchCount == 2)
                {
                    if (cameraMovement && session.CameraEnabled)
                        MobileCameraControl();
                }
            }
        }
        catch (ArgumentOutOfRangeException)
        {
            //Molesta cuando no toucheo nada
            Debug.Log("outofrangeEXCEPTION");
        }
        catch (NullReferenceException e)
        {
            Debug.Log("touching UI ?:" + e);
        }
        catch (Exception e)
        {
            Debug.Log("GameEnviroment-DraggableRoutine: " + e);
        }
    }

    // Control del touch para pc.
    public void PcTouch()
    {
        PcCameraZoom();
        GameObject gameObjectHitted;
        Console.SetActive(ZondaConfigurationScript.ConsoleInGame);
        try
        {
            // PC, single touch only.
            if (Input.GetMouseButtonDown(0))
            {
                lastMousePosition = Input.mousePosition;
                // Se está tocando UI?
                if (EventSystem.current.IsPointerOverGameObject())
                {
                    touchingUI = true;
                    cameraMovement = false;
                }
                else
                {
                    touchingUI = false;
                    cameraMovement = true;
                    this.GGEInformationPanel.SetActive(false);
                }

                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                int objectMask = 1 << 9;
                if (Physics.Raycast(ray, out hit, 300, objectMask) && !touchingUI)
                {
                    ObjBeingTouched = hit.collider.gameObject;
                    gameObjectHitted = hit.collider.gameObject; // Solo sirve para began xq los otros no lo reconocen, se podría remover.
                                                                // if I hitted a GGE and is not static.
                    if (gameObjectHitted.GetComponent<AGGEScript>() != null)
                    {
                        if (!gameObjectHitted.GetComponent<AGGEScript>().isStatic)
                        {
                            cameraMovement = false;
                            // Show the simple menu.
                            //if(!gameObjectHitted.GetComponent<AGGEScript>().simpleMenuOpen)
                            //if (ZondaConfigurationScript.SimpleMenu)
                                gameObjectHitted.GetComponent<AGGEScript>().OpenMenu();

                            if (!one_click)
                            {
                                one_click = true;
                                timer_for_double_click = Time.time;
                            }
                            else
                            {
                                one_click = false;
                                // Double tap.
                                gameObjectHitted.GetComponent<AGGEScript>().DoubleTap();
                            }
                            gameObjectHitted.transform.SetParent(Board.transform);
                            // Levanto el objeto un poco para que parezca como seleccionado.
                            gameObjectHitted.transform.position = new Vector3(gameObjectHitted.transform.position.x, DistanceToFloorOnDrag, gameObjectHitted.transform.position.z);
                            screenPoint = Camera.main.WorldToScreenPoint(gameObjectHitted.transform.position);

                            WorldTouch.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
                            offset = hit.collider.transform.position - WorldTouch.transform.position;

                            // Activo el panel de información del GGE correspondiente.
                            if (ZondaConfigurationScript.InfoPanel)
                                gameObjectHitted.GetComponent<AGGEScript>().Info(/*this.GGEInformationPanel*/);
                        }
                    }
                }
            }
            else if (Input.GetMouseButton(0) && !touchingUI)
            {
                if (cameraMovement && session.CameraEnabled)
                {
                    PcCameraControl();
                }
                else if (hit.collider != null)
                {
                    if (!hit.collider.gameObject.GetComponent<AGGEScript>().isStatic)
                    {
                        hit.collider.gameObject.GetComponent<AGGEScript>().isTouched = true;
                        // Movimiento del objeto.                                
                        WorldTouch.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));

                        if (!hit.collider.gameObject.GetComponent<AGGEScript>().isLocked)
                        {
                            float zValue = 0f;
                            float xValue = 0f;
                            xValue = WorldTouch.transform.position.x + offset.x;
                            zValue = WorldTouch.transform.position.z + offset.z;

                            hit.collider.gameObject.transform.position = new Vector3(xValue, DistanceToFloorOnDrag, zValue);
                        }
                    }
                }
            }
            else if (Input.GetMouseButtonUp(0))
            {
                ObjBeingTouched = null;
                PointerEventData finger = new PointerEventData(EventSystem.current);
                finger.position = Input.mousePosition;
                List<RaycastResult> objectsHit = new List<RaycastResult>();
                EventSystem.current.RaycastAll(finger, objectsHit);
                foreach (RaycastResult r in objectsHit)
                {
                    if (r.gameObject.layer == 11)
                    {
                        if (r.gameObject.GetComponent<ZonePlayerScript>() != null)
                        {
                            r.gameObject.GetComponent<ZonePlayerScript>().SendGGEToPlayer(hit.collider.gameObject.GetComponent<AGGEScript>());
                        }

                        // Agrego a la sidebar.
                        if (r.gameObject.GetComponent<SideBarScript>() != null && hit.collider != null )
                        {
                            if (hit.collider.GetComponent<ACollectionGGEScript>())
                            {
                                r.gameObject.GetComponent<SideBarScript>().AddedToSidebar(hit.collider.gameObject.GetComponent<AGGEScript>().gge);
                                hit.collider.gameObject.GetComponent<AGGEScript>().DestroyMenu();
                                Destroy(hit.collider.gameObject);
                            }
                            else
                            { 
                                for(int i=0; i<= hit.collider.gameObject.transform.childCount; i++)
                                {
                                    r.gameObject.GetComponent<SideBarScript>().AddedToSidebar(hit.collider.gameObject.GetComponent<AGGEScript>().gge);
                                    hit.collider.gameObject.GetComponent<AGGEScript>().DestroyMenu();
                                    Destroy(hit.collider.gameObject);
                                }
                            }
                        }
                    }
                }

                if (hit.collider != null)
                    if (hit.collider.gameObject.GetComponent<AGGEScript>() != null)
                        if (!hit.collider.gameObject.GetComponent<AGGEScript>().isStatic)
                        {
                            // Desactivo el simpleMenu y verifico que mi objeto no caiga sobre otro del mismo tipo (StackObject)
                            if ( hit.collider.gameObject.transform.parent.gameObject.GetComponent<AGGEScript>() == null )
                            {
                                hit.collider.gameObject.GetComponent<AGGEScript>().StackObject();
                            }
                            hit.collider.gameObject.GetComponent<AGGEScript>().isTouched = false;

                            // Si una carta que acabo de soltar se soltó arriba de otra (stackobject) carta no hace falta arreglar la posición.
                            if (hit.collider.gameObject != null && hit.collider.gameObject.transform.parent.gameObject.GetComponent<AGGEScript>() == null)
                            {
                                // Si está lockeado le establezco una altura de 1 y sino 1.2.
                                if (hit.collider.gameObject.GetComponent<AGGEScript>().isLocked)
                                    hit.collider.gameObject.transform.position = new Vector3(hit.collider.gameObject.transform.position.x, 1f, hit.collider.gameObject.transform.position.z);
                                else
                                    hit.collider.gameObject.transform.position = new Vector3(hit.collider.gameObject.transform.position.x, 1.2f, hit.collider.gameObject.transform.position.z);
                            }
                            cameraMovement = true;
                        }
            }
            else if (one_click)
            {
                // if the time now is delay seconds more than when the first click started. 
                if ((Time.time - timer_for_double_click) > delay)
                {
                    //basically if thats true its been too long and we want to reset so the next click is simply a single click and not a double click.
                    one_click = false;
                }
            }
        }
        catch (NullReferenceException e)
        {
            Debug.Log("NullReference: Probably touch just didn't get any Object.:"+e);
        }

    }

    public void PcCameraZoom()
    {
        float maxPCZoomOut = 40;
        float maxPCZoomIn = 15;
        float cameraSpeed = 1.5f;
        // If the camera is orthographic...
        if (GetComponent<Camera>().orthographic)
        {
            float mouseWheel = Input.mouseScrollDelta.y;
            float cameraSize = GetComponent<Camera>().orthographicSize;
            if (mouseWheel > 0)
            {
                cameraSize -= mouseWheel * cameraSpeed;
            }
            else if (mouseWheel < 0)
            {
                cameraSize += (mouseWheel * -1) * cameraSpeed;
            }

            // Make sure the orthographic size never drops below specific range.
            if (cameraSize < maxPCZoomIn)
                cameraSize = maxPCZoomIn;
            if (cameraSize > maxPCZoomOut)
                cameraSize = maxPCZoomOut;

            GetComponent<Camera>().orthographicSize = cameraSize;
        }
    }

    Vector3 lastMousePosition;
    public void PcCameraControl()
    {
        session.CameraIsChanging = true;
        try
        {
            //Move the camera
            if (Input.GetMouseButton(0))
            {

                // Store the touch
                //Touch touchZero = Input.GetTouch(0);
                Vector2 touchZero = Input.mousePosition;

                // Find the position in the previous frame of the touch.
                //Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
                Vector2 touchZeroPrevPos = lastMousePosition;

                //Distancia recorrida
                float deltaMagnitudeDiffx = touchZeroPrevPos.x - touchZero.x;
                float deltaMagnitudeDiffy = touchZeroPrevPos.y - touchZero.y;

                // If the camera is orthographic...
                if (GetComponent<Camera>().orthographic)
                {
                    if (GetComponent<Camera>().transform.position.x + (float)(GetComponent<Camera>().orthographicSize * 1.45) >= CameraLimits("EastWall"))
                    {
                        //Moverse solo a la izquierda
                        if (deltaMagnitudeDiffx < 0)
                            GetComponent<Camera>().transform.Translate(deltaMagnitudeDiffx * orthoZoomSpeed, 0, 0);
                    }
                    else if (GetComponent<Camera>().transform.position.x - (float)(GetComponent<Camera>().orthographicSize * 1.45) <= CameraLimits("WestWall"))
                    {
                        //Moverse solo a la derecha
                        if (deltaMagnitudeDiffx > 0)
                            GetComponent<Camera>().transform.Translate(deltaMagnitudeDiffx * orthoZoomSpeed, 0, 0);
                    }
                    else
                    {
                        GetComponent<Camera>().transform.Translate(deltaMagnitudeDiffx * orthoZoomSpeed, 0, 0);
                    }

                    if ((GetComponent<Camera>().transform.position.z + (float)(GetComponent<Camera>().orthographicSize) >= CameraLimits("NorthWall")))
                    {
                        //Moverse solo para abajo
                        if (deltaMagnitudeDiffy < 0)
                            GetComponent<Camera>().transform.Translate(0, deltaMagnitudeDiffy * orthoZoomSpeed, 0);

                    }
                    else if (GetComponent<Camera>().transform.position.z - (float)(GetComponent<Camera>().orthographicSize) <= CameraLimits("SouthWall"))
                    {
                        //Moverse solo para arriba
                        if (deltaMagnitudeDiffy > 0)
                            GetComponent<Camera>().transform.Translate(0, deltaMagnitudeDiffy * orthoZoomSpeed, 0);
                    }
                    else
                    {
                        GetComponent<Camera>().transform.Translate(0, deltaMagnitudeDiffy * orthoZoomSpeed, 0);
                    }
                }
                else
                {/* Perspective camera mode.*/}
                lastMousePosition = Input.mousePosition;
            }

        }
        catch (Exception) { }
        // Restablezco Y de la cámara para evitar alejamientos/acercamientos no deseados.
        GetComponent<Camera>().transform.position = new Vector3(GetComponent<Camera>().transform.position.x, CameraYPosition, GetComponent<Camera>().transform.position.z);

    }
    // END TOUCH STUFF

    public abstract void MainMenuButton();

}
