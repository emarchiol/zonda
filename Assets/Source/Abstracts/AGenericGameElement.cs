﻿using System.Collections.Generic;
using System.Xml.Serialization;
using System;
/*
 * Clase abstracta de la cual heredan todos los elementos de juego.
 */
[Serializable]
public abstract class AGenericGameElement{

    //Atributos generales de juego
    public string Name;
    public int Id;
    public string FileName;
    public int infoQuantity;
    //Atributos para parseo xml
    [XmlArrayItemAttribute]
    [XmlArray("AttrList")]
    public List<Attr> ggeAttr;

    public abstract void printAtt();
    public abstract void InitObject();
}
