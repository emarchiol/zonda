﻿using UnityEngine;
using System;
using UnityEngine.UI;
using System.Net.Sockets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System.Threading;
using UnityEngine.SceneManagement;

//Manipulacion de objetos y eventos durante el juego
public class GEClient : AGameEnviroment
{

    // Prefab de sidebar para volver a transformar los objetos.
    public GameObject PrefabSideBar;
    public GameObject Canvas;
    public Button ButtonShowSideBar;
    public GameObject SideBar;
    public GameObject WaitingForPlayersScreen;
    
    Player ServerPlayer;

    public override void Start()
    {
        InformationPanel = GGEInformationPanel;

        foreach (Player p in session.GetPlayersList())
        {
            Debug.Log("player:" + p.name + "is in playersList");
            if (p.pNumber == 0)
                this.ServerPlayer = p;
        }
        // Debido a que la cámara está angulada al hacer zoom in se modifica el valor de Y ocasionando que la cámara deje de enfocar a la mesa (se aleja o acerca demasiado).    
        CameraYPosition = GetComponent<Camera>().transform.position.y;
        ConsoleScript.AddConsoleOutput("Board started.");
        WorldTouch = new GameObject("TempWorldTouch");
        //Esto es para que las cartas no se pongan "on top" con la camara
        Camera.main.transparencySortMode = TransparencySortMode.Orthographic;

        // La mesa fisica de la scene la asocio a la que fue cargada desde playerOptions
        //this.FloorPrefab.GetComponent<FloorScript>().Floor = session.PlayerFloor;
    }

    public void CheckServerLoaded()
    {
        Debug.Log("sending status request");
        NetMessage nMsg = new NetMessage(session.PlayerInfo, "SLOADED");
        Network.SendAGGEToPlayer(nMsg, this.ServerPlayer);
    }

    private float nextActionTime = 0.0f;
    public float period = 2f;
    public override void Update()
    {
        if (session.isServerLoaded)
        {
            if(this.WaitingForPlayersScreen.activeSelf)
                this.WaitingForPlayersScreen.SetActive(false);
            DetectDevice();
            if (session.GetNetworkSidebar().Count > 0)
            {
                // Agrego item a la sidebar.
                foreach (AGenericGameElement g in session.GetNetworkSidebar())
                {
                    Debug.Log("networkSidebar agge:" + g.Name);
                    session.AddToSideBarGGEs(g);
                }

                // Lo remuevo de network.
                /*List<AGenericGameElement> sidebarCopy;
                sidebarCopy = session.GetSideBarGGEs();*/
                foreach (AGenericGameElement g in session.GetSideBarGGEs())
                {
                    Debug.Log("sidebar agge:" + g.Name);
                    session.RemoveFromNetworkSidebar(g);
                }
            }
            // Server asked for session restart.
            if (session.restartSession)
            {
                this.RestartSession();
                session.restartSession = false;
            }
        }
        else
        {
            if (Time.time > nextActionTime)
            {
                nextActionTime = Time.time + period;
                CheckServerLoaded();
            }
        }
    }

    public void RestartSession()
    {
        // Limpio las listas sidebar y gameboard.
        session.ResetSession();

        try
        {
            // La carga de objetos del player se hace sola desde AGameEnviroment.
            ShutDownListeningThread();
            // Cargamos la scene.
            SceneManager.LoadScene(7);
            ConsoleScript.AddConsoleOutput("Session restarted.");
            ConsoleScript.AddConsoleOutput("---------------------------------");
        }
        catch (Exception e)
        {
            Debug.Log(e);
            //Application.LoadLevel(5);
        }
    }

    public void DisconnectMessage()
    {
        NetMessage nMsg = new NetMessage(session.PlayerInfo, "NETDISC");
        Network.SendAGGEToPlayer(nMsg, this.ServerPlayer);
    }

    public override void MainMenuButton()
    {
        // Somos buenas personas y avisamos que nos vamos.
        DisconnectMessage();
        // Cierro hilo y socket.
        ShutDownListeningThread();
        // Back to menu.
        SceneManager.LoadScene(0);
    }

    public override void ShutDownListeningThread()
    {
        try
        {
            if (ThreadSocket != null)
                ThreadSocket.Close();
        }
        catch (SocketException e)
        {
            Debug.Log("GEServer: " + e);
        }

        // Solo session ?
        try
        {
            if (WaitingForObjectsThread != null)
                WaitingForObjectsThread.Abort();
        }
        catch (Exception e)
        {
            Debug.Log("GEServer:thread abort:" + e);
        }
    }
}
