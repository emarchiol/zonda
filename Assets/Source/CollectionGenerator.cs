﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Text;
using System;
using System.Xml.Serialization;
using System.Reflection;
using System.Xml.Linq;
using System.Linq;

public class CollectionGenerator : IDisposable{

    private GGEFactory ggeFactory;
    //private string xmlCore;
    private string GamePath;
    private const string AppPath = "app/";
    private const string PlayersPath = "players/";
    private Game session = Game.NewInstance();

    public string gameName;
    public string gameVersion="";
    public string gameAuthor="";

    public CollectionGenerator()
    {
        this.ggeFactory = new GGEFactory();

        // Solo para testear board sin ir por todo el menu //Fake Session
        /*
        if (session == null)
        {
            Debug.Log("fake session initiated");
            AGameEnviroment.session = Game.NewInstance();
            AGameEnviroment.session.GamePath = Application.persistentDataPath+ "/temp/bdbbbcb95b0fa0801b23d4c3b1372cd7.zip/";
            AGameEnviroment.session.PlayerInfo.pNumber = 0;
            AGameEnviroment.session.PlayerFloor = new Board();
            this.session = AGameEnviroment.session;
            this.GamePath = AGameEnviroment.session.GamePath;

            this.CheckGameFormat();
            this.LoadPlayerSetup();
        }*/
        //End fake session

        // Busca el archivo core y le setea al path de la sesión la carpeta dónde se encuentra ese core. (devuelve el primer core que encuentre).
        if(Path.GetDirectoryName(SearchCoreFile(session.GamePath))==null)
            throw new InvalidGameFormatException("Error: core.xml not found.");
        session.GamePath = Path.GetDirectoryName(SearchCoreFile(session.GamePath))+"/";
        this.GamePath = session.GamePath;
    }

    // Esto se usa para generar el games.xml
    public CollectionGenerator(string GamePath)
    {
        if (Path.GetDirectoryName(SearchCoreFile(GamePath)) == null)
            throw new InvalidGameFormatException("");
        GamePath = Path.GetDirectoryName(SearchCoreFile(GamePath)) + "/";

        XDocument xdoc;
        try
        {
            xdoc = XDocument.Load(GamePath + "core.xml");
        }
        catch (XmlException)
        {
            throw new InvalidGameFormatException("");
        }

        if (xdoc.Root.Name.LocalName.CompareTo("ZondaGame") != 0)
        {
            throw new InvalidGameFormatException("");
        }
        else
        {
            // Todo esto solo para sacar el nombre del juego -.-.
            var gameXml = from root in xdoc.Descendants("ZondaGame")
                          select root;
            foreach (var r in gameXml)
            {
                if (r.Attribute("name") != null)
                {
                    this.gameName = r.Attribute("name").Value.ToString().ToLower();
                }
                else
                { 
                    throw new InvalidGameFormatException("");
                }

                if (r.Attribute("version") != null)
                {
                    this.gameVersion = r.Attribute("version").Value.ToString().ToLower();
                }
                else if (r.Attribute("author") != null)
                {
                    this.gameAuthor = r.Attribute("author").Value.ToString().ToLower();
                }

            }
        }

    }
    //Only for core.xml
    public bool CheckXMLTagQuery(XDocument xdoc, string tag, string rootTag)
    {
        var CheckOptions = from op in xdoc.Descendants(rootTag).Descendants()
                           where op.Name.LocalName == tag
                           select op;
        if (CheckOptions.Count() < 1)
            return false;
        else
            return true;
    }

    public string SearchCoreFile(string p)
    {
        // Get array of all file names.
        string[] a = Directory.GetFiles(p, "core.xml", SearchOption.AllDirectories);

        foreach (string name in a)
        {
            return name;
        }
        return null;
    }

    // Check core.xml format and min players files, se pide el path xq todavía no se ha creado una nueva session del juego
    public bool CheckGameFormat()
    {
        ConsoleScript.AddConsoleOutput("Checking core format...");
        XDocument xdoc;
        
        try
        {
            xdoc = XDocument.Load(GamePath + "core.xml");
        }
        catch (XmlException)
        {
            throw new InvalidGameFormatException("Error: core.xml format is invalid.");
        }
        if (xdoc.Root.Name.LocalName.CompareTo("ZondaGame") != 0)
            throw new InvalidGameFormatException("Error: <ZondaGame> root tag is missing in core.xml.");
        else
        {
            // Todo esto solo para sacar el nombre del juego -.-.
            var gameXml = from root in xdoc.Descendants("ZondaGame")
                          select root;
            foreach (var r in gameXml)
            {
                if (r.Attribute("name") != null)
                { 
                    ConsoleScript.AddConsoleOutput("Loading game: '" + r.Attribute("name").Value + "'.");
                    session.GameName = r.Attribute("name").Value.ToString().ToLower();
                }
                else
                    throw new InvalidGameFormatException("Error: <ZondaGame name=''> tag is missing in core.xml.");
            }
        }
        if(!CheckXMLTagQuery(xdoc, "GameOptions","ZondaGame"))
            throw new InvalidGameFormatException("Error: GameOptions tag is missing in core.xml.");
        if (!CheckXMLTagQuery(xdoc, "MinPlayers", "ZondaGame"))
            throw new InvalidGameFormatException("Error: MinPlayers tag is missing in core.xml.");
        if (!CheckXMLTagQuery(xdoc, "MaxPlayers", "ZondaGame"))
            throw new InvalidGameFormatException("Error: MaxPlayers tag is missing in core.xml.");

        // Add options here.
        var GameOptions = from option in xdoc.Descendants("GameOptions")
                          select new
                          {
                              minPlayers = option.Element("MinPlayers"),
                              maxPlayers = option.Element("MaxPlayers")
                          };
        
        // Verify min - max players.
        foreach (var g in GameOptions)
        {
            session.maxPlayers = (int)g.maxPlayers;
            session.maxPlayersGame = (int)g.maxPlayers;
            session.minPlayers = (int)g.minPlayers;

            for (int i = 0; i <= (int)g.minPlayers; i++)
            {
                if (!File.Exists(GamePath + PlayersPath + "player" + i + ".xml"))
                {
                    if (i == 0)
                        throw new InvalidGameFormatException("Error: player0.xml is a must be to at least try the game.");
                    else
                        ConsoleScript.AddConsoleOutput("Warning: player" +i+".xml file is missing.");
                }
            }
        }
        ConsoleScript.AddConsoleOutput("Core look ok.");
        try
        {
            LoadAllGGEFromCore();
            CheckPlayerFiles();
            return true;
        }
        catch (InvalidGameFormatException) { throw; }
    }

    // Revisa los archivos playerN.xml que no tengan más GGE de los que existen en AllGGE, de tal modo que no haya duplicados de id
    public void CheckPlayerFiles()
    {
        ConsoleScript.AddConsoleOutput("Checking players format...");
        int TotalPlayersgge;
        int i = 0;
        XDocument e = XDocument.Load(GamePath + "core.xml");
        try
        {
            foreach (KeyValuePair<string, System.Type> entry in ggeFactory.InstanciablesTypes)
            {
                var query = from d in e.Descendants("ZondaGame").Elements(entry.Key)
                            select d;
                // Para cada query de elementos: card, token etc...
                foreach (var g in query)
                {
                    TotalPlayersgge = 0;
                    string ggeFileName = (string)g.Attribute("filename").Value;
                    int ggeQuantity = (int)g.Attribute("quantity");
                    //Recorro todos los archivos
                    for (i = 0; i <= session.maxPlayers; i++)
                    {
                        if (File.Exists(GamePath + PlayersPath + "player" + i + ".xml"))
                        {
                            XDocument xdoc = XDocument.Load(GamePath + PlayersPath + "player" + i + ".xml");
                            /*var query2 = from el in xdoc.Descendants("SidebarSetup").Descendants()
                                        where el.Attribute("filename")?.Value == ggeFileName
                                        select el;*/
                            var query2 = from d in xdoc.Descendants("SideBarSetup").Descendants()
                                         where (string)d.Attribute("filename").Value == ggeFileName
                                         select d;

                            foreach (var d in query2)
                            {
                                TotalPlayersgge += (int)d.Attribute("quantity");
                            }
                        }
                    }
                    if (TotalPlayersgge > ggeQuantity)
                    {
                        throw new InvalidGameFormatException("Error: players.xml game objects are not redountant with core.xml.");
                    }
                }
            }
        }
        catch (NullReferenceException)
        {
            throw new InvalidGameFormatException("Error: filename or quantity is missing in player"+i+".xml for some game element.");
        }
        catch (ArgumentNullException)
        {
            throw new InvalidGameFormatException("Error: filename or quantity is missing in player"+i+".xml for some game element.");
        }
        ConsoleScript.AddConsoleOutput("Players look ok.");
    }

    // Populate ALLGGEs from core.xml.
    public void LoadAllGGEFromCore()
    {
        AGenericGameElement gge;
        int id = 1;
        XDocument e = XDocument.Load(GamePath + "core.xml");

        foreach (KeyValuePair<string, System.Type> entry in ggeFactory.InstanciablesTypes)
        {
            var query = from d in e.Descendants("ZondaGame").Elements(entry.Key)
                        select d;
            // Para cada query de elementos: card, token etc...
            foreach (var g in query)
            {
                try
                {
                    string ggeFileName = (string)g.Attribute("filename").Value;
                    string ggeType = (string)g.Name.LocalName;
                    int ggeQuantity = (int)g.Attribute("quantity");
                    if (ggeFactory.GetIGGEType(ggeType) != null)
                    {
                        for (int i = 0; i < ggeQuantity; i++)
                        {
                            gge = ggeFactory.CreateInstance(ggeType);
                            gge = ReadSerialized(ggeFileName, ggeType);
                            gge.Id = id;
                            gge.FileName = ggeFileName;
                            id++;
                            session.AllGGEs.Add(gge);
                        }
                    }
                }
                catch (NullReferenceException)
                {
                    throw new InvalidGameFormatException("Error: filename or quantity is missing in core.xml for some game element.");
                }catch (ArgumentNullException)
                {
                    throw new InvalidGameFormatException("Error: filename or quantity is missing in core.xml for some game element.");
                }
            }
        }
    }

    // VERIFICAR ID
    // Load objects for sidebar.
    public void SideBarSetup()
    {
        XDocument xdoc = XDocument.Load(this.GamePath + PlayersPath+ "player" + session.PlayerInfo.pNumber + ".xml");
        // Abre el archivo player correspondiente, los elementos dentro de SideBarSetup los copia desde AllGGE para que se mantenga la unicidad de los id.
        // Busco una entrada XML por cada tipo del framework.
        foreach (KeyValuePair<string, System.Type> entry in ggeFactory.InstanciablesTypes)
        {
            // Query con el tipo especficado dentro de SideBarSetup.
            var query = from d in xdoc.Descendants("SideBarSetup").Elements(entry.Key)
                        select d;
            // Foreach al resultado.
            foreach (XElement d in query)
            {
                int quantity = 0;
                // Si tiene subelementos entonces es un collection de algo.
                if (d.Elements().Count() > 0)
                {
                    // Busco el collection que le correponde en AllGGEs
                    foreach (AGenericGameElement g in session.AllGGEs)
                    {
                        // Si es del mismo tipo y se llama igual lo copio.
                        if ((d.Name.LocalName == g.GetType().ToString()) && (d.Attribute("filename").Value == g.FileName))
                        {
                            // Inicializo el Collection con sus elmentos asignado en el XML y después agrego el item a la sidebar.
                            ACollectionGameElement cg = (ACollectionGameElement) ggeFactory.CreateInstance(g.GetType().ToString());
                            cg = (ACollectionGameElement)g;
                            cg.ClearCollection();
                            cg.InitObject(d);

                            // Esto debería ir en el init de alguna manera.
                            if(d.Attribute("shuffle") != null)
                                if (d.Attribute("shuffle").Value == "true")
                                {
                                    cg.Shuffle();
                                }

                            session.AddToSideBarGGEs(cg);
                            //session.SideBarGGEs.Add(g);
                        }
                    }
                }
                //Si no es collection entonces directamente creo la copia desde AllGGEs
                else
                {
                    foreach (AGenericGameElement g in session.AllGGEs)
                    {
                        //Si es del mismo tipo y se llama igual lo copio
                        if ((d.Name.LocalName == g.GetType().ToString()) && (d.Attribute("filename").Value == g.FileName) && (quantity < (int)d.Attribute("quantity")))
                        {
                            g.InitObject();
                            session.AddToSideBarGGEs(g);
                            quantity++;
                        }
                    }
                }
            }
        }
    }

    // Load Objects for table.
    void BoardSetup()
    {
        /*
        Board playerBoard;
        XDocument xdoc = XDocument.Load(this.GamePath + PlayersPath + "player" + AGameEnviroment.session.PlayerInfo.pNumber + ".xml");
        // Abre el archivo player correspondiente, los elementos dentro de SideBarSetup los copia desde AllGGE para que se mantenga la unicidad de los id.
        // Busco una entrada XML por cada tipo del framework.
        var query = from d in xdoc.Element("ZondaPlayerSetup").Elements("BoardSetup")
                    select d;

        foreach (XElement b in query)
        {
            playerBoard = (Board)ggeFactory.CreateInstance("Board");
            playerBoard = (Board)ReadSerialized(b.Attribute("board").Value, "Board");
            playerBoard.Id = 0;
            playerBoard.FileName = b.Attribute("board").Value;
            if (playerBoard.RatioH < 1)
                playerBoard.RatioH = 1;
            if (playerBoard.RatioW < 1)
                playerBoard.RatioW = 1;
            session.PlayerFloor = playerBoard;
        }
        */
    }

    public void PlayerOptionsSetup()
    {
        ConsoleScript.AddConsoleOutput("Loading Player options...");
        XDocument xdoc = XDocument.Load(this.GamePath + PlayersPath + "player" + session.PlayerInfo.pNumber + ".xml");
        if (!CheckXMLTagQuery(xdoc, "PlayerOptions","ZondaPlayerSetup"))
            ConsoleScript.AddConsoleOutput("Warning: PlayerOptions is missing from player" + session.PlayerInfo.pNumber + ".xml file, loading defaults values.");
        
        // Add options here.
        var GameOptions = from option in xdoc.Descendants("PlayerOptions")
                          select new
                          {
                              FloorW = option.Element("FloorW"),
                              FloorH = option.Element("FloorH"),
                              FloorImage = option.Element("FloorImage")
                          };

        // Verify min - max players.
        if (GameOptions.Count() < 1)
        {
            session.PlayerFloor.RatioH = 1;
            session.PlayerFloor.RatioW = 1;
            //session.PlayerFloor.BoardImage = (string)g.FloorImage;
        }
            
        foreach (var g in GameOptions)
        {
            // Si alguna de estas opciones no está no tiene que importar y se tiene que setear algo por default.
            if (g.FloorH == null || (int)g.FloorH < 1)
                session.PlayerFloor.RatioH = 1;
            else
                session.PlayerFloor.RatioH = (int)g.FloorH;

            if (g.FloorW == null || (int)g.FloorW < 1)
                session.PlayerFloor.RatioW = 1;
            else
                session.PlayerFloor.RatioW = (int)g.FloorW;
            // En proceso
            if (g.FloorImage != null)
                session.PlayerFloor.BoardImage = (string)g.FloorImage;
        }
    }

    // Carga en memoria los objetos que corresponden al player indicado
    public void LoadPlayerSetup()
    {
        try
        {
            PlayerOptionsSetup();
            SideBarSetup();
            BoardSetup();
        }
        catch (InvalidGameFormatException)
        {
            
        }
    }

	//Como dice, lee objetos serializados gge segun lo que encuentra en core.xml
	//AGenericGameElement gge es el objeto en donde se va a guardar el deserializado, ggeType el tipo de objeto (carta, token)
	public AGenericGameElement ReadSerialized(string ggeName, string ggeType){
		string ggePath = this.GamePath + AppPath + ggeName+".xml";
		Type unknowType = Type.GetType (ggeType);
		XmlSerializer serializer = new XmlSerializer(unknowType);
		FileStream stream = new FileStream(ggePath, FileMode.Open);
		try{
            return  serializer.Deserialize(stream) as AGenericGameElement;
		}catch(Exception e)
        {
			Debug.Log("Invalid XML");
			Debug.Log(e);
            return null;
		}
		finally{stream.Close();}
	}

    public void Dispose()
    {
        //throw new NotImplementedException();
    }
}

/*Testeo de SERIALIZACION con atributos
		GGEAttr atributos = new GGEAttr ("titulo", "asdasdasd", 32);
		List<GGEAttr> atlist = new List<GGEAttr>();
		atlist.Add (atributos);
		Card cartaSerializada = new Card(1,2,3,"cartita","testSerialized.png","dsa.png","4","3",15,atlist);

		var serializer = new XmlSerializer(typeof(Card));
		var stream = new FileStream(path+"testSerialized.xml", FileMode.Open);
		serializer.Serialize(stream, cartaSerializada);
		stream.Close();
 */
