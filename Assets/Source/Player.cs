﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class Player {

	public string name { get; set; }
	public string ipAddress { get; set; }
	public int pNumber { get; set; }
    public string GameMd5 { get; set; }
    public string gameName { get; set; }
    //private List<AGenericGameElement> PlayersGGes;

    public Player(string pName, string pIp, int pNum, string pMd5, string gName)
    {
        this.name = pName;
        this.ipAddress = pIp;
        this.pNumber = pNum;
        this.GameMd5 = pMd5;
        this.gameName = gName;
    }

    public Player(string pName, string pIp, int pNum, string pMd5)
    {
        this.name = pName;
        this.ipAddress = pIp;
        this.pNumber = pNum;
        this.GameMd5 = pMd5;
    }

    public Player() { }

	public void printAtt(){

		Debug.Log(this.name);
		Debug.Log(this.pNumber);
		Debug.Log(this.ipAddress);
	}
}
