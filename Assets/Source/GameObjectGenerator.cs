﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Text;
using System;
using System.Xml.Serialization;
using UnityEngine.UI;
/*
 * Esta clase genera los objetos de juego (como cartas, tokens y demas) que fueron leidos por CollectionGenerator desde algun formato de texto
 * Hereda de MonoBehaviour porque necesita interactuar directamente con unity para instanciar los prefabs.
 */
public class GameObjectGenerator : MonoBehaviour{

    //Atributos de spawneo
    //Tienen que ser transform para poder representarlos en el canvas
    //Card es el prefab, clone son los creados desde el prefab, SpriteFF = spriteFromFile (desde una imagen), www se usa para "descargar" la imagen con url.
    static GameObject clone;
    private static Game session = Game.NewInstance();

    //Creo los items que necesito en la mesa

    /*public void SpawnAllObjects()
    {
		float xCoord = -8f;
        
        foreach (AGenericGameElement gge in AGameEnviroment.session.GetSideBarGGEs())
        {
            //Genero una instancia del prefab segun su tipo, Resource.Load se fija en la carpeta Resource si hay un elemento del tipo gge.GetType().ToString()
            clone = Instantiate(Resources.Load("Prefab/"+gge.GetType().ToString()), new Vector3(xCoord,1,0), Quaternion.identity) as GameObject;
            clone.transform.Rotate(new Vector3(0,-180,0));
			this.clone.GetComponent<AGGEScript>().SetPrefabClone(clone);
			//Movimiento de coordenadas x para que los objetos no aparezcan todos en el mismo lugar, esto se cambiara posteriormente
			xCoord+=10f;
			SetObjectScript(gge);
		}
	}*/

    public static void SpawnObject(AGenericGameElement GGe, Vector3 PositionToSpawn)
	{
        GameObject BoardContainer = GameObject.Find("Board"); //Esto es posible que esté mal
        string prefabToInstance;
        //Genero una instancia del prefab segun su tipo, Resource.Load se fija en la carpeta Resource si hay un elemento del tipo gge.GetType().ToString()

        // Esto habría que cambiarlo
        if (GGe.GetType().ToString() != "Token")
        {
            prefabToInstance = "Prefab/" + GGe.GetType().ToString();
        }
        else
        {
            Token t = (Token)GGe;
            prefabToInstance = "Prefab/" + GGe.GetType().ToString()+ t.Shape;
        }

        if (GGe is ACollectionGameElement)
        {
            clone = Instantiate(Resources.Load("Prefab/GGECollections/" + GGe.GetType().ToString()), new Vector3(PositionToSpawn.x, 1, PositionToSpawn.z), Quaternion.identity) as GameObject;
        }
        else
        { 
            clone = Instantiate(Resources.Load(prefabToInstance), new Vector3(PositionToSpawn.x,1, PositionToSpawn.z), Quaternion.identity) as GameObject;
            
        }

        // Esto lo debería hacer el init del objeto.
        if(GGe is Card || GGe is CardDeck)
            clone.transform.Rotate(new Vector3(0, -180, 0));
        clone.GetComponent<AGGEScript>().SetPrefabClone(clone);
        SetObjectScript(GGe);

        // Lo pongo en el GameObject "Board", esto es para que todos los elementos estén en el mismo contenedor.
        clone.transform.SetParent(BoardContainer.transform);

        //El gge ya está spawneado en la mesa por lo tanto lo agrego a la OnBoardList
        session.AddToOnBoardGGEs(clone.GetComponent<AGGEScript>());
    }

    //Cada prefab tendra asociado un script con su objeto especifico para poder interactuar con Monobehaviour
    public static void SetObjectScript(AGenericGameElement gge )
    {
		try
		{
            clone.GetComponent<AGGEScript>().SetGGe(gge);
            clone.GetComponent<AGGEScript>().SetPrefabReference(gge);
            clone.GetComponent<AGGEScript>().InformationPanel = AGameEnviroment.InformationPanel;
            if (gge is ACollectionGameElement)
            {
                clone.GetComponent<ACollectionGGEScript>().InitScriptObject();
            }
        }
        catch(Exception e)
		{
			Debug.Log("No pude crear el gge:"+e);
		}
	}
}
