﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
[Serializable]
public class NetMessage : AGenericGameElement
{
    public Player PlayerInfo;
    public List<AGenericGameElement> sessionGGE;
    
    // Flags de mensajes.
    Dictionary<string, bool> Flags = new Dictionary<string, bool>()
    {
        // Los clientes usan este mensaje para preguntar al server si cargó, server responde con el mismo FLAG, si el cliente lo recibe como verdadero entonces habilita la mesa.
        // Cuando server se queda esperando conexiones no analiza NetMessage, por lo tanto este mensaje solo se leerá cuando la mesa del server se haya cargado.
        {"SLOADED",false },
        // Si verdadero, hay que restartear sesión.
        {"SRESTART",false },
        // Es solo un request de log ?
        {"LOGREQUEST",false },
        // Si verdadero, ToPlayer se agrega a la lista de players del receptor.
        {"PSETUP",false },
        // Si verdadero, se está entregando una lista de GGE para guardar una partida o generar un log.
        {"SESSIONLOG",false },
        // ERROR
        {"NETERROR",false },
        // Usuario se quiso desconectar.
        {"NETDISC",false },
        // No correct flag inserted.
        {"NULL",false }
    };

    // Player setup.
    public NetMessage(Player playerToAdd)
    {
        Name = "Network Message";
        this.PlayerInfo = playerToAdd;
        this.Flags["PSETUP"] = true;
    }

    public string checkFlag()
    {
        string key="";
        foreach (var flagSelected in Flags)
        {
            if(flagSelected.Value)
                key = flagSelected.Key;
        }
        return key;
    }

    // Log request or server finished loaded msg.
    // requesting log = LOGREQUEST, is server loaded = SLOADED, server requesting session restart = SRESTART
    public NetMessage(Player PlayerSendingInfo, string flag) 
    {
        Name = "Network Message";
        this.PlayerInfo = PlayerSendingInfo;

        // Si escribió cualquier pavada.
        if (this.Flags.ContainsKey(flag))
            this.Flags[flag] = true;
        else
            this.Flags["NULL"] = true;
    }

    // Complete log to send.
    public NetMessage(Player PlayerSendingLog, List<AGenericGameElement> allPlayerGGe)
    {
        Name = "Network Message";
        this.PlayerInfo = PlayerSendingLog;
        this.sessionGGE = allPlayerGGe;
        this.Flags["SESSIONLOG"] = true;
    }

    public override void InitObject(){}
    public override void printAtt(){}
}
